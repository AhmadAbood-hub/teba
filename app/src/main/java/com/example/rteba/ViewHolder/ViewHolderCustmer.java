package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderCustmer extends RecyclerView.ViewHolder
{
    private View view;
    private CircleImageView imgCustmerDelete;
    private TextView txtCustmerName;
    private TextView txtCustmerNumber;



    public ViewHolderCustmer(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtCustmerName = (TextView) view.findViewById(R.id.txtCustmerName);
        txtCustmerNumber = (TextView) view.findViewById(R.id.txtCustmerNumber);
        imgCustmerDelete = (CircleImageView) view.findViewById(R.id.imgCustmerDelete);
    }

    public View getView()
    {
        return view;
    }


    public TextView getTxtCustmerName()
    {
        return txtCustmerName;
    }

    public TextView getTxtCustmerNumber()
    {
        return txtCustmerNumber;
    }

    public CircleImageView getImgCustmerDelete()
    {
        return imgCustmerDelete;
    }
}
