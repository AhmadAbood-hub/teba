package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderJob extends RecyclerView.ViewHolder
{
    private View view;
    private CircleImageView imgJobDelete;
    private TextView txtJobName;
    private TextView txtJobSalary;
    private TextView txtJobHour;
    private TextView txtJobCostPerHour;



    public ViewHolderJob(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtJobName = (TextView) view.findViewById(R.id.txtJobName);
        txtJobSalary = (TextView) view.findViewById(R.id.txtJobSalary);
        txtJobHour = (TextView) view.findViewById(R.id.txtJobHour);
        txtJobCostPerHour = (TextView) view.findViewById(R.id.txtJobCostPerHour);
        imgJobDelete = (CircleImageView) view.findViewById(R.id.imgJobDelete);
    }

    public View getView()
    {
        return view;
    }


    public TextView getTxtJobName()
    {
        return txtJobName;
    }

    public TextView getTxtJobSalary()
    {
        return txtJobSalary;
    }

    public TextView getTxtJobHour()
    {
        return txtJobHour;
    }

    public TextView getTxtJobCostPerHour()
    {
        return txtJobCostPerHour;
    }

    public CircleImageView getImgJobDelete()
    {
        return imgJobDelete;
    }
}
