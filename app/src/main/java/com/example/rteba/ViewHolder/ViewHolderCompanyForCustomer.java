package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderCompanyForCustomer extends RecyclerView.ViewHolder
{
    private View view;


    private ImageView imgCompanyLogo;
    private TextView txtCompanyName;
    private TextView txtCompanyLocation;

    public ViewHolderCompanyForCustomer(@NonNull View view)
    {
        super(view);

        this.view = view;

        imgCompanyLogo = (ImageView) view.findViewById(R.id.imgCompanyLogo);

        txtCompanyName = (TextView) view.findViewById(R.id.txtCompanyName);
        txtCompanyLocation = (TextView) view.findViewById(R.id.txtCompanyLocation);


    }


    public View getView()
    {
        return view;
    }

    public ImageView getImgCompanyLogo()
    {
        return imgCompanyLogo;
    }


    public TextView getTxtCompanyName()
    {
        return txtCompanyName;
    }


    public TextView getTxtCompanyLocation()
    {
        return txtCompanyLocation;
    }
}
