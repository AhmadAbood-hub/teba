package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderRepository extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtRepositoryLocation;
    private TextView txtRepositoryAnnualRent;
    private TextView txtRepositoryManeger;

    private CircleImageView imgRepositoryDelete;


    public ViewHolderRepository(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtRepositoryLocation = (TextView) view.findViewById(R.id.txtRepositoryLocation);
        txtRepositoryAnnualRent = (TextView) view.findViewById(R.id.txtRepositoryAnnualRent);
        txtRepositoryManeger = (TextView) view.findViewById(R.id.txtRepositoryManeger);
        imgRepositoryDelete = (CircleImageView)view.findViewById(R.id.imgRepositoryDelete);
    }

    public View getView()
    {
        return view;
    }

    public TextView getTxtRepositoryLocation()
    {
        return txtRepositoryLocation;
    }

    public TextView getTxtRepositoryAnnualRent()
    {
        return txtRepositoryAnnualRent;
    }

    public TextView getTxtRepositoryManeger()
    {
        return txtRepositoryManeger;
    }

    public CircleImageView getImgRepositoryDelete()
    {
        return imgRepositoryDelete;
    }
}
