package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderBillItem extends RecyclerView.ViewHolder {
    private View view;
    private TextView txtBillItemName;
    private TextView txtBillItemCompany;
    private TextView txtBillItemPrice;
    private TextView txtBillItemCount;

    private CircleImageView imgBillItemDelete;
    private ImageView imgBillItemImage;


    public ViewHolderBillItem(@NonNull View view) {
        super(view);

        this.view = view;

        txtBillItemName = (TextView) view.findViewById(R.id.txtBillItemName);
        txtBillItemCompany = (TextView) view.findViewById(R.id.txtBillItemCompany);
        txtBillItemPrice = (TextView) view.findViewById(R.id.txtBillItemPrice);
        txtBillItemCount = (TextView) view.findViewById(R.id.txtBillItemCount);

        imgBillItemDelete = view.findViewById(R.id.imgBillItemDelete);

        imgBillItemImage = (ImageView) view.findViewById(R.id.imgBillItemImage);

    }

    public View getView() {
        return view;
    }

    public TextView getTxtBillItemName() {
        return txtBillItemName;
    }

    public TextView getTxtBillItemCompany() {
        return txtBillItemCompany;
    }

    public TextView getTxtBillItemPrice() {
        return txtBillItemPrice;
    }

    public TextView getTxtBillItemCount() {
        return txtBillItemCount;
    }

    public CircleImageView getImgBillItemDelete() {
        return imgBillItemDelete;
    }

    public ImageView getImgBillItemImage() {
        return imgBillItemImage;
    }
}
