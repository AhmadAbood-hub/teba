package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderItemCart extends RecyclerView.ViewHolder
{
    private View view;


    private ImageView imgItemImage;
    private TextView txtItemName;
    private TextView txtItemCompany;
    private TextView  txtItemPrice;

    public ViewHolderItemCart(@NonNull View view)
    {
        super(view);

        this.view = view;

        imgItemImage = (ImageView) view.findViewById(R.id.imgItemImage);
        txtItemName = (TextView) view.findViewById(R.id.txtItemName);
        txtItemCompany = (TextView) view.findViewById(R.id.txtItemCompany);
        txtItemPrice = (TextView) view.findViewById(R.id.txtItemPrice);


    }


    public View getView()
    {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public ImageView getImgItemImage() {
        return imgItemImage;
    }







    public TextView getTxtItemName() {
        return txtItemName;
    }


    public TextView getTxtItemCompany() {
        return txtItemCompany;
    }


    public TextView getTxtItemPrice() {
        return txtItemPrice;
    }




}
