package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

public class ViewHolderTypeForCustomer extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtTypeNameForCustomer;



    public ViewHolderTypeForCustomer(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtTypeNameForCustomer = (TextView) view.findViewById(R.id.txtTypeNameForCustomer);

    }

    public View getView()
    {
        return view;
    }

    public TextView getTextTypeNameForCustomer()
    {
        return txtTypeNameForCustomer;
    }

}
