package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderQuantity extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtQuantityQuantity;
    private TextView txtQuantityUnit;
    private CircleImageView imgQuantityDelete;


    public ViewHolderQuantity(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtQuantityQuantity = (TextView) view.findViewById(R.id.txtQuantityQuantity);
        txtQuantityUnit = (TextView) view.findViewById(R.id.txtQuantityUnit);
        imgQuantityDelete = (CircleImageView)view.findViewById(R.id.imgQuantityDelete);
    }

    public View getView()
    {
        return view;
    }


    public TextView getTxtQuantityQuantity()
    {
        return txtQuantityQuantity;
    }

    public CircleImageView getImgQuantityDelete()
    {
        return imgQuantityDelete;
    }

    public TextView getTxtQuantityUnit()
    {
        return txtQuantityUnit;
    }
}
