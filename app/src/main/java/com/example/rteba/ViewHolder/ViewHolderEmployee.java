package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderEmployee extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtEmployeeName;
    private TextView txtEmployeeNumber;
    private TextView txtEmployeeJob;

    private CircleImageView imgEmployeeDelete;


    public ViewHolderEmployee(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtEmployeeName = (TextView) view.findViewById(R.id.txtEmployeeName);
        txtEmployeeNumber = (TextView) view.findViewById(R.id.txtEmployeeNumber);
        txtEmployeeJob = (TextView) view.findViewById(R.id.txtEmployeeJob);
        imgEmployeeDelete = (CircleImageView)view.findViewById(R.id.imgEmployeeDelete);
    }

    public View getView()
    {
        return view;
    }

    public TextView getTxtEmployeeName()
    {
        return txtEmployeeName;
    }

    public TextView getTxtEmployeeNumber()
    {
        return txtEmployeeNumber;
    }

    public CircleImageView getImgEmployeeDelete()
    {
        return imgEmployeeDelete;
    }

    public TextView getTxtEmployeeJob()
    {
        return txtEmployeeJob;
    }
}
