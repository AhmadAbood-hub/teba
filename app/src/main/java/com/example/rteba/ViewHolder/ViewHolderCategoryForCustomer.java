package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

public class ViewHolderCategoryForCustomer  extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtCategoryNameForCustomer;



    public ViewHolderCategoryForCustomer(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtCategoryNameForCustomer = (TextView) view.findViewById(R.id.txtCategoryNameForCustomer);

    }

    public View getView()
    {
        return view;
    }

    public TextView getTextCategoryNameForCustomer()
    {
        return txtCategoryNameForCustomer;
    }

}
