package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderType extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtTypeName;
    private CircleImageView imgTypeDelete;


    public ViewHolderType(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtTypeName = (TextView) view.findViewById(R.id.txtTypeName);
        imgTypeDelete = (CircleImageView)view.findViewById(R.id.imgTypeDelete);
    }

    public View getView()
    {
        return view;
    }

    public TextView getTxtTypeName()
    {
        return txtTypeName;
    }

    public CircleImageView getImgTypeDelete()
    {
        return imgTypeDelete;
    }
}
