package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.rteba.View.Fragment.BillsFragment.Cus;

public class ViewHolderBill extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtBillCustmer;
    private TextView txtBillDate;
    private TextView txtBillTime;
    private CircleImageView imgBillDelete;
    private CircleImageView imgUser;


    public ViewHolderBill(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtBillCustmer = (TextView) view.findViewById(R.id.txtbillCustmer);
        txtBillDate = (TextView) view.findViewById(R.id.txtbillDate);
        txtBillTime = (TextView) view.findViewById(R.id.txtbillTime);
        imgUser = (CircleImageView)view.findViewById(R.id.imgUser);

        imgBillDelete = (CircleImageView)view.findViewById(R.id.imgBillDelete);
        if(Cus.getState()==0)
            imgBillDelete.setVisibility(View.GONE);
    }

    public View getView()
    {
        return view;
    }


    public TextView getTxtBillCustmer() {
        return txtBillCustmer;
    }

    public TextView getTxtBillDate() {
        return txtBillDate;
    }

    public TextView getTxtBillTime() {
        return txtBillTime;
    }


    public CircleImageView getImgUser() {
        return imgUser;
    }


    public CircleImageView getImgBillDelete() {
        return imgBillDelete;
    }
}
