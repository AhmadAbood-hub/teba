package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderCategory extends RecyclerView.ViewHolder
{
    private View view;
    private TextView txtCategoryName;
    private CircleImageView imgCategoryDelete;


    public ViewHolderCategory(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtCategoryName = (TextView) view.findViewById(R.id.txtCategoryName);
        imgCategoryDelete = (CircleImageView)view.findViewById(R.id.imgCategoryDelete);
    }

    public View getView()
    {
        return view;
    }

    public TextView getTxtCategoryName()
    {
        return txtCategoryName;
    }

    public CircleImageView getImgCategoryDelete()
    {
        return imgCategoryDelete;
    }
}
