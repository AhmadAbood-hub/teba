package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderItemForCustomer extends RecyclerView.ViewHolder
{
    private View view;


    private ImageView imgItemImage;
    private TextView txtItemName;

    public ViewHolderItemForCustomer(@NonNull View view)
    {
        super(view);

        this.view = view;

        imgItemImage = (ImageView) view.findViewById(R.id.imgItemImage);

        txtItemName = (TextView) view.findViewById(R.id.txtItemName);


    }


    public View getView()
    {
        return view;
    }

    public ImageView getImgItemImage()
    {
        return imgItemImage;
    }

    public TextView getTxtItemName()
    {
        return txtItemName;
    }
}
