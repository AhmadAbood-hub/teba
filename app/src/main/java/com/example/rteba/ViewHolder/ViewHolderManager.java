package com.example.rteba.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tebavet.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewHolderManager extends RecyclerView.ViewHolder
{
    private View view;
            private TextView txtManagerName;
    private TextView txtManagerNumber;
    private CircleImageView imgManagerDelete;


    public ViewHolderManager(@NonNull View view)
    {
        super(view);

        this.view = view;

        txtManagerName = (TextView) view.findViewById(R.id.txtManagerName);
        txtManagerNumber = (TextView) view.findViewById(R.id.txtManagerNumber);
        imgManagerDelete = (CircleImageView)view.findViewById(R.id.imgManagerDelete);
    }

    public View getView()
    {
        return view;
    }

    public TextView getTxtManagerName()
    {
        return txtManagerName;
    }

    public TextView getTxtManagerNumber()
    {
        return txtManagerNumber;
    }

    public CircleImageView getImgManagerDelete()
    {
        return imgManagerDelete;
    }
}
