package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.BillSaleDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.BillsSale;

import java.util.List;

public class RepositoryBillSale {
    private BillSaleDao billSaleDao;
    private LiveData<List<BillsSale>> billSale;
    private LiveData<List<BillsSale>> AllbillSale;
    private static MutableLiveData<Integer> resultBillAdded = new MutableLiveData<>();

    public RepositoryBillSale(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        billSaleDao = db.getBillSaleDao();
    }

    public void insert(BillsSale billsSale) {
        new TaskInsert(billSaleDao).execute(billsSale);
    }

    public void delete(BillsSale billsSale) {
        new TaskDelete(billSaleDao).execute(billsSale);
    }

    public void update(BillsSale billsSale) {
        new TaskUpdate(billSaleDao).execute(billsSale);
    }

    public LiveData<List<BillsSale>> getBillSale(int CustmerID)
    {
        billSale = billSaleDao.getBillsSale(CustmerID);
        return billSale;
    }


    public LiveData<List<BillsSale>> getAllBillsSale()
    {
        AllbillSale = billSaleDao.getAllBillsSale();
        return AllbillSale;
    }



    public MutableLiveData<Integer> getResultBillAdded()
    {
        return resultBillAdded;
    }

    private static class TaskInsert extends AsyncTask<BillsSale, Void, Void>
    {
        private BillSaleDao billSaleDao;

        private int maxID = 0;

        public TaskInsert(BillSaleDao billSaleDao)
        {
            this.billSaleDao = billSaleDao;
        }

        @Override
        protected void onPreExecute() {
            resultBillAdded.postValue(0);
        }

        @Override
        protected Void doInBackground(BillsSale... billsSales) {
            billSaleDao.insert(billsSales[0]);

            maxID = billSaleDao.getMaxBillSaleID();

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid)
        {
            resultBillAdded.postValue(maxID);
        }
    }

        private static class TaskDelete extends AsyncTask<BillsSale, Void, Void>
        {
            private BillSaleDao billSaleDao;

            public TaskDelete(BillSaleDao billSaleDao) {
                this.billSaleDao = billSaleDao;
            }

            @Override
            protected Void doInBackground(BillsSale... billsSale) {
                billSaleDao.delete(billsSale[0]);
                return null;
            }
        }

        private static class TaskUpdate extends AsyncTask<BillsSale, Void, Void>
        {
            private BillSaleDao billSaleDao;

            public TaskUpdate(BillSaleDao billSaleDao) {
                this.billSaleDao = billSaleDao;
            }

            @Override
            protected Void doInBackground(BillsSale... billsSale) {
                billSaleDao.update(billsSale[0]);
                return null;
            }
        }}

