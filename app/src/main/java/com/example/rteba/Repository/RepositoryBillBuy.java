package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.BillBuyDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.BillsBuy;

import java.util.List;

public class RepositoryBillBuy
{
    private BillBuyDao billBuyDao;
    private LiveData<List<BillsBuy>> billBuy;
    public RepositoryBillBuy(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        billBuyDao = db.getBillBuyDao();
        billBuy = billBuyDao.getBillsBuy ();
    }
    public void insert(BillsBuy billsBuy)
    {
        new TaskInsert(billBuyDao).execute(billsBuy);
    }
    public void delete(BillsBuy billsBuy)
    {
        new TaskDelete(billBuyDao).execute(billsBuy);
    }
    public void update(BillsBuy billsBuy)
    {
        new TaskUpdate(billBuyDao).execute(billsBuy);
    }
    public LiveData<List<BillsBuy>> getBillBuy()
    {

        return billBuy;
    }

    private static class TaskInsert extends AsyncTask<BillsBuy, Void, Void>
    {
        private BillBuyDao billBuyDao;

        public TaskInsert(BillBuyDao billBuyDao)
        {
            this.billBuyDao = billBuyDao;
        }
        @Override
        protected Void doInBackground(BillsBuy... billsBuy) {
            billBuyDao.insert(billsBuy[0]);
            return null;
        }
    }

    private static class TaskDelete extends AsyncTask<BillsBuy, Void, Void>
    {
        private BillBuyDao billBuyDao;

        public TaskDelete(BillBuyDao billBuyDao)
        {
            this.billBuyDao = billBuyDao;
        }
        @Override
        protected Void doInBackground(BillsBuy... billsBuy) {
            billBuyDao.delete(billsBuy[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<BillsBuy, Void, Void>
    {
        private BillBuyDao billBuyDao;

        public TaskUpdate(BillBuyDao billBuyDao)
        {
            this.billBuyDao = billBuyDao;
        }
        @Override
        protected Void doInBackground(BillsBuy... billsBuy) {
            billBuyDao.update(billsBuy[0]);
            return null;
        }
    }
}
