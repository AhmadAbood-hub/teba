package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.BillSaleItemDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.BillsSale_Items;

import java.util.ArrayList;
import java.util.List;

public class insertTest
{
    private BillSaleItemDao billSaleItemDao;
    private LiveData<List<BillsSale_Items>> billSaleItems;

    public insertTest(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        billSaleItemDao = db.getBillSaleItemDao();
    }


    public void insert(List<BillsSale_Items> billsSale_items)
    {
        new insertTest.TaskInsert(billSaleItemDao).execute(billsSale_items);
    }


    private static class TaskInsert extends AsyncTask<List<BillsSale_Items>, Void, Void>
    {
        private BillSaleItemDao billSaleItemDao;

        public TaskInsert(BillSaleItemDao billSaleItemDao)
        {
            this.billSaleItemDao = billSaleItemDao;
        }

        @Override
        protected Void doInBackground(List<BillsSale_Items>... billItemsList)
        {
            List<BillsSale_Items> billsSale_items = billItemsList[0];

            for(BillsSale_Items billItem : billsSale_items)
            {
                this.billSaleItemDao.insert(billItem);
            }

            return null;
        }
    }





}
