package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.BillBuyItemDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.BillsBuy_Items;

import java.util.List;

public class RepositoryBillBuyItem
{
    private BillBuyItemDao billBuyItemDao;
    private LiveData<List<BillsBuy_Items>> billBuyItems;

    public RepositoryBillBuyItem(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        billBuyItemDao = db.getBillBuyItemDao();
    }

    public void insert(BillsBuy_Items billsBuy_items)
    {
        new TaskInsert(billBuyItemDao).execute(billsBuy_items);
    }

    public void delete(BillsBuy_Items billsBuyItems)
    {
       new TaskDelete(billBuyItemDao).execute(billsBuyItems);
    }

    public void update(BillsBuy_Items billsBuyItems)
    {
        new TaskUpdate(billBuyItemDao).execute(billsBuyItems);
    }

    public LiveData<List<BillsBuy_Items>> getBillsBuy_Items()
    {
        billBuyItems = billBuyItemDao.getBillsBuy_Items();
        return billBuyItems;
    }

    private static class TaskInsert extends AsyncTask<BillsBuy_Items, Void, Void>
    {
        private BillBuyItemDao billBuyItemDao;

        public TaskInsert(BillBuyItemDao billBuyItemDao)
        {
            this.billBuyItemDao = billBuyItemDao;
        }
        @Override
        protected Void doInBackground(BillsBuy_Items... billsBuy_items) {
            billBuyItemDao.insert(billsBuy_items[0]);
            return null;
        }
    }

    private static class TaskDelete extends AsyncTask<BillsBuy_Items, Void, Void>
    {
        private BillBuyItemDao billBuyItemDao;

        public TaskDelete(BillBuyItemDao billBuyItemDao)
        {
            this.billBuyItemDao = billBuyItemDao;
        }


        @Override
        protected Void doInBackground(BillsBuy_Items... billsBuy_items) {
            billBuyItemDao.delete(billsBuy_items[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<BillsBuy_Items, Void, Void>
    {
        private BillBuyItemDao billBuyItemDao;

        public TaskUpdate(BillBuyItemDao billBuyItemDao)
        {
            this.billBuyItemDao = billBuyItemDao;
        }


        @Override
        protected Void doInBackground(BillsBuy_Items... billsBuy_Items)
        {
            billBuyItemDao.update(billsBuy_Items[0]);
            return null;
        }
    }
}


