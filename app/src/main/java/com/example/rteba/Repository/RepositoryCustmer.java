package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.CustmerDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Custmers;

import java.util.List;

public class RepositoryCustmer
{
    private CustmerDao CustmerDao;
    private LiveData<List<Custmers>> Custmer;
    private static MutableLiveData<Integer> resultID = new MutableLiveData<>();

    public static MutableLiveData<Integer> getResultID()
    {
        return resultID;
    }

    public RepositoryCustmer(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        CustmerDao = db.getCustmersDao();
        Custmer = CustmerDao.getCustmers ();
    }
    public void insert(Custmers Custmers)
    {
        new TaskInsert(CustmerDao).execute(Custmers);
    }
    public void delete(Custmers Custmers)
    {
        new TaskDelete(CustmerDao).execute(Custmers);
    }
    public void update(Custmers Custmers)
    {
        new TaskUpdate(CustmerDao).execute(Custmers);
    }
    public LiveData<List<Custmers>> getCustmer()
    {

        return Custmer;
    }

    public LiveData<List<String>> getNameCustmer()
    {

        return CustmerDao.getNameCustmers();
    }


    public LiveData<String> getNameCustmersFromID(int idCustmer)
    {
        return CustmerDao.getNameCustmersFromID(idCustmer);
    }

    public LiveData<Integer> getIDCustmers(String NameCustmer)
    {

        return CustmerDao.getIDCustmers(NameCustmer);
    }

    private static class TaskInsert extends AsyncTask<Custmers, Void, Void>
    {
        private CustmerDao CustmerDao;

        public TaskInsert(CustmerDao CustmerDao)
        {
            this.CustmerDao = CustmerDao;
        }
        @Override
        protected Void doInBackground(Custmers... Custmers)
        {
            CustmerDao.insert(Custmers[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            resultID.postValue(1);
        }
    }

    private static class TaskDelete extends AsyncTask<Custmers, Void, Void>
    {
        private CustmerDao CustmerDao;

        public TaskDelete(CustmerDao CustmerDao)
        {
            this.CustmerDao = CustmerDao;
        }
        @Override
        protected Void doInBackground(Custmers... Custmers) {
            CustmerDao.delete(Custmers[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<Custmers, Void, Void>
    {
        private CustmerDao CustmerDao;

        public TaskUpdate(CustmerDao CustmerDao)
        {
            this.CustmerDao = CustmerDao;
        }
        @Override
        protected Void doInBackground(Custmers... Custmers) {
            CustmerDao.update(Custmers[0]);
            return null;
        }


    }
}
