package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.JobDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Jobs;

import java.util.List;

public class RepositoryJob
{
    private JobDao JobDao;
    private LiveData<List<Jobs>> Job;
    private LiveData<Integer> ID;
    private LiveData<List<String>> Name;


    public RepositoryJob(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        JobDao = db.getJobsDao();
        Job = JobDao.getJobs ();
        Name =JobDao.getName();

    }
    public void insert(Jobs Jobs)
    {
        new TaskInsert(JobDao).execute(Jobs);
    }
    public void delete(Jobs Jobs)
    {
        new TaskDelete(JobDao).execute(Jobs);
    }
    public void update(Jobs Jobs)
    {
        new TaskUpdate(JobDao).execute(Jobs);
    }
    public LiveData<List<Jobs>> getJob()
    {

        return Job;
    }

    public LiveData<Integer> getID(String Name)
    {
      return   JobDao.getID(Name);

    }

    public LiveData<List<String>> getName()
    {
        return Name;
    }
    public LiveData<String> getNameID(int ID)
    {
        return JobDao.getNameID(ID);
    }

    private static class TaskInsert extends AsyncTask<Jobs, Void, Void>
    {
        private JobDao JobDao;

        public TaskInsert(JobDao JobDao)
        {
            this.JobDao = JobDao;
        }
        @Override
        protected Void doInBackground(Jobs... Jobs) {
            JobDao.insert(Jobs[0]);
            return null;
        }
    }

    private static class TaskDelete extends AsyncTask<Jobs, Void, Void>
    {
        private JobDao JobDao;

        public TaskDelete(JobDao JobDao)
        {
            this.JobDao = JobDao;
        }
        @Override
        protected Void doInBackground(Jobs... Jobs) {
            JobDao.delete(Jobs[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<Jobs, Void, Void>
    {
        private JobDao JobDao;

        public TaskUpdate(JobDao JobDao)
        {
            this.JobDao = JobDao;
        }
        @Override
        protected Void doInBackground(Jobs... Jobs) {
            JobDao.update(Jobs[0]);
            return null;
        }
    }
}
