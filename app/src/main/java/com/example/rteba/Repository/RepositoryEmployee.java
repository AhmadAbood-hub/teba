package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.EmployeeDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Employees;

import java.util.List;

public class RepositoryEmployee
{
    private EmployeeDao EmployeeDao;
    private LiveData<List<Employees>> Employee;
    private LiveData<List<Employees>> Manager;
    private LiveData<List<String>> managerName ;
    private LiveData<Integer> ID;

    private static MutableLiveData<Integer> resultID = new MutableLiveData<>();
    public RepositoryEmployee(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        EmployeeDao = db.getEmployeesDao();
        Employee = EmployeeDao.getEmployees ();
        Manager =EmployeeDao.getManagers();
        managerName =EmployeeDao.getNameManager();
    }
    public void insert(Employees Employees)
    {
        new TaskInsert(EmployeeDao).execute(Employees);
    }
    public void delete(Employees Employees)
    {
        new TaskDelete(EmployeeDao).execute(Employees);
    }
    public void update(Employees Employees)
    {
        new TaskUpdate(EmployeeDao).execute(Employees);
    }
    public LiveData<List<Employees>> getEmployee()
    {

        return Employee;
    }
    public LiveData<Integer> getID(String Name)
    {
        return   EmployeeDao.getID(Name);

    }
    public LiveData<String> getNameID(int ID)
    {
        return EmployeeDao.getNameID(ID);
    }
    public LiveData<List<String>> getNameManager()
    {

        return managerName;
    }

    public LiveData<List<Employees>> getManager()
    {
        return Manager;
    }

    private static class TaskInsert extends AsyncTask<Employees, Void, Void>
    {
        private EmployeeDao EmployeeDao;

        public TaskInsert(EmployeeDao EmployeeDao)
        {
            this.EmployeeDao = EmployeeDao;
        }
        @Override
        protected Void doInBackground(Employees... Employees) {
            EmployeeDao.insert(Employees[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            resultID.postValue(1);

        }
    }
    public MutableLiveData<Integer> getResultID()
    {
        return resultID;
    }
    private static class TaskDelete extends AsyncTask<Employees, Void, Void>
    {
        private EmployeeDao EmployeeDao;

        public TaskDelete(EmployeeDao EmployeeDao)
        {
            this.EmployeeDao = EmployeeDao;
        }
        @Override
        protected Void doInBackground(Employees... Employees) {
            EmployeeDao.delete(Employees[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<Employees, Void, Void>
    {
        private EmployeeDao EmployeeDao;

        public TaskUpdate(EmployeeDao EmployeeDao)
        {
            this.EmployeeDao = EmployeeDao;
        }
        @Override
        protected Void doInBackground(Employees... Employees) {
            EmployeeDao.update(Employees[0]);
            return null;
        }
    }
}
