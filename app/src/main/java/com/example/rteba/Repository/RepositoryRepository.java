package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.RepositoryDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Repositories;

import java.util.List;

public class RepositoryRepository
{
    private RepositoryDao RepositoryDao;
    private LiveData<List<Repositories>> Repository;
    public RepositoryRepository(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        RepositoryDao = db.getRepositoryDao();
        Repository = RepositoryDao.getRepository ();
    }
    public void insert(Repositories Repository)
    {
        new TaskInsert(RepositoryDao).execute(Repository);
    }
    public void delete(Repositories Repository)
    {
        new TaskDelete(RepositoryDao).execute(Repository);
    }
    public void update(Repositories Repository)
    {
        new TaskUpdate(RepositoryDao).execute(Repository);
    }

    public LiveData<List<Repositories>> getRepository()
    {

        return Repository;
    }

    public LiveData<List<String>> getLocation()
    {

        return RepositoryDao.getLocatin();
    }
    public LiveData<String> getOneLocation(int ID)
    {

        return RepositoryDao.getOneLocatin(ID);
    }




    public LiveData<Integer> getID(String Location)
    {
        return RepositoryDao.getID(Location);
    }




    private static class TaskInsert extends AsyncTask<Repositories, Void, Void>
    {
        private RepositoryDao RepositoryDao;

        public TaskInsert(RepositoryDao RepositoryDao)
        {
            this.RepositoryDao = RepositoryDao;
        }
        @Override
        protected Void doInBackground(Repositories... Repository) {
            RepositoryDao.insert(Repository[0]);
            return null;
        }
    }

    private static class TaskDelete extends AsyncTask<Repositories, Void, Void>
    {
        private RepositoryDao RepositoryDao;

        public TaskDelete(RepositoryDao RepositoryDao)
        {
            this.RepositoryDao = RepositoryDao;
        }
        @Override
        protected Void doInBackground(Repositories... Repository) {
            RepositoryDao.delete(Repository[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<Repositories, Void, Void>
    {
        private RepositoryDao RepositoryDao;

        public TaskUpdate(RepositoryDao RepositoryDao)
        {
            this.RepositoryDao = RepositoryDao;
        }
        @Override
        protected Void doInBackground(Repositories... Repository) {
            RepositoryDao.update(Repository[0]);
            return null;
        }
    }
}
