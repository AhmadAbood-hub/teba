package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.CategoryDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Categories;

import java.util.List;

public class RepositoryCategory
{
    private CategoryDao categoriesDao;
    private LiveData<List<Categories>> Category ;
    public RepositoryCategory(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        categoriesDao = db.getCategoriesDao();
        Category = categoriesDao.getCategories ();
    }

    public void insert(Categories categories)
    {
        new TaskInsert(categoriesDao).execute(categories);
    }
    public void delete(Categories categories)
    {
        new TaskDelete(categoriesDao).execute(categories);
    }
    public void update(Categories categories)
    {
        new TaskUpdate(categoriesDao).execute(categories);
    }




    public LiveData<List<Categories>> getCategory()
    {

        return Category;
    }

    public LiveData<List<String>> getName()
    {

        return categoriesDao.getName();
    }

    public LiveData<String> getOneName(String ID)
    {

        return categoriesDao.getOneName(ID);
    }



    public LiveData<Integer> getID(String Name)
    {

        return categoriesDao.getID(Name);
    }



    private static class TaskInsert extends AsyncTask<Categories, Void, Void>
    {
        private CategoryDao categoriesDao;

        public TaskInsert(CategoryDao categoriesDao)
        {
            this.categoriesDao = categoriesDao;
        }
        @Override
        protected Void doInBackground(Categories... categories) {
            categoriesDao.insert(categories[0]);
            return null;
        }
    }
    private static class TaskDelete extends AsyncTask<Categories, Void, Void>
    {
        private CategoryDao categoriesDao;

        public TaskDelete(CategoryDao categoriesDao)
        {
            this.categoriesDao = categoriesDao;
        }
        @Override
        protected Void doInBackground(Categories... categories) {
            categoriesDao.delete(categories[0]);
            return null;
        }
    }
    private static class TaskUpdate extends AsyncTask<Categories, Void, Void>
    {
        private CategoryDao categoriesDao;

        public TaskUpdate(CategoryDao categoriesDao)
        {
            this.categoriesDao = categoriesDao;
        }
        @Override
        protected Void doInBackground(Categories... categories) {
            categoriesDao.update(categories[0]);
            return null;
        }
    }
}
