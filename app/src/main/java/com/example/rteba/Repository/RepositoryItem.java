package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.ItemDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Items;

import java.util.List;

public class RepositoryItem
{
    private ItemDao ItemDao;

    public RepositoryItem(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        ItemDao = db.getItemsDao();

    }
    public void insert(Items Items)
    {
        new TaskInsert(ItemDao).execute(Items);
    }
    public void delete(Items Items)
    {
        new TaskDelete(ItemDao).execute(Items);
    }
    public void update(Items Items)
    {
        new TaskUpdate(ItemDao).execute(Items);
    }
    public LiveData<List<Items>> getItems(int CompanyID)
    {

        return ItemDao.getItems(CompanyID);
    }

    public LiveData<List<Items>> getFavItems()
    {

        return ItemDao.getFavItems();
    }




    public LiveData<Items> getItemFromID(int itemID)
    {

        return ItemDao.getItemFromID(itemID);
    }

    public LiveData<List<Items>> getItem()
    {
        return ItemDao.getItem();
    }

    private static class TaskInsert extends AsyncTask<Items, Void, Void>
    {
        private ItemDao ItemDao;

        public TaskInsert(ItemDao ItemDao)
        {
            this.ItemDao = ItemDao;
        }
        @Override
        protected Void doInBackground(Items... Items)
        {
            ItemDao.insert(Items[0]);
            return null;
        }
    }

    private static class TaskDelete extends AsyncTask<Items, Void, Void>
    {
        private ItemDao ItemDao;

        public TaskDelete(ItemDao ItemDao)
        {
            this.ItemDao = ItemDao;
        }
        @Override
        protected Void doInBackground(Items... Items) {
            ItemDao.delete(Items[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<Items, Void, Void>
    {
        private ItemDao ItemDao;

        public TaskUpdate(ItemDao ItemDao)
        {
            this.ItemDao = ItemDao;
        }
        @Override
        protected Void doInBackground(Items... Items) {
            ItemDao.update(Items[0]);
            return null;
        }
    }
}
