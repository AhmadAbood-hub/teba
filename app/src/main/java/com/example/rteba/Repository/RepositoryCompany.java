package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.CompanyDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Companies;

import java.util.List;

public class RepositoryCompany
{
    private CompanyDao companyDao;
    private LiveData<List<Companies>> Company ;
    public RepositoryCompany(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        companyDao = db.getCompaniesDao();
        Company = companyDao.getCompanies();
    }

    public void insert(Companies companies)
    {
        new TaskInsert(companyDao).execute(companies);
    }
    public void delete(Companies companies)
    {
        new TaskDelete(companyDao).execute(companies);
    }
    public void update(Companies companies)
    {
        new TaskUpdate(companyDao).execute(companies);
    }
    public LiveData<List<Companies>> getCompany()
    {
        return Company;
    }
    public LiveData<Companies> getOneCompany(int ID)
    {
        return companyDao.getOneCompany(ID);
    }

    private static class TaskInsert extends AsyncTask<Companies, Void, Void>
    {
        private CompanyDao companyDao;

        public TaskInsert(CompanyDao companyDao)
        {
            this.companyDao = companyDao;
        }
        @Override
        protected Void doInBackground(Companies... companies) {
            companyDao.insert(companies[0]);
            return null;
        }
    }
    private static class TaskDelete extends AsyncTask<Companies, Void, Void>
    {
        private CompanyDao companyDao;

        public TaskDelete(CompanyDao companyDao)
        {
            this.companyDao = companyDao;
        }
        @Override
        protected Void doInBackground(Companies... companies) {
            companyDao.delete(companies[0]);
            return null;
        }
    }
    private static class TaskUpdate extends AsyncTask<Companies, Void, Void>
    {
        private CompanyDao companyDao;

        public TaskUpdate(CompanyDao companyDao)
        {
            this.companyDao = companyDao;
        }
        @Override
        protected Void doInBackground(Companies... companies) {
            companyDao.update(companies[0]);
            return null;
        }
    }

}
