package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.TypeDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Types;

import java.util.List;

public class RepositoryType
{
    private TypeDao TypeDao;
    private LiveData<List<Types>> Type;
    public RepositoryType(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        TypeDao = db.getTypesDao();
        Type = TypeDao.getTypes ();
    }
    public void insert(Types Type)
    {
        new TaskInsert(TypeDao).execute(Type);
    }
    public void delete(Types Type)
    {
        new TaskDelete(TypeDao).execute(Type);
    }
    public void update(Types Type)
    {
        new TaskUpdate(TypeDao).execute(Type);
    }

    public LiveData<List<String>> getName()
    {

        return TypeDao.getName();
    }

    public LiveData<Integer> getID(String Name)
    {

        return TypeDao.getID(Name);
    }

    public LiveData<String> getOneName(String ID)
    {

        return TypeDao.getOneName(ID);
    }



    public LiveData<List<Types>> getTypes()
    {

        return Type;
    }

    private static class TaskInsert extends AsyncTask<Types, Void, Void>
    {
        private TypeDao TypeDao;

        public TaskInsert(TypeDao TypeDao)
        {
            this.TypeDao = TypeDao;
        }
        @Override
        protected Void doInBackground(Types... Type) {
            TypeDao.insert(Type[0]);
            return null;
        }
    }

    private static class TaskDelete extends AsyncTask<Types, Void, Void>
    {
        private TypeDao TypeDao;

        public TaskDelete(TypeDao TypeDao)
        {
            this.TypeDao = TypeDao;
        }
        @Override
        protected Void doInBackground(Types... Type) {
            TypeDao.delete(Type[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<Types, Void, Void>
    {
        private TypeDao TypeDao;

        public TaskUpdate(TypeDao TypeDao)
        {
            this.TypeDao = TypeDao;
        }
        @Override
        protected Void doInBackground(Types... Type) {
            TypeDao.update(Type[0]);
            return null;
        }
    }
}
