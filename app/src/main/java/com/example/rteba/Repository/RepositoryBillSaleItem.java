package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.BillSaleItemDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.BillsSale_Items;

import java.util.ArrayList;
import java.util.List;

public class RepositoryBillSaleItem
{
    private BillSaleItemDao billSaleItemDao;
    private LiveData<List<BillsSale_Items>> billSaleItems;

    public RepositoryBillSaleItem(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        billSaleItemDao = db.getBillSaleItemDao();
    }

    public void insert(BillsSale_Items billsSale_items)
    {
        new TaskInsert(billSaleItemDao).execute(billsSale_items);
    }

    public void delete(BillsSale_Items billsSaleItems)
    {
       new TaskDelete(billSaleItemDao).execute(billsSaleItems);
    }

    public void update(BillsSale_Items billsSaleItems)
    {
        new TaskUpdate(billSaleItemDao).execute(billsSaleItems);
    }

    public LiveData<List<BillsSale_Items>> getBillsSaleItems(int IDBillSale)
    {
        billSaleItems = billSaleItemDao.getBillsSaleItems(IDBillSale);
        return billSaleItems;
    }

    private static class TaskInsert extends AsyncTask<BillsSale_Items, Void, Void>
    {
        private BillSaleItemDao billSaleItemDao;

        public TaskInsert(BillSaleItemDao billSaleItemDao)
        {
            this.billSaleItemDao = billSaleItemDao;
        }

        @Override
        protected Void doInBackground(BillsSale_Items... billItemsList)
        {

                billSaleItemDao.insert(billItemsList[0]);
                return null;
        }
    }

    private static class TaskDelete extends AsyncTask<BillsSale_Items, Void, Void>
    {
        private BillSaleItemDao billSaleItemDao;

        public TaskDelete(BillSaleItemDao billItemDao)
        {
            this.billSaleItemDao = billItemDao;
        }


        @Override
        protected Void doInBackground(BillsSale_Items... billsSale_items)
        {
            billSaleItemDao.delete(billsSale_items[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<BillsSale_Items, Void, Void>
    {
        private BillSaleItemDao billItemDao;

        public TaskUpdate(BillSaleItemDao billItemDao)
        {
            this.billItemDao = billItemDao;
        }


        @Override
        protected Void doInBackground(BillsSale_Items... billsSale_items)
        {
            billItemDao.update(billsSale_items[0]);
            return null;
        }
    }
}


