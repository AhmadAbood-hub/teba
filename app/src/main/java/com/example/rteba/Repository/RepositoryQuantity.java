package com.example.rteba.Repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.rteba.Model.Dao.QuantityDao;
import com.example.rteba.Model.Database.RoomDatabaseTebaVet;
import com.example.rteba.Model.Entity.Quantities;

import java.util.List;

public class RepositoryQuantity
{
    private QuantityDao QuantityDao;
    private LiveData<List<Quantities>> Quantity;
    public RepositoryQuantity(Application application)
    {
        RoomDatabaseTebaVet db = RoomDatabaseTebaVet.getDatabase(application);
        QuantityDao = db.getQuantityDao();
        Quantity = QuantityDao.getQuantity ();
    }
    public void insert(Quantities Quantity)
    {
        new TaskInsert(QuantityDao).execute(Quantity);
    }
    public void delete(Quantities Quantity)
    {
        new TaskDelete(QuantityDao).execute(Quantity);
    }
    public void update(Quantities Quantity)
    {
        new TaskUpdate(QuantityDao).execute(Quantity);
    }
    public LiveData<List<Quantities>> getQuantity()
    {

        return Quantity;
    }


    public LiveData<Integer> getID(int size , String unit)
    {

      return   QuantityDao.getID(size,unit);

    }



    public LiveData<List<Integer>> getSize()
    {
      return  QuantityDao.getSize() ;
    }
    public LiveData<Integer> getOneSize(String ID)

    {
        return  QuantityDao.getOneSize(ID) ;
    }

    public LiveData<List<String>> getUnit()
    {
        return  QuantityDao.getUnit() ;
    }

    public LiveData<String> getOneUnit(String ID)
    {
        return  QuantityDao.getOneUnit(ID) ;
    }





    private static class TaskInsert extends AsyncTask<Quantities, Void, Void>
    {
        private QuantityDao QuantityDao;

        public TaskInsert(QuantityDao QuantityDao)
        {
            this.QuantityDao = QuantityDao;
        }
        @Override
        protected Void doInBackground(Quantities... Quantity) {
            QuantityDao.insert(Quantity[0]);
            return null;
        }
    }

    private static class TaskDelete extends AsyncTask<Quantities, Void, Void>
    {
        private QuantityDao QuantityDao;

        public TaskDelete(QuantityDao QuantityDao)
        {
            this.QuantityDao = QuantityDao;
        }
        @Override
        protected Void doInBackground(Quantities... Quantity) {
            QuantityDao.delete(Quantity[0]);
            return null;
        }
    }

    private static class TaskUpdate extends AsyncTask<Quantities, Void, Void>
    {
        private QuantityDao QuantityDao;

        public TaskUpdate(QuantityDao QuantityDao)
        {
            this.QuantityDao = QuantityDao;
        }
        @Override
        protected Void doInBackground(Quantities... Quantity) {
            QuantityDao.update(Quantity[0]);
            return null;
        }
    }
}
