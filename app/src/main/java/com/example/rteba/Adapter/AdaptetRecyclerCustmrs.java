package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Custmers;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.CustmersActivity;
import com.example.rteba.ViewHolder.ViewHolderCustmer;

import java.util.List;

public class AdaptetRecyclerCustmrs extends RecyclerView.Adapter<ViewHolderCustmer>
{
    private Context context;
    private List<Custmers> custmers;

    public AdaptetRecyclerCustmrs(Context context, List<Custmers> custmers)
    {
        this.context = context;
        this.custmers = custmers;
    }


    @NonNull
    @Override
    public ViewHolderCustmer onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_custmer, viewGroup, false);
        return new ViewHolderCustmer(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCustmer viewHolderCustmer, int position)
    {
        final Custmers custmer = custmers.get(position);

        viewHolderCustmer.getTxtCustmerName().setText(custmer.getNameCustmer());
       viewHolderCustmer.getTxtCustmerNumber().setText(custmer.getNumberCustmer());

        viewHolderCustmer.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((CustmersActivity) context).showData(custmer);
                return true;
            }
        });


        viewHolderCustmer.getImgCustmerDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((CustmersActivity) context).delete(custmer);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return custmers.size();
    }

    public void setData(List<Custmers> custmers)
    {
        this.custmers = custmers;
        notifyDataSetChanged();
    }
}
