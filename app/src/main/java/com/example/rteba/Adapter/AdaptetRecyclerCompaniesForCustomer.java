package com.example.rteba.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.rteba.Model.Entity.Companies;
import com.example.rteba.View.Activity.CategoriesForCustomer;
import com.example.rteba.View.Activity.CompaniesActivity;
import com.example.rteba.ViewHolder.ViewHolderCompanyForCustomer;
import com.example.tebavet.R;

import java.util.List;

public class AdaptetRecyclerCompaniesForCustomer extends RecyclerView.Adapter<ViewHolderCompanyForCustomer>
{
private Context context;
private List<Companies> companies;







public interface OnItemClickListener
{
    void onItemClick(int position);
}
    public AdaptetRecyclerCompaniesForCustomer(Context context, List<Companies> brands)
    {
        this.context = context;
        this.companies = brands;
    }


    @NonNull
    @Override
    public ViewHolderCompanyForCustomer onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_company_for_customer, viewGroup, false);
        return new ViewHolderCompanyForCustomer(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCompanyForCustomer ViewHolderCompany, int position)
    {
        final Companies Companies = companies.get(position);

        ViewHolderCompany.getTxtCompanyName().setText(Companies.getCompanyName());
        ViewHolderCompany.getTxtCompanyLocation().setText(Companies.getLocation());
        if(Companies.getImage()==null)
            Glide.with(context).load(Companies.getLogo()).into(ViewHolderCompany.getImgCompanyLogo());
        else
            Glide.with(context).load(Companies.getImage()).into(ViewHolderCompany.getImgCompanyLogo());



        ViewHolderCompany.getView().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                CompaniesActivity.selectedCompany =Companies;
                Intent intent = new Intent(context, CategoriesForCustomer.class);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount()
    {
        return companies.size();
    }

    public void setData(List<Companies> companies)
    {
        this.companies = companies;
        notifyDataSetChanged();
    }
}
