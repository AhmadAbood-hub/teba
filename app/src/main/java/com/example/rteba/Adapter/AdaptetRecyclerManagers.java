package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Employees;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.EmpActivity;
import com.example.rteba.ViewHolder.ViewHolderManager;

import java.util.List;

public class AdaptetRecyclerManagers extends RecyclerView.Adapter<ViewHolderManager>
{
    private Context context;
    private List<Employees> managers;

    public AdaptetRecyclerManagers(Context context, List<Employees> managers)
    {
        this.context = context;
        this.managers = managers;
    }


    @NonNull
    @Override
    public ViewHolderManager onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_manager, viewGroup, false);
        return new ViewHolderManager(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderManager viewHolderManager, int position)
    {
        final Employees manager = managers.get(position);

        viewHolderManager.getTxtManagerName().setText("Name: "+manager.getName());
        viewHolderManager.getTxtManagerNumber().setText("Number: "+manager.getNumber());

        viewHolderManager.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((EmpActivity) context).showDataManager(manager);
                return true;
            }
        });


        viewHolderManager.getImgManagerDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((EmpActivity) context).deleteManager(manager);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return managers.size();
    }

    public void setData(List<Employees> managers)
    {
        this.managers = managers;
        notifyDataSetChanged();
    }
}
