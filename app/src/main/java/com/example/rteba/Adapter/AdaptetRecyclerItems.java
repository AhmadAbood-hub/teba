package com.example.rteba.Adapter;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.ViewModel.ViewModelItem;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.ItemsActivity;
import com.example.rteba.ViewHolder.ViewHolderItem;

import java.util.List;

public class AdaptetRecyclerItems extends RecyclerView.Adapter<ViewHolderItem>
{
    private Context context;
    private List<Items> items;
    public  static int ItemApiID;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }
    public AdaptetRecyclerItems(Context context, List<Items> items)
    {
        this.context = context;
        this.items = items;
    }


    @NonNull
    @Override
    public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_items, viewGroup, false);
        return new ViewHolderItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderItem viewHolderItem, int position)
    {


        final Items item = items.get(position);


        viewHolderItem.getTxtItemName().setText(item.getItemName());
     ////   byte[] logo = item.getImg();
     //   Bitmap bitmap = BitmapFactory.decodeByteArray(logo, 0, logo.length);
     //   viewHolderItem.getImgItemImage().setImageBitmap(bitmap);

        Glide.with(context).load(item.getUrl()).into( viewHolderItem.getImgItemImage());


        viewHolderItem.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ItemsActivity.SelectItem=item;
                ((ItemsActivity) context).showData(item);
                return true;
            }
        });


        viewHolderItem.getImgItemDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ItemApiID =item.getItemApiID();

                ((ItemsActivity) context).delete(item);

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    public void setData(List<Items> items)
    {
        this.items = items;
        notifyDataSetChanged();
    }
}
