package com.example.rteba.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Categories;
import com.example.rteba.View.Activity.ItemsActivity;
import com.example.rteba.View.Activity.TypesForCustomer;
import com.example.rteba.ViewHolder.ViewHolderCategoryForCustomer;
import com.example.tebavet.R;

import java.util.List;

import static com.example.rteba.View.Activity.QuaTypCatActivity.selectedCategory;

public class AdaptetRecyclerCategoriesForCustomer extends RecyclerView.Adapter<ViewHolderCategoryForCustomer>
{
    private Context context;
    private List<Categories> categories;


    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }
    public AdaptetRecyclerCategoriesForCustomer(Context context, List<Categories> categories)
    {
        this.context = context;
        this.categories = categories;
    }


    @NonNull
    @Override
    public ViewHolderCategoryForCustomer onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_category_for_customer, viewGroup, false);
        return new ViewHolderCategoryForCustomer(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCategoryForCustomer viewHolderCategoryForCustomer, int position)
    {


        final Categories category = categories.get(position);


        viewHolderCategoryForCustomer.getTextCategoryNameForCustomer().setText(category.getName());




        viewHolderCategoryForCustomer.getView().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                selectedCategory =category;
                Intent intent = new Intent(context, TypesForCustomer.class);
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount()
    {
        return categories.size();
    }

    public void setData(List<Categories> categories)
    {
        this.categories = categories;
        notifyDataSetChanged();
    }
}
