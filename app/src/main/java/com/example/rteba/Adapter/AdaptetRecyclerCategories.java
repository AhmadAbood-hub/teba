package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Categories;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.QuaTypCatActivity;
import com.example.rteba.ViewHolder.ViewHolderCategory;

import java.util.List;

public class AdaptetRecyclerCategories extends RecyclerView.Adapter<ViewHolderCategory>
{
    private Context context;
    private List<Categories> categories;

    public AdaptetRecyclerCategories(Context context, List<Categories> categories)
    {
        this.context = context;
        this.categories = categories;
    }


    @NonNull
    @Override
    public ViewHolderCategory onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_category, viewGroup, false);
        return new ViewHolderCategory(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCategory viewHolderCategory, int position)
    {
        final Categories Category = categories.get(position);

        viewHolderCategory.getTxtCategoryName().setText("Name: "+Category.getName());

        viewHolderCategory.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((QuaTypCatActivity) context).showDataCategory(Category);
                return true;
            }
        });

        viewHolderCategory.getImgCategoryDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((QuaTypCatActivity) context).deleteCategory(Category);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return categories.size();
    }

    public void setData(List<Categories> categories)
    {
        this.categories = categories;
        notifyDataSetChanged();
    }
}
