package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Repositories;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.RepActivity;
import com.example.rteba.ViewHolder.ViewHolderRepository;

import java.util.List;

public class AdaptetRecyclerRepository extends RecyclerView.Adapter<ViewHolderRepository>
{
    private Context context;
    private List<Repositories> repositories;
    public AdaptetRecyclerRepository(Context context, List<Repositories> repositories)
    {
        this.context = context;
        this.repositories = repositories;
    }


    @NonNull
    @Override
    public ViewHolderRepository onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_repository, viewGroup, false);
        return new ViewHolderRepository(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRepository viewHolderRepository, int position)
    {

        final Repositories repository = repositories.get(position);

        viewHolderRepository.getTxtRepositoryLocation().setText("Location: "+repository.getLocation());
        viewHolderRepository.getTxtRepositoryAnnualRent().setText("AnnualRent: "+repository.getAnnualrent());
        viewHolderRepository.getTxtRepositoryManeger().setText("Maneger:" + repository.getManagerName());
        viewHolderRepository.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((RepActivity) context).showDataRepository(repository);
                return true;
            }
        });


        viewHolderRepository.getImgRepositoryDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((RepActivity) context).deleteRepository(repository);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return repositories.size();
    }

    public void setData(List<Repositories> repositories)
    {
        this.repositories = repositories;
        notifyDataSetChanged();
    }
}
