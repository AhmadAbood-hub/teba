package com.example.rteba.Adapter;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.View.Activity.ItemsForCustomer;

import com.example.rteba.View.Fragment.SearchFragment;
import com.example.rteba.ViewModel.ViewModelItem;
import com.example.tebavet.R;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class MyAdapter extends PagerAdapter
{
    Context context;
    List<Items> itemsList;
    public static List<Items>  Favitems =new ArrayList<>() ;


    public MyAdapter(Context context, List<Items> itemsList)

    {
        this.context = context;
        this.itemsList = itemsList;


    }

    @Override
    public int getCount()
    {
        return itemsList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o)
    {
        return view.equals(o);
    }

    @Override

    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position)
    {


        View view = LayoutInflater.from(context).inflate(R.layout.item_card,container,false);
        ImageView movie_image =(ImageView) view.findViewById(R.id.movie_image);
        TextView Name = (TextView) view.findViewById(R.id.Name);
        TextView Company = (TextView) view.findViewById(R.id.Company);
        TextView Cost = (TextView) view.findViewById(R.id.Cost);

        FloatingActionButton btn_fav = (FloatingActionButton) view.findViewById(R.id.btn_fav);



        Glide.with(context).load(itemsList.get(position).getUrl()).into(movie_image);


        Name.setText(itemsList.get(position).getItemName());

        Company.setText(itemsList.get(position).getCompanyName());

        Cost.setText( itemsList.get(position).getCost()+"");






        btn_fav.setOnClickListener(new View.OnClickListener()
       {
            @Override
            public void onClick(View view)
            {



            if(!Favitems.contains(itemsList.get(position)))
                Favitems.add(itemsList.get(position));

            }
       });



        container.addView(view);
        return view ;
    }
}
