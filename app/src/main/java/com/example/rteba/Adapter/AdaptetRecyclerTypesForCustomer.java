package com.example.rteba.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Types;
import com.example.rteba.View.Activity.ItemsForCustomer;
import com.example.rteba.ViewHolder.ViewHolderTypeForCustomer;
import com.example.tebavet.R;

import java.util.List;

import static com.example.rteba.View.Activity.QuaTypCatActivity.selectedType;

public class AdaptetRecyclerTypesForCustomer extends RecyclerView.Adapter<ViewHolderTypeForCustomer>
{
    private Context context;
    private List<Types> types;


    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }
    public AdaptetRecyclerTypesForCustomer(Context context, List<Types> types)
    {
        this.context = context;
        this.types = types;
    }


    @NonNull
    @Override
    public ViewHolderTypeForCustomer onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_type_for_customer, viewGroup, false);
        return new ViewHolderTypeForCustomer(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderTypeForCustomer viewHolderTypeForCustomer, int position)
    {


        final Types type = types.get(position);


        viewHolderTypeForCustomer.getTextTypeNameForCustomer().setText(type.getName());




        viewHolderTypeForCustomer.getView().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                selectedType = type;
                Intent intent = new Intent(context, ItemsForCustomer.class);
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount()
    {
        return types.size();
    }

    public void setData(List<Types> types)
    {
        this.types = types;
        notifyDataSetChanged();
    }
}
