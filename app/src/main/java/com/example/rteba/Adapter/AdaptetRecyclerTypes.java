package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Types;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.QuaTypCatActivity;
import com.example.rteba.ViewHolder.ViewHolderType;

import java.util.List;

public class AdaptetRecyclerTypes extends RecyclerView.Adapter<ViewHolderType>
{
    private Context context;
    private List<Types> types;

    public AdaptetRecyclerTypes(Context context, List<Types> types)
    {
        this.context = context;
        this.types = types;
    }


    @NonNull
    @Override
    public ViewHolderType onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_type, viewGroup, false);
        return new ViewHolderType(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderType viewHolderType, int position)
    {
        final Types type = types.get(position);

        viewHolderType.getTxtTypeName().setText("Name: "+type.getName());

        viewHolderType.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((QuaTypCatActivity) context).showDataType(type);
                return true;
            }
        });


        viewHolderType.getImgTypeDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((QuaTypCatActivity) context).deleteType(type);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return types.size();
    }

    public void setData(List<Types> types)
    {
        this.types =types ;
        notifyDataSetChanged();
    }
}
