package com.example.rteba.Adapter;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.View.Fragment.BillsFragment;
import com.example.rteba.ViewHolder.ViewHolderBillItem;
import com.example.rteba.Model.Entity.BillsSale_Items;
import com.example.rteba.ViewModel.ViewModelItem;
import com.example.tebavet.R;
import com.example.rteba.ViewHolder.ViewHolderBillItem;

import java.util.List;

public class AdaptetRecyclerBillItem extends RecyclerView.Adapter<ViewHolderBillItem>
{
    private Context context;
    private Fragment fragment;

    private Items item;
    private ViewModelItem viewModelItem;

    private List<BillsSale_Items> billsSale_items;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }
    public AdaptetRecyclerBillItem(Context context, List<BillsSale_Items> billsSale_items, Fragment fragment)
    {
        this.context = context;
        this.fragment = fragment;
        this.billsSale_items = billsSale_items;
    }
    public AdaptetRecyclerBillItem(Context context, List<BillsSale_Items> billsSale_items)
    {
        this.context = context;

        this.billsSale_items = billsSale_items;

    }


    @NonNull
    @Override
    public ViewHolderBillItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_bill_item, viewGroup, false);
        return new ViewHolderBillItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBillItem viewHolderBillItem, int position)
    {

        final BillsSale_Items item = billsSale_items.get(position);

        viewHolderBillItem.getTxtBillItemName().setText("Name: "+ item.getItemName());
        viewHolderBillItem.getTxtBillItemCompany().setText("Company: "+item.getCompanyName() );
        viewHolderBillItem.getTxtBillItemPrice().setText("Cost: "+item.getCost());
        viewHolderBillItem.getTxtBillItemCount().setText("Count: "+item.getBillSaleItemCount());
        Glide.with(context).load(item.getUrl()).into(viewHolderBillItem.getImgBillItemImage());



        viewHolderBillItem.getView().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ((BillsFragment) fragment).ShowDialog(item);
            }
        });



        viewHolderBillItem.getImgBillItemDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ((BillsFragment) fragment).DeleteBillItemDialog(item);
            }
        });

    }

    @Override
    public int getItemCount()
    {
        return billsSale_items.size();
    }

    public void setData(List<BillsSale_Items> billsSale_items)
    {
        this.billsSale_items = billsSale_items;
        notifyDataSetChanged();
    }
}
