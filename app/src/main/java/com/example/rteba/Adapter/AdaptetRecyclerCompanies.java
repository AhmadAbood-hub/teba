package com.example.rteba.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.rteba.Model.Entity.Companies;
import com.example.rteba.ViewModel.ViewModelItem;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.CompaniesActivity;
import com.example.rteba.View.Activity.ItemsActivity;
import com.example.rteba.ViewHolder.ViewHolderCompany;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class AdaptetRecyclerCompanies extends RecyclerView.Adapter<ViewHolderCompany>
{
    private Context context;
    private List<Companies> companies;







    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }
    public AdaptetRecyclerCompanies(Context context, List<Companies> brands)
    {
        this.context = context;
        this.companies = brands;
    }


    @NonNull
    @Override
    public ViewHolderCompany onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_company, viewGroup, false);
        return new ViewHolderCompany(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCompany ViewHolderCompany, int position)
    {
        final Companies Companies = companies.get(position);

        ViewHolderCompany.getTxtCompanyName().setText(Companies.getCompanyName());
        ViewHolderCompany.getTxtCompanyLocation().setText(Companies.getLocation());
        if(Companies.getImage()==null)
        {
            Glide.with(context).load(Companies.getLogo()).into(ViewHolderCompany.getImgCompanyLogo());
            Log.i(TAG, "onBindViewHolder111: "+Companies.getLogo());
        }


        else
        {
            Glide.with(context).load(Companies.getImage()).into(ViewHolderCompany.getImgCompanyLogo());
            Log.i(TAG, "onBindViewHolder222: "+Companies.getImage());
        }



        ViewHolderCompany.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((CompaniesActivity) context).showData(Companies);
                return true;
            }
        });
        ViewHolderCompany.getView().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
            CompaniesActivity.selectedCompany =Companies;
                Intent intent = new Intent(context, ItemsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("CompanyApiID",Companies.getCompanyApiID());
                bundle.putInt("CompanyID",Companies.getCompanyPreID());
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });

        ViewHolderCompany.getImgCompanyDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                ((CompaniesActivity) context).delete(Companies);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return companies.size();
    }

    public void setData(List<Companies> companies)
    {
        this.companies = companies;
        notifyDataSetChanged();
    }
}
