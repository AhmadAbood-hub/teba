package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Employees;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.EmpActivity;
import com.example.rteba.ViewHolder.ViewHolderEmployee;

import java.util.List;

public class AdaptetRecyclerEmployees extends RecyclerView.Adapter<ViewHolderEmployee>
{
    private Context context;
    private List<Employees> employees;
    public AdaptetRecyclerEmployees(Context context, List<Employees> employees)
    {
        this.context = context;
        this.employees = employees;
    }


    @NonNull
    @Override
    public ViewHolderEmployee onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_employee, viewGroup, false);
        return new ViewHolderEmployee(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderEmployee viewHolderEmployee, int position)
    {

        final Employees employee = employees.get(position);

          viewHolderEmployee.getTxtEmployeeName().setText("Name: "+employee.getName());
        viewHolderEmployee.getTxtEmployeeNumber().setText("Number: "+employee.getNumber());
           viewHolderEmployee.getTxtEmployeeJob().setText("Job:" + employee.getJobName());
        viewHolderEmployee.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((EmpActivity) context).showDataEmployee(employee);
                return true;
            }
        });


        viewHolderEmployee.getImgEmployeeDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((EmpActivity) context).deleteEmployee(employee);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return employees.size();
    }

    public void setData(List<Employees> employees)
    {
        this.employees = employees;
        notifyDataSetChanged();
    }
}
