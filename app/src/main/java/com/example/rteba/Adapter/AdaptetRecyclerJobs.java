package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Jobs;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.JobsActivity;
import com.example.rteba.ViewHolder.ViewHolderJob;

import java.util.List;

public class AdaptetRecyclerJobs extends RecyclerView.Adapter<ViewHolderJob>
{
    private Context context;
    private List<Jobs> Jobs;

    public AdaptetRecyclerJobs(Context context, List<Jobs> Jobs)
    {
        this.context = context;
        this.Jobs = Jobs;
    }


    @NonNull
    @Override
    public ViewHolderJob onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_job, viewGroup, false);
        return new ViewHolderJob(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderJob ViewHolderJob, int position)
    {
        final Jobs job = Jobs.get(position);

        ViewHolderJob.getTxtJobName().setText("Name: "+job.getName());
       ViewHolderJob.getTxtJobSalary().setText("Salary: "+job.getSalary());
        ViewHolderJob.getTxtJobHour().setText("Hour: "+job.getHour());
        ViewHolderJob.getTxtJobCostPerHour().setText("CostPerHour: "+job.getCostPerHour());

        ViewHolderJob.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((JobsActivity) context).showData(job);
                return true;
            }
        });


        ViewHolderJob.getImgJobDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((JobsActivity) context).delete(job);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return Jobs.size();
    }

    public void setData(List<Jobs> Jobs)
    {
        this.Jobs = Jobs;
        notifyDataSetChanged();
    }
}
