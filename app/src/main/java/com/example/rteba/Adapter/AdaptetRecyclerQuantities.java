package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Model.Entity.Quantities;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.QuaTypCatActivity;
import com.example.rteba.ViewHolder.ViewHolderQuantity;

import java.util.List;

public class AdaptetRecyclerQuantities extends RecyclerView.Adapter<ViewHolderQuantity>
{
    private Context context;
    private List<Quantities> quantities;

    public AdaptetRecyclerQuantities(Context context, List<Quantities> quantities)
    {
        this.context = context;
        this.quantities = quantities;
    }


    @NonNull
    @Override
    public ViewHolderQuantity onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycle_quantity, viewGroup, false);
        return new ViewHolderQuantity(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderQuantity viewHolderQuantity, int position)
    {
        final Quantities quantity = quantities.get(position);

        viewHolderQuantity.getTxtQuantityQuantity().setText("Quantity: "+quantity.getQuantity());
       viewHolderQuantity.getTxtQuantityUnit().setText("Unit: "+quantity.getUnit());

        viewHolderQuantity.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((QuaTypCatActivity) context).showDataQuantity(quantity);
                return true;
            }
        });


        viewHolderQuantity.getImgQuantityDelete().setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ((QuaTypCatActivity) context).deleteQuantity(quantity);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return quantities.size();
    }

    public void setData(List<Quantities> quantities)
    {
        this.quantities = quantities;
        notifyDataSetChanged();
    }
}
