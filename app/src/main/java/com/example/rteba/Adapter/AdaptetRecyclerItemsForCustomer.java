package com.example.rteba.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.View.Activity.ItemsActivity;
import com.example.rteba.ViewHolder.ViewHolderItem;
import com.example.rteba.ViewHolder.ViewHolderItemForCustomer;
import com.example.tebavet.R;

import java.util.List;

public class AdaptetRecyclerItemsForCustomer  extends RecyclerView.Adapter<ViewHolderItemForCustomer>
{
    private Context context;
    private List<Items> items;
    public  static int ItemApiID;

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }
    public AdaptetRecyclerItemsForCustomer(Context context, List<Items> items)
    {
        this.context = context;
        this.items = items;
    }


    @NonNull
    @Override
    public ViewHolderItemForCustomer onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_items_for_customer, viewGroup, false);
        return new ViewHolderItemForCustomer(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderItemForCustomer viewHolderItemForCustomer, int position)
    {


        final Items item = items.get(position);


        viewHolderItemForCustomer.getTxtItemName().setText(item.getItemName());
        ////   byte[] logo = item.getImg();
        //   Bitmap bitmap = BitmapFactory.decodeByteArray(logo, 0, logo.length);
        //   viewHolderItem.getImgItemImage().setImageBitmap(bitmap);

        Glide.with(context).load(item.getUrl()).into( viewHolderItemForCustomer.getImgItemImage());


        viewHolderItemForCustomer.getView().setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ItemsActivity.SelectItem=item;
                ((ItemsActivity) context).showData(item);
                return true;
            }
        });


    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    public void setData(List<Items> items)
    {
        this.items = items;
        notifyDataSetChanged();
    }
}
