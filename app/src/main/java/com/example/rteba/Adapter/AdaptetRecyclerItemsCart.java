package com.example.rteba.Adapter;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.View.Dialog.DialogEnterCount;

import com.example.rteba.ViewModel.ViewModelItem;
import com.example.tebavet.R;
import com.example.rteba.ViewHolder.ViewHolderItemCart;

import java.util.List;

public class AdaptetRecyclerItemsCart extends RecyclerView.Adapter<ViewHolderItemCart>
{
    private Context context;

    private List<Items> items;


    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }
    public AdaptetRecyclerItemsCart(Context context, List<Items> items)
    {
        this.context = context;

        this.items = items;
    }


    @NonNull
    @Override
    public ViewHolderItemCart onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recycler_favorite_item, viewGroup, false);
        return new ViewHolderItemCart(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderItemCart viewHolderItemCart, int position)
    {

        final Items item = items.get(position);

        viewHolderItemCart.getTxtItemName().setText("Name: "+item.getItemName());

        viewHolderItemCart.getTxtItemCompany().setText("Company: "+item.getCompanyName());

        viewHolderItemCart.getTxtItemPrice().setText("Cost: "+item.getCost()+"");



        Glide.with(context).load(item.getUrl()).into(viewHolderItemCart.getImgItemImage());


    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    public void setData(List<Items> items)
    {
        this.items = items;
        notifyDataSetChanged();
    }
}
