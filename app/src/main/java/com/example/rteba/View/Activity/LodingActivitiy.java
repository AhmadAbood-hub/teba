package com.example.rteba.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.tebavet.R;

public class LodingActivitiy extends AppCompatActivity
{
    private ImageView imgTebaVetImage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loding);
        imgTebaVetImage = (ImageView) findViewById(R.id.imgTebaVetImage);
        Animation myanimation = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        imgTebaVetImage.setAnimation(myanimation);
        final Intent intent = new Intent(this, LoginActivity.class);
        Thread timer = new Thread()
        {
            public void run ()
            {
                try
                {
                 sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally
                {
                startActivity(intent);
                finish();
                }

            }


        };
        timer.start();
    }
}
