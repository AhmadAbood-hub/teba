package com.example.rteba.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.rteba.Adapter.AdaptetRecyclerEmployees;
import com.example.rteba.Adapter.AdaptetRecyclerManagers;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Employees;
import com.example.tebavet.R;
import com.example.rteba.View.Dialog.DialogEmployees;
import com.example.rteba.View.Dialog.DialogManagers;
import com.example.rteba.ViewModel.ViewModelEmployee;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EmpActivity extends AppCompatActivity {
    public int ManagerID;
    public int EmployeeID;


    public JsonPlaceHolderApi jsonPlaceHolderApi;
    public static Employees selectedEmployee;
    public static Employees selectedManager;


    private ViewModelEmployee viewModelEmployee;


    private FloatingActionButton FabAddmanager;
    private FloatingActionButton FabAddEmployee;

    private RecyclerView recyclerManagers;
    private RecyclerView.LayoutManager layoutManagerManagers;
    private AdaptetRecyclerManagers adaptetRecyclerManagers;
    private List<Employees> managers;
    private List<Employees> managersApi;


    private RecyclerView recyclerEmployees;
    private RecyclerView.LayoutManager layoutManagerEmployees;
    private AdaptetRecyclerEmployees adaptetRecyclerEmployees;
    private List<Employees> employees;
    private List<Employees> employeesApi;


    private AlertDialog.Builder alertDialogManager;
    private AlertDialog.Builder alertDialogEmployee;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);


        viewModelEmployee = ViewModelProviders.of(EmpActivity.this).get(ViewModelEmployee.class);
        viewModelEmployee.getEmployee().observe(EmpActivity.this, new Observer<List<Employees>>() {
            @Override
            public void onChanged(@Nullable List<Employees> employees) {

                EmpActivity.this.employees = employees;
                adaptetRecyclerEmployees.setData(employees);
            }
        });


        viewModelEmployee.getManager().observe(EmpActivity.this, new Observer<List<Employees>>() {
            @Override
            public void onChanged(@Nullable List<Employees> managers) {

                EmpActivity.this.managers = managers;
                adaptetRecyclerManagers.setData(managers);
            }
        });
        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        getManagers();


        getEmployees();

        FabAddmanager = (FloatingActionButton) findViewById(R.id.FabAddmanager);
        FabAddmanager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogManagers dialogManagers = new DialogManagers();
                dialogManagers.show(getSupportFragmentManager(), "dialogManagers");
                DialogManagers.AddSave = false;
            }
        });

        FabAddEmployee = (FloatingActionButton) findViewById(R.id.FabAddEmployee);
        FabAddEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogEmployees dialogEmployees = new DialogEmployees();
                dialogEmployees.show(getSupportFragmentManager(), "dialogEmployees");
                DialogEmployees.AddSave = false;
            }
        });


        recyclerManagers = (RecyclerView) findViewById(R.id.recyclerManagers);
        recyclerManagers.setHasFixedSize(true);
        layoutManagerManagers = new LinearLayoutManager(EmpActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(CustmersActivity.this, 2);
        recyclerManagers.setLayoutManager(layoutManagerManagers);
        managers = new ArrayList<>();
        managersApi = new ArrayList<>();
        adaptetRecyclerManagers = new AdaptetRecyclerManagers(EmpActivity.this, managers);
        recyclerManagers.setAdapter(adaptetRecyclerManagers);


        recyclerEmployees = (RecyclerView) findViewById(R.id.recyclerEmployees);
        recyclerEmployees.setHasFixedSize(true);
        layoutManagerEmployees = new LinearLayoutManager(EmpActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(CustmersActivity.this, 2);
        recyclerEmployees.setLayoutManager(layoutManagerEmployees);
        employees = new ArrayList<>();
        employeesApi = new ArrayList<>();
        adaptetRecyclerEmployees = new AdaptetRecyclerEmployees(EmpActivity.this, employees);
        recyclerEmployees.setAdapter(adaptetRecyclerEmployees);


        createDialogEmployee();
        createDialogManager();

    }


    public void deleteEmployee(Employees employee) {
        selectedEmployee = employee;
        alertDialogEmployee.show();

    }

    public void deleteManager(Employees manager) {
        selectedManager = manager;
        alertDialogManager.show();

    }


    public void showDataEmployee(Employees employee) {
        selectedEmployee = employee;
        DialogEmployees dialogEmployees = new DialogEmployees();
        dialogEmployees.setEmployeeName(selectedEmployee.getName());
        dialogEmployees.setEmployeeNumber(selectedEmployee.getNumber());
        dialogEmployees.setEmployeeJob(selectedEmployee.getJobName());
        dialogEmployees.show(getSupportFragmentManager(), "dialogEmployees");
        DialogEmployees.AddSave = true;

    }

    public void showDataManager(Employees manager) {
        selectedManager = manager;
        DialogManagers dialogManagers = new DialogManagers();
        dialogManagers.setManagerName(selectedManager.getName());
        dialogManagers.setManagerNumber(selectedManager.getNumber());
        dialogManagers.show(getSupportFragmentManager(), "dialogManagers");
        DialogManagers.AddSave = true;

    }


    private void createDialogEmployee() {

        alertDialogEmployee = new AlertDialog.Builder(EmpActivity.this);
        alertDialogEmployee.setTitle("Confirm Delete...");
        alertDialogEmployee.setMessage("Do you sure you want Delete this?");
        alertDialogEmployee.setIcon(R.drawable.ic_delete);

        alertDialogEmployee.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                getOneEmployee();
                viewModelEmployee.delete(selectedEmployee);
                adaptetRecyclerEmployees.setData(employees);

                dialog.dismiss();
            }
        });
        alertDialogEmployee.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                dialog.dismiss();
            }
        });
    }


    private void createDialogManager() {

        alertDialogManager = new AlertDialog.Builder(EmpActivity.this);
        alertDialogManager.setTitle("Confirm Delete...");
        alertDialogManager.setMessage("Do you sure you want Delete this?");
        alertDialogManager.setIcon(R.drawable.ic_delete);

        alertDialogManager.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getOneManager();
                viewModelEmployee.delete(selectedManager);
                adaptetRecyclerManagers.setData(managers);

                dialog.dismiss();
            }
        });
        alertDialogManager.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                dialog.dismiss();
            }
        });
    }


    public void getEmployees() {


        Call<List<Employees>> call = jsonPlaceHolderApi.getApiAllEmployees();

        call.enqueue(new Callback<List<Employees>>() {
            @Override
            public void onResponse(Call<List<Employees>> call, Response<List<Employees>> response1) {
                if (!response1.isSuccessful()) {
                    Toast.makeText(EmpActivity.this, "فشلو", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Employees> employees = response1.body();


                for (Employees employee : employees) {
                    if (employee.getManegerID() != 0)
                        employeesApi.add(employee);
                }


                for (Employees employee : EmpActivity.this.employees) {
                    if (employee.getManegerID() != 0)
                        viewModelEmployee.delete(employee);
                }
                for (Employees employee : employeesApi) {
                    if (employee.getManegerID() != 0)
                        viewModelEmployee.insert(employee);
                }


                adaptetRecyclerEmployees.setData(EmpActivity.this.employees);

            }

            @Override
            public void onFailure(Call<List<Employees>> call, Throwable t) {
            }
        });
    }

    public void DeleteEmployee(int id) {
        Call<Void> call = jsonPlaceHolderApi.deleteEmployee(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    public void getOneEmployee() {


        Call<List<Employees>> call = jsonPlaceHolderApi.getOneEmployee();

        call.enqueue(new Callback<List<Employees>>() {
            @Override
            public void onResponse(Call<List<Employees>> call, Response<List<Employees>> response3) {
                if (!response3.isSuccessful()) {
                    Toast.makeText(EmpActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Employees> employees = response3.body();

                for (int i = 0; i < employees.size(); i++) {
                    if (employees.get(i).getName().equals(selectedEmployee.getName())) {
                        EmployeeID = employees.get(i).getEmpid();
                        DeleteEmployee(EmployeeID);
                        return;
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Employees>> call, Throwable t) {
                Toast.makeText(EmpActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getManagers() {
        Call<List<Employees>> call = jsonPlaceHolderApi.getApiAllManagers();

        call.enqueue(new Callback<List<Employees>>() {
            @Override
            public void onResponse(Call<List<Employees>> call, Response<List<Employees>> response1) {
                if (!response1.isSuccessful()) {

                }
                List<Employees> Managers = response1.body();
                for (Employees manager : Managers) {
                    if (manager.getManegerID() == 0)
                        managersApi.add(manager);
                }


                for (Employees manager : EmpActivity.this.managers) {
                    if (manager.getManegerID() == 0)
                        viewModelEmployee.delete(manager);
                }
                for (Employees manager : managersApi) {
                    if (manager.getManegerID() == 0)
                        viewModelEmployee.insert(manager);
                }


                adaptetRecyclerManagers.setData(EmpActivity.this.managers);


            }

            @Override
            public void onFailure(Call<List<Employees>> call, Throwable t) {
                Toast.makeText(EmpActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void DeleteManager(int id) {
        Call<Void> call = jsonPlaceHolderApi.deleteManager(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    public void getOneManager() {

        Call<List<Employees>> callTypes = jsonPlaceHolderApi.getOneManager();

        callTypes.enqueue(new Callback<List<Employees>>() {
            @Override
            public void onResponse(Call<List<Employees>> call, Response<List<Employees>> response3) {
                if (!response3.isSuccessful()) {
                    Toast.makeText(EmpActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Employees> managers = response3.body();

                for (int i = 0; i < managers.size(); i++) {
                    if (managers.get(i).getName().equals(selectedManager.getName())) {
                        ManagerID = managers.get(i).getEmpid();
                        DeleteManager(ManagerID);
                        return;
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Employees>> call, Throwable t) {
                Toast.makeText(EmpActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
