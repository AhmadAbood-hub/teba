package com.example.rteba.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.rteba.Adapter.AdaptetRecyclerCategoriesForCustomer;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Categories;
import com.example.rteba.ViewModel.ViewModelCategory;
import com.example.tebavet.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CategoriesForCustomer extends AppCompatActivity
{



    private JsonPlaceHolderApi jsonPlaceHolderApi;

    private List<Categories> Categories;


    ViewModelCategory viewModelCategory;



    private RecyclerView recyclerCategoriesForCustomer;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerCategoriesForCustomer adaptetRecyclerCategoriesForCustomer;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_for_customer);




        recyclerCategoriesForCustomer = (RecyclerView) findViewById(R.id.recyclerCategoriesForCustomer);
        recyclerCategoriesForCustomer.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(CategoriesForCustomer.this, 2);
        recyclerCategoriesForCustomer.setLayoutManager(layoutManager);
        Categories = new ArrayList<>();
        adaptetRecyclerCategoriesForCustomer = new AdaptetRecyclerCategoriesForCustomer(this, Categories);
        recyclerCategoriesForCustomer.setAdapter(adaptetRecyclerCategoriesForCustomer);







        viewModelCategory= ViewModelProviders.of(this).get(ViewModelCategory.class);

        viewModelCategory.getCategory().observe(this, new Observer<List<Categories>>()
        {
            @Override
            public void onChanged(@Nullable List<Categories> categories)
            {
                CategoriesForCustomer.this.Categories =categories;
                adaptetRecyclerCategoriesForCustomer.setData(CategoriesForCustomer.this.Categories);
            }
        });



        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        getCategory();



    }




        public void getCategory()
        {
            Call<List<Categories>> callCategory = jsonPlaceHolderApi.getApiAllCategories();

            callCategory.enqueue(new Callback<List<Categories>>() {
                @Override
                public void onResponse(Call<List<Categories>> call, Response<List<Categories>> response1) {
                    if (!response1.isSuccessful()) {
                        return;
                    }
                    List<Categories> categories = response1.body();


                    for (Categories categor : CategoriesForCustomer.this.Categories) {
                        viewModelCategory.delete(categor);
                    }
                    for (Categories category : categories) {
                        viewModelCategory.insert(category);
                    }


                    adaptetRecyclerCategoriesForCustomer.setData(CategoriesForCustomer.this.Categories);

                }

                @Override
                public void onFailure(Call<List<Categories>> call, Throwable t) {
                }
            });
        }


    }





