package com.example.rteba.View.Dialog;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Custmers;
import com.example.rteba.View.Activity.LoginActivity;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.CustmersActivity;
import com.example.rteba.ViewModel.ViewModelCustmer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogCustmers extends DialogFragment
{
    JsonPlaceHolderApi jsonPlaceHolderApi;

    private ViewModelCustmer viewModelCustmer;
    public static boolean  AddSave = false;
    private String custmerName;
    private String  custmerNumber;

    public void setCustmerName(String custmerName)

    {
        this.custmerName = custmerName;
    }

    public void setCustmerNumber(String custmerNumber)
    {
        this.custmerNumber = custmerNumber;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {

        View view  = inflater.inflate(R.layout.dialog_custmers, container, false);
        final TextInputEditText edtDialogCustmerName = (TextInputEditText) view.findViewById(R.id.edtDialogCustmerName);
        final TextInputEditText edtDialogCustmerNumber = (TextInputEditText) view.findViewById(R.id.edtDialogCustmerNumber);
        final Button btnDialogCustmerAdd = (Button) view.findViewById(R.id.btnDialogCustmerAdd);
        final Button btnDialogCustmerSave = (Button) view.findViewById(R.id.btnDialogCustmerSave);


        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);




        viewModelCustmer = ViewModelProviders.of(DialogCustmers.this).get(ViewModelCustmer.class);
if(AddSave)
{
    btnDialogCustmerAdd.setVisibility(View.GONE);
    btnDialogCustmerSave.setVisibility(View.VISIBLE);
    edtDialogCustmerName.setText(custmerName);
    edtDialogCustmerNumber.setText(custmerNumber);

}
        btnDialogCustmerAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String name = edtDialogCustmerName.getText().toString();
                String number = edtDialogCustmerNumber.getText().toString();
                if(name.equals("")||number.equals(""))
                {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }




                createCustmer(name,number);




                edtDialogCustmerName.setText("");
                edtDialogCustmerNumber.setText("");
                dismiss();
            }
        });
btnDialogCustmerSave.setOnClickListener(new View.OnClickListener()
{
    @Override
    public void onClick(View view)
    {



      custmerName = edtDialogCustmerName.getText().toString();
      custmerNumber = edtDialogCustmerNumber.getText().toString();
        if(custmerName.equals("")||custmerNumber.equals(""))
        {
            Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
            return;
        }
        CustmersActivity.selectedCustmer.setNameCustmer(custmerName);
        CustmersActivity.selectedCustmer.setNumberCustmer(custmerNumber);
        viewModelCustmer.update(CustmersActivity.selectedCustmer);
        edtDialogCustmerName.setText("");
        edtDialogCustmerNumber.setText("");
        updateCustmer(CustmersActivity.selectedCustmer);
        startActivity(new Intent(getContext(),LoginActivity.class));


        dismiss();

    }
});
        return view;
    }



    private void createCustmer(String custmerName,String custmerNumber)
    {
        Custmers custmer = new Custmers(custmerName,custmerNumber,0);


        Call<Custmers> call = jsonPlaceHolderApi.createCustmer(custmer);


        viewModelCustmer.insert(custmer);


        call.enqueue(new Callback<Custmers>()
        {
            @Override
            public void onResponse(Call<Custmers> call, Response<Custmers> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Custmers> call, Throwable t) {

            }
        });


    }




    private void updateCustmer(Custmers custmer)
    {


        Call<Custmers> call = jsonPlaceHolderApi.updateCustmer(custmer.getIdCustmer(),custmer);



        call.enqueue(new Callback<Custmers>()
        {
            @Override
            public void onResponse(Call<Custmers> call, Response<Custmers> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Custmers> call, Throwable t) {

            }
        });


    }



}
