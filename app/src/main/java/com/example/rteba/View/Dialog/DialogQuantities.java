package com.example.rteba.View.Dialog;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Quantities;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.QuaTypCatActivity;
import com.example.rteba.ViewModel.ViewModelQuantity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogQuantities extends DialogFragment
{
    JsonPlaceHolderApi  jsonPlaceHolderApi;

    private ViewModelQuantity viewModelQuantity;
   public static boolean  AddSave = false;
    private int QuantityQuantity;
    private String  QuantityUnit;

    public void setQuantityQuantity(int quantityQuantity)
    {
        QuantityQuantity = quantityQuantity;
    }

    public void setQuantityUnit(String quantityUnit)
    {
        QuantityUnit = quantityUnit;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {

        View view  = inflater.inflate(R.layout.dialog_quantity, container, false);
        final TextInputEditText edtDialogQuantityQuantity = (TextInputEditText) view.findViewById(R.id.edtDialogQuantityQuantity);
        final TextInputEditText edtDialogQuantityUnit = (TextInputEditText) view.findViewById(R.id.edtDialogQuantityUnit);
        final Button btnDialogQuantityAdd = (Button) view.findViewById(R.id.btnDialogQuantityAdd);
        final Button btnDialogQuantitySave = (Button) view.findViewById(R.id.btnDialogQuantitySave);

        viewModelQuantity = ViewModelProviders.of(DialogQuantities.this).get(ViewModelQuantity.class);
if(AddSave)
{
    btnDialogQuantityAdd.setVisibility(View.GONE);
    btnDialogQuantitySave.setVisibility(View.VISIBLE);
    edtDialogQuantityQuantity.setText(QuantityQuantity+"");
    edtDialogQuantityUnit.setText(QuantityUnit);

}

        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        btnDialogQuantityAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try
                {
                    int quantity =Integer.parseInt(edtDialogQuantityQuantity.getText().toString()) ;
                    String unit = edtDialogQuantityUnit.getText().toString();
                    if(quantity<0||unit.equals(""))
                    {
                        Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                        return;
                    }



                    createQuantity(unit,quantity);
                    Quantities quantities =new Quantities(quantity,unit);
                    edtDialogQuantityQuantity.setText("");
                    edtDialogQuantityUnit.setText("");
                    dismiss();
                }
               catch (Exception e)
               {

               }
            }
        });
        btnDialogQuantitySave.setOnClickListener(new View.OnClickListener()
{
    @Override
    public void onClick(View view)
    {


try
{
    QuantityQuantity = Integer.parseInt(edtDialogQuantityQuantity.getText().toString()) ;
    QuantityUnit =  edtDialogQuantityUnit.getText().toString();
    if(QuantityQuantity<0||QuantityUnit.equals(""))
    {
        Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
        return;
    }
    QuaTypCatActivity.selectedQuantity.setQuantity(QuantityQuantity);
    QuaTypCatActivity.selectedQuantity.setUnit(QuantityUnit);
    viewModelQuantity.update(QuaTypCatActivity.selectedQuantity);
    edtDialogQuantityQuantity.setText("");
    edtDialogQuantityUnit.setText("");
    updateQuantity(QuaTypCatActivity.selectedQuantity);
    dismiss();
}
catch (Exception e)
{

}


    }
});
        return view;
    }
    private void createQuantity(String unit, int quantity)
    {
        Quantities  Quantity = new Quantities(quantity,unit);


        Call<Quantities> call = jsonPlaceHolderApi.createQuantity(Quantity);


        viewModelQuantity.insert(Quantity);


        call.enqueue(new Callback<Quantities>()
        {
            @Override
            public void onResponse(Call<Quantities> call, Response<Quantities> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Quantities> call, Throwable t) {

            }
        });


    }







    private void updateQuantity(Quantities quantity)
    {


        Call<Quantities> call = jsonPlaceHolderApi.updateQuantity(quantity.getId(), quantity);


        call.enqueue(new Callback<Quantities>()
        {
            @Override
            public void onResponse(Call<Quantities> call, Response<Quantities> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Quantities> call, Throwable t)
            {

            }
        });


    }





}
