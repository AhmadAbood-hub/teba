package com.example.rteba.View.Dialog;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Employees;
import com.example.rteba.Model.Entity.Jobs;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.EmpActivity;
import com.example.rteba.ViewModel.ViewModelEmployee;
import com.example.rteba.ViewModel.ViewModelJob;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogEmployees extends DialogFragment
{
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    private ViewModelEmployee viewModelEmployee;
    private ViewModelJob viewModelJob;
    public static boolean  AddSave = false;
    private String employeeName;
    private String  employeeNumber;
    private List<String> jobName ;
    private   int jobid;
    private  String SpJob;

    public void setEmployeeName(String employeeName)
    {
        this.employeeName = employeeName;
    }
    public void setEmployeeNumber(String employeeNumber)
    {
        this.employeeNumber = employeeNumber;
    }
    public void setEmployeeJob(String SpJob)
    {
        this.SpJob = SpJob;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
jobName = new ArrayList<>();
        View view  = inflater.inflate(R.layout.dialog_employee, container, false);
        final TextInputEditText edtDialogEmployeeName = (TextInputEditText) view.findViewById(R.id.edtDialogEmployeeName);
        final TextInputEditText edtDialogEmployeeNumber = (TextInputEditText) view.findViewById(R.id.edtDialogEmployeeNumber);
        final Spinner spDialogEmployeesJob = (Spinner) view.findViewById(R.id.spDialogEmployeesJob);

        final Button btnDialogEmployeeAdd = (Button) view.findViewById(R.id.btnDialogEmployeeAdd);
        final Button btnDialogEmployeeSave = (Button) view.findViewById(R.id.btnDialogEmployeeSave);
        viewModelEmployee = ViewModelProviders.of(DialogEmployees.this).get(ViewModelEmployee.class);
        viewModelJob = ViewModelProviders.of(DialogEmployees.this).get(ViewModelJob.class);
        viewModelJob.getJob().observe(getActivity(), new Observer<List<Jobs>>()
        {
            @Override
            public void onChanged(@Nullable List<Jobs> jobs)
            {
           //     jobe = jobs;
            }
        });
        jobName =new ArrayList<>();
        viewModelJob.getName().observe(getActivity(), new Observer<List<String>>()
        {
            @Override
            public void onChanged(@Nullable List<String> strings)
            {

                for(String a : strings)
                {
                    jobName.add(a+"");
                }
            }
        });



        jobName.add("");




        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, jobName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogEmployeesJob.setAdapter(adapter);



if(AddSave)
{
    btnDialogEmployeeAdd.setVisibility(View.GONE);
    btnDialogEmployeeSave.setVisibility(View.VISIBLE);
    edtDialogEmployeeName.setText(employeeName);
    edtDialogEmployeeNumber.setText(employeeNumber);
}
        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        btnDialogEmployeeAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                SpJob = spDialogEmployeesJob.getSelectedItem().toString();
                if(SpJob.equals(""))
                {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }
                viewModelJob.getID(SpJob+"").observe(getActivity(), new Observer<Integer>()
                {
                    @Override
                    public void onChanged(@Nullable Integer integer)
                    {
                        jobid= integer;
                        viewModelJob.getNameID(jobid).observe(getActivity(), new Observer<String>()
                        {
                            @Override
                            public void onChanged(@Nullable String s)
                            {

                                String name = edtDialogEmployeeName.getText().toString();
                                String number = edtDialogEmployeeNumber.getText().toString();

                                if(name.equals("")||number.equals("")||SpJob.equals(""))
                                {
                                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                                    return;
                                }




                                  createEmployee(name,jobid,number,1,s);




                                edtDialogEmployeeName.setText("");
                                edtDialogEmployeeNumber.setText("");
                                dismiss();
                            }
                        });

                    }
                });

            }
        });


        btnDialogEmployeeSave.setOnClickListener(new View.OnClickListener()
{
    @Override
    public void onClick(View view)
    {

try {

    employeeName = edtDialogEmployeeName.getText().toString();
    employeeNumber = edtDialogEmployeeNumber.getText().toString();
    SpJob = spDialogEmployeesJob.getSelectedItem().toString();
    if(employeeName.equals("")||employeeNumber.equals("")||SpJob.equals(""))
    {
        Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
        return;
    }
    EmpActivity.selectedEmployee.setName(employeeName);
    EmpActivity.selectedEmployee.setNumber(employeeNumber);
    EmpActivity.selectedEmployee.setJobName(SpJob);
    viewModelJob.getID(SpJob+"").observe(getActivity(), new Observer<Integer>() {
                @Override
                public void onChanged(@Nullable Integer integer)
                {
                    jobid = integer;
                    EmpActivity.selectedEmployee.setJobID(jobid);
                    viewModelEmployee.update(EmpActivity.selectedEmployee);
                }
            });


    edtDialogEmployeeName.setText("");
    edtDialogEmployeeNumber.setText("");

    updateEmployee(EmpActivity.selectedEmployee);
    dismiss();
}

     catch (Exception e)
     {

     }
    }
});
        return view;
    }

    private void createEmployee( String name, int jobID,String num,int managerID,String jobName)
    {
        Employees employee = new Employees(name,jobID,num,managerID,jobName);


        Call<Employees> call = jsonPlaceHolderApi.createEmployee(employee);


        viewModelEmployee.insert(employee);


        call.enqueue(new Callback<Employees>()
        {
            @Override
            public void onResponse(Call<Employees> call, Response<Employees> response)
            {
                if (!response.isSuccessful())
                {
                    Toast.makeText(getContext(), "فشلو", Toast.LENGTH_SHORT).show();
                    return;
                }

            }

            @Override
            public void onFailure(Call<Employees> call, Throwable t)
            {
                return;
            }
        });

    }



    private void updateEmployee(Employees employee)
    {


        Call<Employees> call = jsonPlaceHolderApi.updateEmployee(employee.getEmpid(),employee);


        call.enqueue(new Callback<Employees>()
        {
            @Override
            public void onResponse(Call<Employees> call, Response<Employees> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Employees> call, Throwable t)
            {

            }
        });


    }
}
