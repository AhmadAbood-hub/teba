package com.example.rteba.View.Dialog;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Employees;
import com.example.rteba.Model.Entity.Jobs;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.EmpActivity;
import com.example.rteba.ViewModel.ViewModelEmployee;
import com.example.rteba.ViewModel.ViewModelJob;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogManagers extends DialogFragment
{
    JsonPlaceHolderApi jsonPlaceHolderApi;

    private ViewModelEmployee viewModelEmployee;
    private ViewModelJob viewModelJob;
   public static boolean  AddSave = false;
    private String managerName;
    private String  managerNumber;
    private List<Jobs> Job;
    int JobID;
    int ResultID;
    public void setManagerName(String managerName)
    {
        this.managerName = managerName;
    }

    public void setManagerNumber(String managerNumber)
    {
        this.managerNumber = managerNumber;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {

        View view  = inflater.inflate(R.layout.dialog_manager, container, false);
        final TextInputEditText edtDialogManagerName = (TextInputEditText) view.findViewById(R.id.edtDialogManagerName);
        final TextInputEditText edtDialogManagerNumber = (TextInputEditText) view.findViewById(R.id.edtDialogManagerNumber);
        final Button btnDialogManagerAdd = (Button) view.findViewById(R.id.btnDialogManagerAdd);
        final Button btnDialogManagerSave = (Button) view.findViewById(R.id.btnDialogManagerSave);

        viewModelEmployee = ViewModelProviders.of(DialogManagers.this).get(ViewModelEmployee.class);
        viewModelJob = ViewModelProviders.of(DialogManagers.this).get(ViewModelJob.class);
        viewModelJob.getJob().observe(getActivity(), new Observer<List<Jobs>>()
                {
                    @Override
                    public void onChanged(@Nullable List<Jobs> jobs)
                    {
                       Job =jobs;
                    }
                });
                viewModelEmployee.getResultID().observe(getActivity(), new Observer<Integer>() {
                    @Override
                    public void onChanged(@Nullable Integer integer) {
                        ResultID = integer;

                    }
                });
if(AddSave)
{
    btnDialogManagerAdd.setVisibility(View.GONE);
    btnDialogManagerSave.setVisibility(View.VISIBLE);
    edtDialogManagerName.setText(managerName);
    edtDialogManagerNumber.setText(managerNumber);

}

        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        btnDialogManagerAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String name = edtDialogManagerName.getText().toString();
                String number = edtDialogManagerNumber.getText().toString();
                if(name.equals("")||number.equals(""))
                {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }

                    for(Jobs j :Job)
                    {
                        if(j.getName().equals("Manager"))
                        {
                            JobID = j.getId();
                        }
                    }



                createManager(name+"",JobID,number+"",0,"");
                edtDialogManagerName.setText("");
                edtDialogManagerNumber.setText("");

                dismiss();
            }
        });
        btnDialogManagerSave.setOnClickListener(new View.OnClickListener()
{
    @Override
    public void onClick(View view)
    {

try {

    managerName = edtDialogManagerName.getText().toString();
    managerNumber = edtDialogManagerNumber.getText().toString();
    if(managerName.equals("")||managerNumber.equals(""))
    {
        Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
        return;
    }
    EmpActivity.selectedManager.setName(managerName);
    EmpActivity.selectedManager.setNumber(managerNumber);
    viewModelEmployee.update(EmpActivity.selectedManager);
    edtDialogManagerName.setText("");
    edtDialogManagerNumber.setText("");
    updateManager(EmpActivity.selectedManager);
    dismiss();
}
catch (Exception e)
{

}



    }
});
        return view;
    }



    private void createManager( String name, int jobID,String num,int managerID,String jobName)
    {
        Employees manager = new Employees(name,jobID,num,managerID,jobName);


        Call<Employees> call = jsonPlaceHolderApi.createManager(manager);


        viewModelEmployee.insert(manager);


        call.enqueue(new Callback<Employees>()
        {
            @Override
            public void onResponse(Call<Employees> call, Response<Employees> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Employees> call, Throwable t)
            {
                    return;
            }
        });

    }


    private void updateManager(Employees employee)
    {
    

        Call<Employees> call = jsonPlaceHolderApi.updateManager(employee.getEmpid(),employee);


        call.enqueue(new Callback<Employees>()
        {
            @Override
            public void onResponse(Call<Employees> call, Response<Employees> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Employees> call, Throwable t)
            {

            }
        });


    }

}
