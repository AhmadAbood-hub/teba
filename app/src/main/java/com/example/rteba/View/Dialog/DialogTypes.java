package com.example.rteba.View.Dialog;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Types;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.QuaTypCatActivity;
import com.example.rteba.ViewModel.ViewModelType;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogTypes extends DialogFragment {

    private ViewModelType viewModelType;
    public static boolean AddSave = false;
    private String typeName;
    JsonPlaceHolderApi jsonPlaceHolderApi;

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_type, container, false);
        final TextInputEditText edtDialogTypeName = (TextInputEditText) view.findViewById(R.id.edtDialogTypeName);
        final Button btnDialogTypeAdd = (Button) view.findViewById(R.id.btnDialogTypeAdd);
        final Button btnDialogTypeSave = (Button) view.findViewById(R.id.btnDialogTypeSave);

        viewModelType = ViewModelProviders.of(DialogTypes.this).get(ViewModelType.class);
        if (AddSave) {
            btnDialogTypeAdd.setVisibility(View.GONE);
            btnDialogTypeSave.setVisibility(View.VISIBLE);
            edtDialogTypeName.setText(typeName);

        }
        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        btnDialogTypeAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtDialogTypeName.getText().toString();
                if (name.equals("")) {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }


                createType(name);


                edtDialogTypeName.setText("");
                dismiss();
            }
        });
        btnDialogTypeSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                typeName = edtDialogTypeName.getText().toString();
                if (typeName.equals("")) {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }
                QuaTypCatActivity.selectedType.setName(typeName);
                viewModelType.update(QuaTypCatActivity.selectedType);
                edtDialogTypeName.setText("");

                updateType(QuaTypCatActivity.selectedType);

                dismiss();

            }
        });
        return view;
    }

    private void createType(final String name) {
        Types type = new Types(name);


        Call<Types> call = jsonPlaceHolderApi.createType(type);


        viewModelType.insert(type);


        call.enqueue(new Callback<Types>() {
            @Override
            public void onResponse(Call<Types> call, Response<Types> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Types> call, Throwable t) {

            }
        });

    }


    private void updateType(Types type) {


        Call<Types> call = jsonPlaceHolderApi.updateType(type.getId(), type);


        call.enqueue(new Callback<Types>() {
            @Override
            public void onResponse(Call<Types> call, Response<Types> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Types> call, Throwable t) {

            }
        });


    }
}
