package com.example.rteba.View.Dialog;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.BillsSale_Items;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.ViewModel.ViewModelBillSaleItem;
import com.example.tebavet.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogEnterCount extends DialogFragment
{
    JsonPlaceHolderApi jsonPlaceHolderApi;

    BillsSale_Items billsSale_items;
    ViewModelBillSaleItem viewModelBillSaleItem;
     public   int oldCountBillItem;


    public void setItem(BillsSale_Items billsSale_items)

    {
        this.billsSale_items = billsSale_items;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {

        View view  = inflater.inflate(R.layout.dialog_enter_count, container, false);
        viewModelBillSaleItem = ViewModelProviders.of(this).get(ViewModelBillSaleItem.class);


        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);



        final TextInputEditText edtDialogEnterCount = (TextInputEditText) view.findViewById(R.id.edtDialogEnterCount);
        final Button Done = (Button) view.findViewById(R.id.btnDialogEnterCount);

        Done.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                try {

                    oldCountBillItem =billsSale_items.getBillSaleItemCount();

                    billsSale_items.setBillSaleItemCount(Integer.parseInt( edtDialogEnterCount.getText().toString()));
                    viewModelBillSaleItem.update(billsSale_items);

                    updateBillSaleItem(billsSale_items);

                    updateCountItem(billsSale_items);

                    dismiss();
                }
                catch (Exception e)
                {

                }

            }
        });

        return view;
    }



    private void updateBillSaleItem(BillsSale_Items billsSale_item)
    {


        Call<BillsSale_Items> call = jsonPlaceHolderApi.updateBillSaleItem(billsSale_item.getID(),billsSale_item);



        call.enqueue(new Callback<BillsSale_Items>()
        {
            @Override
            public void onResponse(Call<BillsSale_Items> call, Response<BillsSale_Items> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<BillsSale_Items> call, Throwable t) {

            }
        });


    }


    private void updateCountItem(BillsSale_Items billsSale_item)
    {


        Call<Items> call = jsonPlaceHolderApi.updateCountItem(billsSale_item.getBillSaleItemID(),billsSale_item.getBillSaleItemCount()-oldCountBillItem);



        call.enqueue(new Callback<Items>()
        {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {

            }
        });


    }



}

