package com.example.rteba.View.Activity;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.rteba.Adapter.AdaptetRecyclerJobs;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Jobs;
import com.example.tebavet.R;
import com.example.rteba.View.Dialog.DialogJobs;
import com.example.rteba.ViewModel.ViewModelJob;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class JobsActivity extends AppCompatActivity
{
    JsonPlaceHolderApi jsonPlaceHolderApi;

    public int jobID;

    public static Jobs selectedJob;

   
    private ViewModelJob viewModelJob;


    private FloatingActionButton FabAddJob;

    private RecyclerView recyclerJobs;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerJobs adaptetRecyclerJobs;
    private List<Jobs> Jobs;


    private  AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);


        viewModelJob = ViewModelProviders.of(JobsActivity.this).get(ViewModelJob.class);
        viewModelJob.getJob().observe(JobsActivity.this, new Observer<List<Jobs>>() {
            @Override
            public void onChanged(@Nullable List<Jobs> Jobs) {

                JobsActivity.this.Jobs = Jobs;
                adaptetRecyclerJobs.setData(Jobs);
            }
        });
        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);






        getJobs();









        FabAddJob = (FloatingActionButton) findViewById(R.id.FabAddJob);
        FabAddJob.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogJobs DialogJobs = new DialogJobs();
                DialogJobs.show(getSupportFragmentManager(), "DialogJobs");
                DialogJobs.AddSave =false;
            }
        });

        recyclerJobs = (RecyclerView) findViewById(R.id.recyclerJobs);
        recyclerJobs.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(JobsActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(JobsActivity.this, 2);
        recyclerJobs.setLayoutManager(layoutManager);
        Jobs = new ArrayList<>();
        adaptetRecyclerJobs = new AdaptetRecyclerJobs(JobsActivity.this, Jobs);
        recyclerJobs.setAdapter(adaptetRecyclerJobs);

        createDialog();
    }





    public  void delete(Jobs Jobs)
    {
        selectedJob =Jobs;
        alertDialog.show();

    }

    public void showData(Jobs Jobs)
    {
        selectedJob = Jobs;
        DialogJobs dialogJobs = new DialogJobs();
        dialogJobs.setJobName(selectedJob.getName());
        dialogJobs.setJobSalay(selectedJob.getSalary());
        dialogJobs.setJobHour(selectedJob.getHour());
        dialogJobs.setJobCostPerHour(selectedJob.getCostPerHour());
        dialogJobs.show(getSupportFragmentManager(), "DialogJobs");
        dialogJobs.AddSave =true;

    }
    private void createDialog()

    {

        alertDialog = new AlertDialog.Builder(JobsActivity.this);
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Do you sure you want Delete this?");
        alertDialog.setIcon(R.drawable.ic_delete);

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                getOneJob();
                viewModelJob.delete(selectedJob);
                adaptetRecyclerJobs.setData(Jobs);

                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {


                dialog.dismiss();
            }
        });
    }




    public void getJobs()
    {
        Call<List<Jobs>> call = jsonPlaceHolderApi.getApiAllJobs();

        call.enqueue(new Callback<List<Jobs>>()
        {
            @Override
            public void onResponse(Call<List<Jobs>> call, Response<List<Jobs>> response1)
            {
                if (!response1.isSuccessful())
                {

                }
                List<Jobs> jobs = response1.body();



                    for (Jobs job : JobsActivity.this.Jobs)
                    {
                            viewModelJob.delete(job);
                    }
                    for (Jobs job :jobs )
                    {

                            viewModelJob.insert(job);
                    }


                adaptetRecyclerJobs.setData(JobsActivity.this.Jobs);



            }

            @Override
            public void onFailure(Call<List<Jobs>> call, Throwable t)
            {
                Toast.makeText(JobsActivity.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void DeleteJob(int id)
    {
        Call<Void> call =jsonPlaceHolderApi.deleteJob(id);
        call.enqueue(new Callback<Void>()
        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t)
            {

            }
        });
    }
    public void getOneJob()
    {
        Call<List<Jobs>> call = jsonPlaceHolderApi.getOneJob();

        call.enqueue(new Callback<List<Jobs>>()
        {
            @Override
            public void onResponse(Call<List<Jobs>> call, Response<List<Jobs>> response3)
            {
                if (!response3.isSuccessful())
                {
                    Toast.makeText(JobsActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Jobs> jobs = response3.body();

                for(int i=0 ;i<jobs.size();i++)
                {
                    if(jobs.get(i).getName().equals(selectedJob.getName()) )
                    {
                        jobID =jobs.get(i).getId();
                        DeleteJob(jobID);
                        return;
                    }

                }
            }
            @Override
            public void onFailure(Call<List<Jobs>> call, Throwable t)
            {
                Toast.makeText(JobsActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }





}
