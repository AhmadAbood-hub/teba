package com.example.rteba.View.Dialog;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Categories;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.QuaTypCatActivity;
import com.example.rteba.ViewModel.ViewModelCategory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogCategories extends DialogFragment {
    JsonPlaceHolderApi jsonPlaceHolderApi;


    private ViewModelCategory viewModelCategory;
    public static boolean AddSave = false;
    private String categoryName;


    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_category, container, false);
        final TextInputEditText edtDialogCategoryName = (TextInputEditText) view.findViewById(R.id.edtDialogCategoryName);
        final Button btnDialogCategoryAdd = (Button) view.findViewById(R.id.btnDialogCategoryAdd);
        final Button btnDialogCategorySave = (Button) view.findViewById(R.id.btnDialogCategorySave);

        viewModelCategory = ViewModelProviders.of(DialogCategories.this).get(ViewModelCategory.class);
        if (AddSave) {
            btnDialogCategoryAdd.setVisibility(View.GONE);
            btnDialogCategorySave.setVisibility(View.VISIBLE);
            edtDialogCategoryName.setText(categoryName);

        }

        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        btnDialogCategoryAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = edtDialogCategoryName.getText().toString();
                if (name.equals("")) {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }


                createCategory(name);

                edtDialogCategoryName.setText("");
                dismiss();
            }
        });
        btnDialogCategorySave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                categoryName = edtDialogCategoryName.getText().toString();
                if (categoryName.equals("")) {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }
                QuaTypCatActivity.selectedCategory.setName(categoryName);
                viewModelCategory.update(QuaTypCatActivity.selectedCategory);
                edtDialogCategoryName.setText("");

                updateCategory(QuaTypCatActivity.selectedCategory);

                dismiss();

            }
        });
        return view;
    }

    private void createCategory(String name) {
        Categories category = new Categories(name);


        Call<Categories> call = jsonPlaceHolderApi.createCategory(category);


        viewModelCategory.insert(category);


        call.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {

            }
        });


    }


    private void updateCategory(Categories category) {


        Call<Categories> call = jsonPlaceHolderApi.updateCategory(category.getId(), category);


        call.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {

            }
        });


    }
}
