package com.example.rteba.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.rteba.Adapter.AdaptetRecyclerTypesForCustomer;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Types;
import com.example.rteba.ViewModel.ViewModelType;
import com.example.tebavet.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TypesForCustomer extends AppCompatActivity
{




    private JsonPlaceHolderApi jsonPlaceHolderApi;

    private List<Types> types;


    ViewModelType viewModelType;



    private RecyclerView recyclerTypesForCustomer;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerTypesForCustomer adaptetRecyclerTypesForCustomer;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_types_for_customer);




        recyclerTypesForCustomer = (RecyclerView) findViewById(R.id.recyclerTypesForCustomer);
        recyclerTypesForCustomer.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(TypesForCustomer.this, 2);
        recyclerTypesForCustomer.setLayoutManager(layoutManager);
        types = new ArrayList<>();
        adaptetRecyclerTypesForCustomer = new AdaptetRecyclerTypesForCustomer(this,types);
        recyclerTypesForCustomer.setAdapter(adaptetRecyclerTypesForCustomer);







        viewModelType= ViewModelProviders.of(this).get(ViewModelType.class);

        viewModelType.getTypes().observe(this, new Observer<List<Types>>()
        {
            @Override
            public void onChanged(@Nullable List<Types> types)
            {
                TypesForCustomer.this.types =types;
                adaptetRecyclerTypesForCustomer.setData(TypesForCustomer.this.types);
            }
        });



        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        getType();


    }




    public void getType()
    {
        Call<List<Types>> callTypes = jsonPlaceHolderApi.getApiTypes();

        callTypes.enqueue(new Callback<List<Types>>()
        {
            @Override
            public void onResponse(Call<List<Types>> call, Response<List<Types>> response3)
            {
                if (!response3.isSuccessful())
                {
                    return;
                }
                List<Types> types = response3.body();


                for (Types Type : TypesForCustomer.this.types)
                {
                    viewModelType.delete(Type);
                }
                for (Types TypeApi : types) {
                    viewModelType.insert(TypeApi);
                }

                adaptetRecyclerTypesForCustomer.setData(TypesForCustomer.this.types);
            }
            @Override
            public void onFailure(Call<List<Types>> call, Throwable t)
            {
            }
        });
    }

}





