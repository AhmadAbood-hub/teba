package com.example.rteba.View.Fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rteba.Adapter.AdaptetRecyclerCompaniesForCustomer;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Companies;
import com.example.rteba.ViewModel.ViewModelCompany;
import com.example.tebavet.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchFragment extends Fragment
{

  //  private ViewModelItem viewModelItem;
 //   private ProgressBar ProgressPar;
 //   HorizontalInfiniteCycleViewPager viewPager;
 //   List<Items> ItemList = new ArrayList<>();
 //   List<Items> Item = new ArrayList<>();
  public JsonPlaceHolderApi jsonPlaceHolderApi;

    public int CompanyID;
    private ViewModelCompany viewModelCompany;
    private RecyclerView recyclerCompany;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerCompaniesForCustomer adaptetRecyclerCompanies;
    private List<Companies> companies;







    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {



       View view =inflater.inflate(R.layout.activity_company_for_customer,container,false);





        viewModelCompany = ViewModelProviders.of(SearchFragment.this).get(ViewModelCompany.class);
        viewModelCompany.getCompany().observe(SearchFragment.this, new Observer<List<Companies>>()
        {
            @Override
            public void onChanged(@Nullable List<Companies> companies)
            {
              SearchFragment.this.companies = companies;
                adaptetRecyclerCompanies.setData(companies);
            }
        });


        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);



        getCompany();


        SearchFragment.this.companies = new ArrayList<>();
        adaptetRecyclerCompanies = new AdaptetRecyclerCompaniesForCustomer(getContext(),SearchFragment.this.companies);
        recyclerCompany = (RecyclerView)view.findViewById(R.id.recyclerBrands);
        recyclerCompany.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerCompany.setLayoutManager(layoutManager);
        recyclerCompany.setAdapter(adaptetRecyclerCompanies);














        //      ProgressPar = (ProgressBar) view.findViewById(R.id.ProgressPar);

  //  final    FloatingActionButton btn_done = (FloatingActionButton) view.findViewById(R.id.btn_done);

   //     viewPager =(HorizontalInfiniteCycleViewPager) view.findViewById(R.id.view_pager);
   //     viewModelItem = ViewModelProviders.of(getActivity()).get(ViewModelItem.class);
    //    viewModelItem.getItem().observe(getActivity(), new Observer<List<Items>>()
    //    {
    //        @Override
     //       public void onChanged(@Nullable List<Items> items)
     //       {
     //           try
       //         {
        //            ItemList = items;
         //           ProgressPar.setVisibility(View.GONE);
         ///           final MyAdapter myAdapter = new MyAdapter(getActivity(),ItemList,SearchFragment.this);
         //           viewPager.setAdapter(myAdapter);
         //           btn_done.setOnClickListener(new View.OnClickListener()
         //           {
         //               @Override
         //               public void onClick(View view)
         //               {
         //                   Paper.book().write("Cart",Item);
          //              }
         //           });
          //      }

//catch (Exception e)
//{

//}
  //          }
   //     });
































        return view;
    }
//public void ExportData(List<Items> items)
//{
 //   Item =items;
//}

    public void getCompany()
    {
        Call<List<Companies>> call = jsonPlaceHolderApi.getApiAllCompanies();

        call.enqueue(new Callback<List<Companies>>()
        {
            @Override
            public void onResponse(Call<List<Companies>> call, Response<List<Companies>> response3) {
                if (!response3.isSuccessful())
                {
                    return;
                }

                List<Companies> companies = response3.body();




                for (Companies company : SearchFragment.this.companies)
                {
                    viewModelCompany.delete(company);
                }
                for (Companies company  :companies )
                {
                    viewModelCompany.insert(company);
                }

                adaptetRecyclerCompanies.setData(SearchFragment.this.companies);
            }
            @Override
            public void onFailure(Call<List<Companies>> call, Throwable t)
            {

            }
        });
    }



}
