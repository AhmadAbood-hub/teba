package com.example.rteba.View.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.rteba.Adapter.AdaptetRecyclerCompanies;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Companies;
import com.example.tebavet.R;
import com.example.rteba.ViewModel.ViewModelCompany;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompaniesActivity extends AppCompatActivity {

    public JsonPlaceHolderApi jsonPlaceHolderApi;

    public int CompanyID;


    public static Companies selectedCompany;
    private boolean isExternalGranted = false;
    private final int REQUEST_EXTERNAL = 1000;
    private final int GALLERY_PICK = 2000;
    private RelativeLayout bottomSheetCompany;
    private TextInputEditText edtBottomSheetCompanyName;
    private TextInputEditText edtBottomSheetCompanyLocatin;
    private CircleImageView imgBottomSheetCompanyLogo;
    private Button btnBottomSheetCompanyAdd;
    private Button btnBottomSheetCompanySave;
    private BottomSheetBehavior bottomSheetBehavior;
    private ViewModelCompany viewModelCompany;
    private FloatingActionButton fabCompany;
    private RecyclerView recyclerCompany;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerCompanies adaptetRecyclerCompanies;
    private List<Companies> companies;

    private AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        viewModelCompany = ViewModelProviders.of(CompaniesActivity.this).get(ViewModelCompany.class);
        viewModelCompany.getCompany().observe(CompaniesActivity.this, new Observer<List<Companies>>() {
            @Override
            public void onChanged(@Nullable List<Companies> companies) {
                CompaniesActivity.this.companies = companies;
                adaptetRecyclerCompanies.setData(companies);
            }
        });


        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        getCompany();


        bottomSheetCompany = (RelativeLayout) findViewById(R.id.bottomSheetCompany);
        imgBottomSheetCompanyLogo = (CircleImageView) findViewById(R.id.imgBottomSheetCompanyLogo);
        edtBottomSheetCompanyName = (TextInputEditText) findViewById(R.id.edtBottomSheetCompanyName);
        edtBottomSheetCompanyLocatin = (TextInputEditText) findViewById(R.id.edtBottomSheetCompanyLocatin);
        btnBottomSheetCompanyAdd = (Button) findViewById(R.id.btnBottomSheetCompanyAdd);

        btnBottomSheetCompanySave = (Button) findViewById(R.id.btnBottomSheetCompanySave);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetCompany);

        fabCompany = (FloatingActionButton) findViewById(R.id.fabBrands);
        fabCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                btnBottomSheetCompanyAdd.setVisibility(View.VISIBLE);
                btnBottomSheetCompanySave.setVisibility(View.GONE);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }
        });

        imgBottomSheetCompanyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExternalGranted) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, GALLERY_PICK);
                } else {
                    Toast.makeText(CompaniesActivity.this, "Permissions denied", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnBottomSheetCompanyAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtBottomSheetCompanyName.getText().toString();
                String Location = edtBottomSheetCompanyLocatin.getText().toString();

                if (name.equals("") || Location.equals("")) {
                    Toast.makeText(CompaniesActivity.this, "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }


                try {
                    byte[] logo = imageViewToByteArray(imgBottomSheetCompanyLogo);
                    createCompany(name, Location, bittofile(((BitmapDrawable) imgBottomSheetCompanyLogo.getDrawable()).getBitmap()), logo);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    edtBottomSheetCompanyName.setText("");
                    edtBottomSheetCompanyLocatin.setText("");
                    imgBottomSheetCompanyLogo.setImageBitmap(null);
                } catch (Exception e) {

                    createCompanyWithoutImage(name, Location);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    edtBottomSheetCompanyName.setText("");
                    edtBottomSheetCompanyLocatin.setText("");
                }

            }

        });
        btnBottomSheetCompanySave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    selectedCompany.setCompanyName(edtBottomSheetCompanyName.getText().toString());
                    selectedCompany.setLocation(edtBottomSheetCompanyLocatin.getText().toString());
                    byte[] image = imageViewToByteArray(imgBottomSheetCompanyLogo);
                    selectedCompany.setImage(image);


                    updateCompany(selectedCompany, bittofile(((BitmapDrawable) imgBottomSheetCompanyLogo.getDrawable()).getBitmap()));

                    viewModelCompany.update(selectedCompany);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


                    Toast.makeText(CompaniesActivity.this, "Please wait ...", Toast.LENGTH_LONG).show();
                    Toast.makeText(CompaniesActivity.this, "Successful update ", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CompaniesActivity.this, NavigationDrawer.class);
                    startActivity(intent);


                } catch (Exception e) {

                }

            }
        });

        if (ContextCompat.checkSelfPermission(CompaniesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                ContextCompat.checkSelfPermission(CompaniesActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            requestRuntimePermission();
        } else {
            isExternalGranted = true;
        }


        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    fabCompany.setVisibility(View.GONE);
                } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED ||
                        bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    edtBottomSheetCompanyName.setText("");
                    edtBottomSheetCompanyLocatin.setText("");
                    fabCompany.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
        CompaniesActivity.this.companies = new ArrayList<>();
        adaptetRecyclerCompanies = new AdaptetRecyclerCompanies(this, CompaniesActivity.this.companies);
        recyclerCompany = (RecyclerView) findViewById(R.id.recyclerBrands);
        recyclerCompany.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerCompany.setLayoutManager(layoutManager);
        recyclerCompany.setAdapter(adaptetRecyclerCompanies);


        createDialog();
    }

    public void showData(Companies companies) {
        selectedCompany = companies;
        btnBottomSheetCompanySave.setVisibility(View.VISIBLE);
        btnBottomSheetCompanyAdd.setVisibility(View.GONE);
        edtBottomSheetCompanyName.setText(companies.getCompanyName());
        edtBottomSheetCompanyLocatin.setText(companies.getLocation());
        if (companies.getImage() == null)
            Glide.with(CompaniesActivity.this).load(companies.getLogo()).into(imgBottomSheetCompanyLogo);
        else
            Glide.with(CompaniesActivity.this).load(companies.getImage()).into(imgBottomSheetCompanyLogo);


        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void delete(Companies companies) {
        selectedCompany = companies;
        alertDialog.show();


    }

    private byte[] imageViewToByteArray(CircleImageView circleImageView) {
        Bitmap bitmap = ((BitmapDrawable) circleImageView.getDrawable()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void requestRuntimePermission() {
        ActivityCompat.requestPermissions(
                CompaniesActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_EXTERNAL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isExternalGranted = true;
                }
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = intent.getData();
            CropImage.activity(imageUri)
                    .setAspectRatio(3, 2)
                    .setMinCropWindowSize(100, 100)
                    .start(CompaniesActivity.this);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(intent);
            Uri croppedImageUri = result.getUri();

            File imageFile = new File(croppedImageUri.getPath());
            try {
                Bitmap bitmap = new Compressor(CompaniesActivity.this)
                        .setQuality(10)
                        .compressToBitmap(imageFile);

                imgBottomSheetCompanyLogo.setImageBitmap(bitmap);
            } catch (IOException ex) {

            }
        }
    }

    private void createDialog() {

        alertDialog = new AlertDialog.Builder(CompaniesActivity.this);
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Do you sure you want Delete this?");
        alertDialog.setIcon(R.drawable.ic_delete);

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OneCompany();
                viewModelCompany.delete(selectedCompany);
                adaptetRecyclerCompanies.setData(CompaniesActivity.this.companies);

                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public void getCompany() {
        Call<List<Companies>> call = jsonPlaceHolderApi.getApiAllCompanies();

        call.enqueue(new Callback<List<Companies>>() {
            @Override
            public void onResponse(Call<List<Companies>> call, Response<List<Companies>> response3) {
                if (!response3.isSuccessful()) {
                    return;
                }

                List<Companies> companies = response3.body();


                for (Companies company : CompaniesActivity.this.companies) {
                    viewModelCompany.delete(company);
                }
                for (Companies company : companies) {
                    viewModelCompany.insert(company);
                }

                adaptetRecyclerCompanies.setData(CompaniesActivity.this.companies);
            }

            @Override
            public void onFailure(Call<List<Companies>> call, Throwable t) {
                Toast.makeText(CompaniesActivity.this, "Get error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void DeleteCompany(int id) {
        Call<Void> call = jsonPlaceHolderApi.deleteCompany(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(CompaniesActivity.this, "Delete Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void OneCompany() {

        Call<List<Companies>> call = jsonPlaceHolderApi.getOneCompany();

        call.enqueue(new Callback<List<Companies>>() {
            @Override
            public void onResponse(Call<List<Companies>> call, Response<List<Companies>> response3) {
                if (!response3.isSuccessful()) {
                    Toast.makeText(CompaniesActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Companies> companies = response3.body();

                for (int i = 0; i < companies.size(); i++) {
                    if (companies.get(i).getCompanyName().equals(selectedCompany.getCompanyName())) {
                        CompanyID = companies.get(i).getCompanyApiID();
                        DeleteCompany(CompanyID);
                        return;
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Companies>> call, Throwable t) {
                Toast.makeText(CompaniesActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createCompany(String name, String location, File file, byte[] logo) {

        Companies company = new Companies(name, location, logo);


        RequestBody name1 = RequestBody.create(MediaType.parse("text/plain"), name);


        RequestBody location1 = RequestBody.create(MediaType.parse("text/plain"), location);


        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);


        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), fileBody);


        viewModelCompany.insert(company);


        Call<Companies> call = jsonPlaceHolderApi.createCompany(name1, location1, filePart);


        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(CompaniesActivity.this, response.body() + "", Toast.LENGTH_SHORT).show();

                    return;
                }
                Toast.makeText(CompaniesActivity.this, response.body() + "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                Toast.makeText(CompaniesActivity.this, "Add Successful", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void createCompanyWithoutImage(String name, String location) {


        Companies company = new Companies(name, location, null);


        Call<Companies> call = jsonPlaceHolderApi.createCompanyWithoutImage(company);


        viewModelCompany.insert(company);


        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(CompaniesActivity.this, response.body() + "", Toast.LENGTH_SHORT).show();

                    return;
                }
                Toast.makeText(CompaniesActivity.this, response.body() + "", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {
                Toast.makeText(CompaniesActivity.this, "Add Successful", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public File bittofile(Bitmap bitmap) throws IOException {
        //create a file to write bitmap data
        File f = new File(CompaniesActivity.this.getCacheDir(), Calendar.getInstance().getTime().toString());
        f.createNewFile();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }


    private void updateCompany(Companies company, File file) {


        RequestBody name1 = RequestBody.create(MediaType.parse("text/plain"), company.getCompanyName());


        RequestBody location1 = RequestBody.create(MediaType.parse("text/plain"), company.getLocation());


        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);


        MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", file.getName(), fileBody);


        Call<Companies> call = jsonPlaceHolderApi.updateCompany(company.getCompanyApiID(), name1, location1, filePart);


        call.enqueue(new Callback<Companies>() {
            @Override
            public void onResponse(Call<Companies> call, Response<Companies> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Companies> call, Throwable t) {

            }
        });


    }


    private void test(Uri uri) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileInputStream fis = new FileInputStream(new File(String.valueOf(uri)));

        byte[] buf = new byte[1024];
        int n;
        while (-1 != (n = fis.read(buf)))
            baos.write(buf, 0, n);

        byte[] videoBytes = baos.toByteArray();
    }


}
