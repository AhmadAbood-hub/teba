package com.example.rteba.View.Activity;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Custmers;
import com.example.rteba.ViewModel.ViewModelCustmer;
import com.example.tebavet.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivitiy extends AppCompatActivity
{
private JsonPlaceHolderApi jsonPlaceHolderApi;
private ViewModelCustmer viewModelCustmer;

private EditText Name;
private EditText Password;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Name = findViewById(R.id.editTextNameRegister);
        Password = findViewById(R.id.editTextPasswordRegister);



        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        viewModelCustmer = ViewModelProviders.of(RegisterActivitiy.this).get(ViewModelCustmer.class);


    }
    public void onLoginClick(View view)
    {



        if(Name.getText().toString().equals("")&&Password.getText().toString().equals(""))
        {
            Toast.makeText(RegisterActivitiy.this, "Please enter your data", Toast.LENGTH_SHORT).show();

        }
                   else if(Name.getText().toString().equals(""))
    {
        Toast.makeText(RegisterActivitiy.this, "Please enter the name", Toast.LENGTH_SHORT).show();

    }
                  else if(Password.getText().toString().equals(""))
    {
        Toast.makeText(RegisterActivitiy.this, "Please enter the password", Toast.LENGTH_SHORT).show();

    }

else

        {
            createCustmer(Name.getText().toString(),Password.getText().toString());
            startActivity(new Intent(this, LoginActivity.class));
            return;
        }







    }

    public void onClick(View view)
    {
        startActivity(new Intent(this, LoginActivity.class));
        return;

    }




    private void createCustmer(String custmerName,String custmerNumber)
    {
        Custmers custmer = new Custmers(custmerName,custmerNumber,0);


        Call<Custmers> call = jsonPlaceHolderApi.createCustmer(custmer);


        viewModelCustmer.insert(custmer);


        call.enqueue(new Callback<Custmers>()
        {
            @Override
            public void onResponse(Call<Custmers> call, Response<Custmers> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Custmers> call, Throwable t) {

            }
        });


    }


}
