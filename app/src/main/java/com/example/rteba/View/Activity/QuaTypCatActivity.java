package com.example.rteba.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.rteba.Adapter.AdaptetRecyclerCategories;
import com.example.rteba.Adapter.AdaptetRecyclerQuantities;
import com.example.rteba.Adapter.AdaptetRecyclerTypes;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Categories;
import com.example.rteba.Model.Entity.Quantities;
import com.example.rteba.Model.Entity.Types;
import com.example.tebavet.R;
import com.example.rteba.View.Dialog.DialogCategories;
import com.example.rteba.View.Dialog.DialogQuantities;
import com.example.rteba.View.Dialog.DialogTypes;
import com.example.rteba.ViewModel.ViewModelCategory;
import com.example.rteba.ViewModel.ViewModelQuantity;
import com.example.rteba.ViewModel.ViewModelType;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class QuaTypCatActivity extends AppCompatActivity
{

    JsonPlaceHolderApi jsonPlaceHolderApi;

    public int TypeId;
    public int CategoryId;
    public int QuantityId;



    public static Types selectedType;
    public static Quantities selectedQuantity;
    public static Categories selectedCategory;


    private ViewModelType viewModelType;
    private ViewModelQuantity viewModelQuantity;
    private ViewModelCategory viewModelCategory;


    private FloatingActionButton FabAddType;
    private FloatingActionButton FabAddQuantity;
    private FloatingActionButton FabAddCategory;

    private RecyclerView recyclerTypes;
    private RecyclerView.LayoutManager layoutManagerTypes;
    private AdaptetRecyclerTypes adaptetRecyclerTypes;
    private List<Types> types;


    private RecyclerView recyclerQuantites;
    private RecyclerView.LayoutManager layoutManagerQuantities;
    private AdaptetRecyclerQuantities adaptetRecyclerQuantities;
    private List<Quantities> quantities;


    private RecyclerView recyclerCategories;
    private RecyclerView.LayoutManager layoutManagerCategories;
    private AdaptetRecyclerCategories adaptetRecyclerCategories;
    private List<Categories> categories;



    private  AlertDialog.Builder alertDialogType;
    private  AlertDialog.Builder alertDialogQuantity;
    private  AlertDialog.Builder alertDialogCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qua_typ_cat);


        viewModelQuantity = ViewModelProviders.of(QuaTypCatActivity.this).get(ViewModelQuantity.class);
        viewModelQuantity.getQuantity().observe(QuaTypCatActivity.this, new Observer<List<Quantities>>() {
            @Override
            public void onChanged(@Nullable List<Quantities> quantities) {

                QuaTypCatActivity.this.quantities = quantities;
                adaptetRecyclerQuantities.setData(quantities);
            }
        });
        Retrofit retrofit=new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
         jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        getQuantity();
///////////////////////////////////////////////////////////////////////////////////////////////////////////

        viewModelCategory = ViewModelProviders.of(QuaTypCatActivity.this).get(ViewModelCategory.class);
        viewModelCategory.getCategory().observe(QuaTypCatActivity.this, new Observer<List<Categories>>() {
            @Override
            public void onChanged(@Nullable List<Categories> categories) {

                QuaTypCatActivity.this.categories = categories;
                adaptetRecyclerCategories.setData(categories);
            }
        });


        getCategory();




//////////////////////////////////////////////////////////////////////////////////////////////////////////
        viewModelType = ViewModelProviders.of(QuaTypCatActivity.this).get(ViewModelType.class);
        viewModelType.getTypes().observe(QuaTypCatActivity.this, new Observer<List<Types>>() {
            @Override
            public void onChanged(@Nullable List<Types> types) {

                QuaTypCatActivity.this.types = types;
                adaptetRecyclerTypes.setData(types);
            }
        });

        getType();

//////////////////////////////////////////////////////////////////////////////////////////////////
        FabAddType = (FloatingActionButton) findViewById(R.id.FabAddType);
        FabAddType.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogTypes dialogTypes = new DialogTypes();
                dialogTypes.show(getSupportFragmentManager(), "dialogTypes");
                DialogTypes.AddSave =false;
            }
        });

        FabAddQuantity = (FloatingActionButton) findViewById(R.id.FabAddQuantity);
        FabAddQuantity.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogQuantities dialogQuantities = new DialogQuantities();
                dialogQuantities.show(getSupportFragmentManager(), "dialogQuantities");
                DialogQuantities.AddSave =false;
            }
        });

        FabAddCategory = (FloatingActionButton) findViewById(R.id.FabAddCategory);
        FabAddCategory.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                DialogCategories dialogCategories = new DialogCategories();
                dialogCategories.show(getSupportFragmentManager(), "dialogCategories");
                DialogCategories.AddSave =false;
            }
        });

        recyclerCategories = (RecyclerView) findViewById(R.id.recyclerCategories);
        recyclerCategories.setHasFixedSize(true);
        layoutManagerCategories = new LinearLayoutManager(QuaTypCatActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(CustmersActivity.this, 2);
        recyclerCategories.setLayoutManager(layoutManagerCategories);
        categories = new ArrayList<>();
        adaptetRecyclerCategories = new AdaptetRecyclerCategories(QuaTypCatActivity.this, categories);
        recyclerCategories.setAdapter(adaptetRecyclerCategories);


        recyclerQuantites = (RecyclerView) findViewById(R.id.recyclerQuantites);
        recyclerQuantites.setHasFixedSize(true);
        layoutManagerQuantities = new LinearLayoutManager(QuaTypCatActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(CustmersActivity.this, 2);
        recyclerQuantites.setLayoutManager(layoutManagerQuantities);
        quantities = new ArrayList<>();
        adaptetRecyclerQuantities = new AdaptetRecyclerQuantities(QuaTypCatActivity.this, quantities);
        recyclerQuantites.setAdapter(adaptetRecyclerQuantities);


        recyclerTypes = (RecyclerView) findViewById(R.id.recyclerTypes);
        recyclerTypes.setHasFixedSize(true);
        layoutManagerTypes = new LinearLayoutManager(QuaTypCatActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(CustmersActivity.this, 2);
        recyclerTypes.setLayoutManager(layoutManagerTypes);
        types = new ArrayList<>();
        adaptetRecyclerTypes = new AdaptetRecyclerTypes(QuaTypCatActivity.this, types);
        recyclerTypes.setAdapter(adaptetRecyclerTypes);

        createDialogType();
        createDialogCategory();
        createDialogQuantity();
    }





    public  void deleteQuantity(Quantities quantities)
    {
        selectedQuantity =quantities;
        alertDialogQuantity.show();
    }
    public  void deleteType(Types types)
    {
        selectedType =types;
        alertDialogType.show();

    }
    public  void deleteCategory(Categories categories)
    {
        selectedCategory =categories;
        alertDialogCategory.show();

    }

    public void showDataType(Types types)
    {
        selectedType = types;
        DialogTypes dialogTypes = new DialogTypes();
        dialogTypes.setTypeName(selectedType.getName());
        dialogTypes.show(getSupportFragmentManager(), "dialogTypes");
        DialogTypes.AddSave =true;

    }

    public void showDataQuantity(Quantities quantities)
    {
        selectedQuantity = quantities;
        DialogQuantities dialogQuantities = new DialogQuantities();
        dialogQuantities.setQuantityQuantity(selectedQuantity.getQuantity());
        dialogQuantities.setQuantityUnit(selectedQuantity.getUnit());
        dialogQuantities.show(getSupportFragmentManager(), "dialogQuantities");
        DialogQuantities.AddSave =true;

    }

    public void showDataCategory(Categories category)
    {
        selectedCategory = category;
        DialogCategories dialogCategories = new DialogCategories();
        dialogCategories.setCategoryName(selectedCategory.getName());
        dialogCategories.show(getSupportFragmentManager(), "dialogCategories");
        DialogCategories.AddSave =true;

    }
    private void createDialogType()
    {

        alertDialogType = new AlertDialog.Builder(QuaTypCatActivity.this);
        alertDialogType.setTitle("Confirm Delete...");
        alertDialogType.setMessage("Do you sure you want Delete this?");
        alertDialogType.setIcon(R.drawable.ic_delete);

        alertDialogType.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                OneType();
                viewModelType.delete(selectedType);
                adaptetRecyclerTypes.setData(types);

                dialog.dismiss();
            }
        });
        alertDialogType.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {


                dialog.dismiss();
            }
        });
    }


    private void createDialogQuantity()
    {

        alertDialogQuantity = new AlertDialog.Builder(QuaTypCatActivity.this);
        alertDialogQuantity.setTitle("Confirm Delete...");
        alertDialogQuantity.setMessage("Do you sure you want Delete this?");
        alertDialogQuantity.setIcon(R.drawable.ic_delete);

        alertDialogQuantity.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {

                getOneQuantity();
                viewModelQuantity.delete(selectedQuantity);
                adaptetRecyclerQuantities.setData(quantities);

                dialog.dismiss();
            }
        });
        alertDialogQuantity.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {


                dialog.dismiss();
            }
        });
    }



    private void createDialogCategory()
    {

        alertDialogCategory = new AlertDialog.Builder(QuaTypCatActivity.this);
        alertDialogCategory.setTitle("Confirm Delete...");
        alertDialogCategory.setMessage("Do you sure you want Delete this?");
        alertDialogCategory.setIcon(R.drawable.ic_delete);

        alertDialogCategory.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                getOneCategory();
                viewModelCategory.delete(selectedCategory);
                adaptetRecyclerCategories.setData(categories);

                 dialog.dismiss();
            }
        });
        alertDialogCategory.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {


                dialog.dismiss();
                   }
        });
    }



    public void getQuantity()
    {

        Call<List<Quantities>> callQuantities = jsonPlaceHolderApi.getApiAllQuantities();

        callQuantities.enqueue(new Callback<List<Quantities>>()
        {
            @Override
            public void onResponse(Call<List<Quantities>> call, Response<List<Quantities>> response2)
            {
                if (!response2.isSuccessful())
                {
                    return;
                }
                List<Quantities> quantities = response2.body();



                    for(Quantities quantities1 : QuaTypCatActivity.this.quantities)
                    {

                        viewModelQuantity.delete(quantities1);

                    }

                    for(Quantities QuantityApi : quantities)
                    {
                        viewModelQuantity.insert(QuantityApi);
                    }

                adaptetRecyclerQuantities.setData(QuaTypCatActivity.this.quantities);

            }

            @Override
            public void onFailure(Call<List<Quantities>> call, Throwable t)
            {
            }
        });
    }
    public void getOneQuantity()
    {


        Call<List<Quantities>> callTypes = jsonPlaceHolderApi.getOneQuantities();

        callTypes.enqueue(new Callback<List<Quantities>>()
        {
            @Override
            public void onResponse(Call<List<Quantities>> call, Response<List<Quantities>> response3)
            {
                if (!response3.isSuccessful())
                {
                    Toast.makeText(QuaTypCatActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Quantities> quantities = response3.body();

                for(int i=0 ;i<quantities.size();i++)
                {
                    if(quantities.get(i).getQuantity()==selectedQuantity.getQuantity() )
                    {
                        QuantityId =quantities.get(i).getId();
                        DeleteQuantity(QuantityId);
                        return;
                    }

                }
            }
            @Override
            public void onFailure(Call<List<Quantities>> call, Throwable t)
            {
                Toast.makeText(QuaTypCatActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void DeleteQuantity(int id)
    {
        Call<Void> call =jsonPlaceHolderApi.deleteQuantity(id);
        call.enqueue(new Callback<Void>()
        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t)
            {

            }
        });
    }


    public void getCategory()
    {  Call<List<Categories>> callCategory = jsonPlaceHolderApi.getApiAllCategories();

        callCategory.enqueue(new Callback<List<Categories>>()
        {
            @Override
            public void onResponse(Call<List<Categories>> call, Response<List<Categories>> response1)
            {
                if (!response1.isSuccessful())
                {
                    return;
                }
                List<Categories> categories = response1.body();


                    for(Categories categor : QuaTypCatActivity.this.categories)
                    {
                        viewModelCategory.delete(categor);
                    }
                    for(Categories category : categories)
                    {
                        viewModelCategory.insert(category);
                    }



                adaptetRecyclerCategories.setData(QuaTypCatActivity.this.categories);

            }

            @Override
            public void onFailure(Call<List<Categories>> call, Throwable t)
            {
            }
        });
    }
    public void DeleteCategory(int id)
    {
        Call<Void> call =jsonPlaceHolderApi.deleteCategory(id);
        call.enqueue(new Callback<Void>()
        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t)
            {

            }
        });
    }
    public void getOneCategory()
    {


        Call<List<Categories>> callTypes = jsonPlaceHolderApi.getOneCategory();

        callTypes.enqueue(new Callback<List<Categories>>()
        {
            @Override
            public void onResponse(Call<List<Categories>> call, Response<List<Categories>> response3)
            {
                if (!response3.isSuccessful())
                {
                    Toast.makeText(QuaTypCatActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Categories> categories = response3.body();

                for(int i=0 ;i<categories.size();i++)
                {
                    if(categories.get(i).getName().equals(selectedCategory.getName()) )
                    {
                        CategoryId =categories.get(i).getId();
                        DeleteCategory(CategoryId);
                        return;
                    }

                }
            }
            @Override
            public void onFailure(Call<List<Categories>> call, Throwable t)
            {
                Toast.makeText(QuaTypCatActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getType()
    {
        Call<List<Types>> callTypes = jsonPlaceHolderApi.getApiTypes();

        callTypes.enqueue(new Callback<List<Types>>()
        {
            @Override
            public void onResponse(Call<List<Types>> call, Response<List<Types>> response3)
            {
                if (!response3.isSuccessful())
                {
                    return;
                }
                List<Types> types = response3.body();


                    for (Types Type : QuaTypCatActivity.this.types)
                    {
                        viewModelType.delete(Type);
                    }
                    for (Types TypeApi : types) {
                        viewModelType.insert(TypeApi);
                    }

                adaptetRecyclerTypes.setData(QuaTypCatActivity.this.types);
            }
            @Override
            public void onFailure(Call<List<Types>> call, Throwable t)
            {
            }
        });
    }
    public void DeleteType(int id)
    {
        Call<Void> call =jsonPlaceHolderApi.deleteType(id);
        call.enqueue(new Callback<Void>()
        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t)
            {

            }
        });
    }
    public void OneType()
    {


        Call<List<Types>> callTypes = jsonPlaceHolderApi.getOneType();

        callTypes.enqueue(new Callback<List<Types>>()
        {
            @Override
            public void onResponse(Call<List<Types>> call, Response<List<Types>> response3)
            {
                if (!response3.isSuccessful())
                {
                    Toast.makeText(QuaTypCatActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
             List<Types> types = response3.body();

                for(int i=0 ;i<types.size();i++)
                {
                    if(types.get(i).getName().equals(selectedType.getName()) )
                    {
                        TypeId =types.get(i).getId();
                        DeleteType(TypeId);

                        return;
                    }

                }
            }
            @Override
            public void onFailure(Call<List<Types>> call, Throwable t)
            {
                Toast.makeText(QuaTypCatActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }




}
