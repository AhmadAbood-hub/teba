package com.example.rteba.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.rteba.Adapter.AdaptetRecyclerRepository;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Repositories;
import com.example.tebavet.R;
import com.example.rteba.View.Dialog.DialogRepository;
import com.example.rteba.ViewModel.ViewModelRepository;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RepActivity extends AppCompatActivity
{
    public static Repositories selectedRepository;

    public int RepID;

    private ViewModelRepository viewModelRepository;

    private JsonPlaceHolderApi jsonPlaceHolderApi;

    private FloatingActionButton FabAddRepository;

    private RecyclerView recyclerRepositories;
    private RecyclerView.LayoutManager layoutManagerRepositories;
    private AdaptetRecyclerRepository adaptetRecyclerRepositories;
    private List<Repositories> repositories;








    private  AlertDialog.Builder alertDialogRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rep);


        recyclerRepositories = (RecyclerView) findViewById(R.id.recyclerRepositories);
        recyclerRepositories.setHasFixedSize(true);
        layoutManagerRepositories = new LinearLayoutManager(RepActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(CustmersActivity.this, 2);
        recyclerRepositories.setLayoutManager(layoutManagerRepositories);
        repositories = new ArrayList<>();
        adaptetRecyclerRepositories = new AdaptetRecyclerRepository(RepActivity.this, repositories);
        recyclerRepositories.setAdapter(adaptetRecyclerRepositories);




        viewModelRepository = ViewModelProviders.of(RepActivity.this).get(ViewModelRepository.class);
        viewModelRepository.getRepository().observe(RepActivity.this, new Observer<List<Repositories>>() {
            @Override
            public void onChanged(@Nullable List<Repositories> repositories)
            {

                RepActivity.this.repositories = repositories;
                adaptetRecyclerRepositories.setData(repositories);
            }
        });
        Retrofit retrofit=new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        getRepository();

        FabAddRepository = (FloatingActionButton) findViewById(R.id.FabAddRepository);
        FabAddRepository.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {


                DialogRepository dialogRepository = new DialogRepository();
                dialogRepository.show(getSupportFragmentManager(), "dialogRepository");
                DialogRepository.AddSave =false;
            }
        });
        createDialogRepository();
    }
    public  void deleteRepository(Repositories repositories)
    {
        selectedRepository =repositories;
        alertDialogRepository.show();

    }

    public void showDataRepository(Repositories  repositories)
    {
        selectedRepository = repositories;
        DialogRepository dialogRepository = new DialogRepository();
        dialogRepository.setRepositoryLocation(selectedRepository.getLocation());
        dialogRepository.setRepositoryAnnualRent(selectedRepository.getAnnualrent());
        dialogRepository.show(getSupportFragmentManager(), "dialogRepository");
        DialogRepository.AddSave =true;

    }
    private void createDialogRepository()
    {

        alertDialogRepository = new AlertDialog.Builder(RepActivity.this);
        alertDialogRepository.setTitle("Confirm Delete...");
        alertDialogRepository.setMessage("Do you sure you want Delete this?");
        alertDialogRepository.setIcon(R.drawable.ic_delete);

        alertDialogRepository.setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                OneRepository();
                viewModelRepository.delete(selectedRepository);
                adaptetRecyclerRepositories.setData(repositories);

                dialog.dismiss();
            }
        });
        alertDialogRepository.setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {


                dialog.dismiss();
            }
        });
    }



    public void getRepository()
    {
        Call<List<Repositories>> callTypes = jsonPlaceHolderApi.getApiAllRepositories();

        callTypes.enqueue(new Callback<List<Repositories>>()
        {
            @Override
            public void onResponse(Call<List<Repositories>> call, Response<List<Repositories>> response3)
            {
                if (!response3.isSuccessful())
                {
                    return;
                }
                List<Repositories> repositories = response3.body();


                    for (Repositories repository : RepActivity.this.repositories)
                    {
                        viewModelRepository.delete(repository);
                    }
                    for (Repositories repository : repositories) {
                        viewModelRepository.insert(repository);
                    }

                adaptetRecyclerRepositories.setData(RepActivity.this.repositories);
            }
            @Override
            public void onFailure(Call<List<Repositories>> call, Throwable t)
            {
            }
        });
    }
    public void DeleteRepository(int id)
    {
        Call<Void> call =jsonPlaceHolderApi.deleteRepository(id);
        call.enqueue(new Callback<Void>()
        {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t)
            {

            }
        });
    }
    public void OneRepository()
    {

        Call<List<Repositories>> callTypes = jsonPlaceHolderApi.getOneRepository();

        callTypes.enqueue(new Callback<List<Repositories>>()
        {
            @Override
            public void onResponse(Call<List<Repositories>> call, Response<List<Repositories>> response3)
            {
                if (!response3.isSuccessful())
                {
                    Toast.makeText(RepActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Repositories> repositories = response3.body();

                for(int i=0 ;i<repositories.size();i++)
                {
                    if(repositories.get(i).getLocation().equals(selectedRepository.getLocation()) )
                    {
                        RepID =repositories.get(i).getId();
                        DeleteRepository(RepID);

                        return;
                    }

                }
            }
            @Override
            public void onFailure(Call<List<Repositories>> call, Throwable t)
            {
                Toast.makeText(RepActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
