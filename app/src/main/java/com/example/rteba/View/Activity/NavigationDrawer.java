package com.example.rteba.View.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.rteba.View.Activity.CompaniesActivity;
import com.example.rteba.View.Activity.CustmersActivity;
import com.example.rteba.View.Activity.EmpActivity;
import com.example.rteba.View.Activity.JobsActivity;
import com.example.rteba.View.Activity.LoginActivity;
import com.example.rteba.View.Activity.QuaTypCatActivity;
import com.example.rteba.View.Activity.RepActivity;
import com.example.rteba.View.Fragment.BillsFragment;

import com.example.rteba.View.Fragment.SearchFragment;
import com.example.tebavet.R;

import static com.example.rteba.View.Fragment.BillsFragment.Cus;

public class NavigationDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        //  Menu nav_Menu = navigationView.getMenu();
        //   nav_Menu.findItem(R.id.nav_Custmers).setVisible(false);
        //   nav_Menu.findItem(R.id.nav_Employees).setVisible(false);
        //   nav_Menu.findItem(R.id.nav_Repository).setVisible(false);
        //   nav_Menu.findItem(R.id.nav_home).setVisible(false);
        //   nav_Menu.findItem(R.id.nav_InformationAboutItem).setVisible(false);
        //   nav_Menu.findItem(R.id.nav_Jobs).setVisible(false);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);


        NavigationView navigationView = findViewById(R.id.nav_view);


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if (Cus.getState() == 0)
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        navigationView.setNavigationItemSelectedListener(this);


        BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new BillsFragment()).commit();


    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem Item) {
            Fragment selectedFragment = null;
            switch (Item.getItemId()) {
                case R.id.nav_home:
                    selectedFragment = new BillsFragment();
                    break;

            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }

    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.Logout) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, CompaniesActivity.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_Custmers) {

            Intent intent = new Intent(this, CustmersActivity.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_Jobs) {
            Intent intent = new Intent(this, JobsActivity.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_InformationAboutItem) {
            Intent intent = new Intent(this, QuaTypCatActivity.class);
            this.startActivity(intent);
        } else if (id == R.id.nav_Employees) {

            Intent intent = new Intent(this, EmpActivity.class);
            this.startActivity(intent);


        } else if (id == R.id.nav_Repository) {

            Intent intent = new Intent(this, RepActivity.class);
            this.startActivity(intent);


        } else if (id == R.id.nav_Close) {
            Intent intent = new Intent(this, LoginActivity.class);
            this.startActivity(intent);
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
