package com.example.rteba.View.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.rteba.Adapter.AdaptetRecyclerItems;
import com.example.rteba.Adapter.AdaptetRecyclerItemsForCustomer;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Categories;
import com.example.rteba.Model.Entity.Companies;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.Model.Entity.Quantities;
import com.example.rteba.Model.Entity.Repositories;
import com.example.rteba.Model.Entity.Types;
import com.example.rteba.Model.Entity.body;
import com.example.rteba.Model.Entity.notification;
import com.example.tebavet.R;
import com.example.rteba.View.Dialog.DialogDatePicker;
import com.example.rteba.ViewModel.ViewModelCategory;
import com.example.rteba.ViewModel.ViewModelCompany;
import com.example.rteba.ViewModel.ViewModelItem;
import com.example.rteba.ViewModel.ViewModelQuantity;
import com.example.rteba.ViewModel.ViewModelRepository;
import com.example.rteba.ViewModel.ViewModelType;
import com.google.firebase.messaging.FirebaseMessaging;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.rteba.View.Activity.CompaniesActivity.selectedCompany;

public class ItemsActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private JsonPlaceHolderApi jsonPlaceHolderApi;


    Boolean BolRepository;
    Boolean BolType;
    Boolean BolSize;
    Boolean BolUnit;
    Boolean BolCategory;

    private List<Repositories> Rep;
    private List<Types> Typ;
    private List<Quantities> Qun;
    private List<Categories> Cat;


    private int CompanyID;
    private int CompanyApiID;
    ViewModelCompany viewModelCompany;
    ViewModelQuantity viewModelQuantity;
    ViewModelCategory viewModelCategory;
    ViewModelRepository viewModelRepository;

    ViewModelType viewModelType;

    ViewModelItem viewModelItem;
    private List<String> Unit;
    private List<String> Size;
    private List<String> Repository;
    private List<String> Category;
    private List<String> Type;

    private CircleImageView imgDialogCompany;

    private boolean isExternalGranted = false;
    private final int REQUEST_EXTERNAL = 1000;
    private final int GALLERY_PICK = 2000;
    private byte[] ItemImage;

    private int RepositoryID;
    private int CategoryID;
    private int QuantityID;


    private String strExpirStart;
    private String strExpirEnd;
    private boolean StartDate;
    private boolean EndDate;


    private FloatingActionButton fabItems;

    private Spinner spDialogRepository;
    private Spinner spDialogCategory;
    private Spinner spDialogType;
    private Spinner spDialogQuantity;
    private Spinner spDialogUnit;


    private String spRepository;
    private String spCategory;
    private String spType;
    private String spSize;
    private String spUnit;


    private RelativeLayout bottomSheetItem;
    private BottomSheetBehavior bottomSheetBehavior;

    Button btnBottomSheetItemAdd;
    Button btnBottomSheetItemSave;

    TextView txtExpirStart;
    TextView txtExpirEnd;


    private RecyclerView recyclerItems;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerItems adaptetRecyclerItems;

    private List<Items> items;
    private List<Items> itemsApi;


    private AlertDialog.Builder alertDialog;

    public static Items SelectItem;


    private TextInputEditText edtBottomSheetItemName;
    private TextInputEditText edtBottomSheetItemCost;
    private TextInputEditText edtBottomSheetItemCount;

    private ImageView imgItemsCompanyImage;


    String NameTypeItem;
    String NameRepositoryItem;
    String NameCategoryItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);


        FirebaseMessaging.getInstance().subscribeToTopic("all");


        recyclerItems = (RecyclerView) findViewById(R.id.recyclerItems);
        recyclerItems.setHasFixedSize(true);
        //layoutManager = new LinearLayoutManager(CategoriesActivity.this, LinearLayoutManager.HORIZONTAL, false);
        layoutManager = new GridLayoutManager(ItemsActivity.this, 2);
        recyclerItems.setLayoutManager(layoutManager);
        items = new ArrayList<>();
        itemsApi = new ArrayList<>();
        adaptetRecyclerItems = new AdaptetRecyclerItems(ItemsActivity.this, itemsApi);


        recyclerItems.setAdapter(adaptetRecyclerItems);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();


        CompanyApiID = bundle.getInt("CompanyApiID");
        CompanyID = bundle.getInt("CompanyID");


        viewModelItem = ViewModelProviders.of(this).get(ViewModelItem.class);

        viewModelItem.getItems(CompanyApiID).observe(ItemsActivity.this, new Observer<List<Items>>() {
            @Override
            public void onChanged(@Nullable List<Items> items) {
                ItemsActivity.this.items = items;
                adaptetRecyclerItems.setData(ItemsActivity.this.items);
            }
        });


        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        getItems(CompanyApiID);


        viewModelQuantity = ViewModelProviders.of(this).get(ViewModelQuantity.class);
        viewModelCategory = ViewModelProviders.of(this).get(ViewModelCategory.class);
        viewModelRepository = ViewModelProviders.of(this).get(ViewModelRepository.class);
        viewModelType = ViewModelProviders.of(this).get(ViewModelType.class);
        btnBottomSheetItemAdd = (Button) findViewById(R.id.btnBottomSheetItemAdd);
        btnBottomSheetItemSave = (Button) findViewById(R.id.btnBottomSheetItemSave);
        final Button btnExpirStart = (Button) findViewById(R.id.btnExpirStart);
        fabItems = findViewById(R.id.fabItems);
        txtExpirStart = findViewById(R.id.txtExpirStart);
        txtExpirEnd = findViewById(R.id.txtExpirEnd);
        final Button btnExpirEnd = (Button) findViewById(R.id.btnExpirEnd);
        edtBottomSheetItemName = (TextInputEditText) findViewById(R.id.edtBottomSheetItemName);
        edtBottomSheetItemCount = (TextInputEditText) findViewById(R.id.edtBottomSheetItemCount);
        edtBottomSheetItemCost = (TextInputEditText) findViewById(R.id.edtBottomSheetItemCost);
        spDialogRepository = (Spinner) findViewById(R.id.spDialogRepository);
        spDialogQuantity = (Spinner) findViewById(R.id.spDialogQuantity);
        spDialogUnit = (Spinner) findViewById(R.id.spDialogUnit);
        spDialogCategory = (Spinner) findViewById(R.id.spDialogCategory);
        spDialogType = (Spinner) findViewById(R.id.spDialogType);
        imgDialogCompany = (CircleImageView) findViewById(R.id.imgDialogCompany);
        imgItemsCompanyImage = (ImageView) findViewById(R.id.imgItemsCompanyImage);
        Glide.with(ItemsActivity.this).load(R.drawable.a).into(imgDialogCompany);

        bottomSheetItem = (RelativeLayout) findViewById(R.id.bottomSheetItem);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetItem);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                    fabItems.setVisibility(View.GONE);
                } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED ||
                        bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    edtBottomSheetItemName.setText("");
                    edtBottomSheetItemCost.setText("");
                    edtBottomSheetItemCount.setText("");
                    fabItems.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });

        fabItems = (FloatingActionButton) findViewById(R.id.fabItems);
        fabItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)////////////////////////////////////////////////////////////////////////////////////////////////////////////
            {
                Clear();

                getRepository();
                getQuantity();
                getType();
                getCategory();

                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                Glide.with(ItemsActivity.this).load(R.drawable.a).into(imgDialogCompany);
                btnBottomSheetItemAdd.setVisibility(View.VISIBLE);
                btnBottomSheetItemSave.setVisibility(View.GONE);

            }
        });


        Size = new ArrayList<>();
        Unit = new ArrayList<>();
        Repository = new ArrayList<>();
        Category = new ArrayList<>();
        Type = new ArrayList<>();


        viewModelType.getName().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings) {
                Type.clear();
                Type.add("");
                for (String a : strings) {
                    Type.add(a + "");
                }
            }
        });
        Type.add("");
        ArrayAdapter adapterType = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Type);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogType.setAdapter(adapterType);


        viewModelCategory.getName().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings) {
                Category.clear();
                Category.add("");
                for (String a : strings) {
                    Category.add(a + "");
                }
            }
        });
        Category.add("");
        ArrayAdapter adapterCategory = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Category);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogCategory.setAdapter(adapterCategory);


        viewModelRepository.getLocation().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings) {
                Repository.clear();
                Repository.add("");
                for (String a : strings) {
                    Repository.add(a + "");
                }
            }
        });
        Repository.add("");
        ArrayAdapter adapterRepository = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Repository);
        adapterRepository.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogRepository.setAdapter(adapterRepository);


        viewModelQuantity.getSize().observe(this, new Observer<List<Integer>>() {
            @Override
            public void onChanged(@Nullable List<Integer> integers) {
                Size.clear();
                Size.add("");
                for (int a : integers) {
                    Size.add(a + "");
                }
            }
        });
        Size.add("");
        ArrayAdapter adapterSize = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Size);
        adapterSize.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogQuantity.setAdapter(adapterSize);


        viewModelQuantity.getUnit().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings) {
                Unit.clear();
                Unit.add("");
                for (String a : strings) {
                    Unit.add(a + "");
                }
            }
        });
        Unit.add("");
        ArrayAdapter adapterUnit = new ArrayAdapter(this, android.R.layout.simple_spinner_item, Unit);
        adapterUnit.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogUnit.setAdapter(adapterUnit);


        viewModelCompany = ViewModelProviders.of(this).get(ViewModelCompany.class);
        viewModelCompany.getOneCompany(CompanyID).observe(ItemsActivity.this, new Observer<Companies>() {
            @Override
            public void onChanged(@Nullable Companies companies) {
                try {
                    Glide.with(ItemsActivity.this).load(selectedCompany.getLogo()).into(imgItemsCompanyImage);
                } catch (Exception e) {

                }
            }
        });


        if (ContextCompat.checkSelfPermission(ItemsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                ContextCompat.checkSelfPermission(ItemsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            requestRuntimePermission();
        } else {
            isExternalGranted = true;
        }


        btnBottomSheetItemAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spCategory = spDialogCategory.getSelectedItem().toString();
                spSize = spDialogQuantity.getSelectedItem().toString();
                spType = spDialogType.getSelectedItem().toString();
                spRepository = spDialogRepository.getSelectedItem().toString();
                spUnit = spDialogUnit.getSelectedItem().toString();

                if (spCategory.equals("") || spSize.equals("") || spType.equals("") || spRepository.equals("") || spUnit.equals("")) {
                    Toast.makeText(ItemsActivity.this, "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }

                viewModelCategory.getID(spCategory + "").observe(ItemsActivity.this, new Observer<Integer>() {
                    @Override
                    public void onChanged(@Nullable Integer categoryID) {
                        CategoryID = categoryID;
                    }
                });
                viewModelQuantity.getID(Integer.parseInt(spSize + ""), spUnit + "").observe(ItemsActivity.this, new Observer<Integer>() {
                    @Override
                    public void onChanged(@Nullable Integer quantityID) {
                        if (quantityID == null) {
                            return;

                        } else {
                            QuantityID = quantityID;
                        }
                    }
                });
                viewModelRepository.getID(spRepository + "").observe(ItemsActivity.this, new Observer<Integer>() {

                    @Override
                    public void onChanged(@Nullable Integer repositoryID) {

                        RepositoryID = repositoryID;
                    }
                });
                viewModelType.getID(spType + "").observe(ItemsActivity.this, new Observer<Integer>() {
                    @Override
                    public void onChanged(@Nullable Integer typeID) {
                        final String name = edtBottomSheetItemName.getText().toString();
                        String cost = edtBottomSheetItemCost.getText().toString();
                        String count = edtBottomSheetItemCount.getText().toString();
                        try {
                            Bitmap bitmap = ((BitmapDrawable) imgDialogCompany.getDrawable()).getBitmap();
                            if (bitmap == null || name.equals("") || count.equals("") || cost.equals("") || spDialogCategory.equals("") || spDialogQuantity.equals("") || spDialogUnit.equals("") || spDialogType.equals("")) {
                                Toast.makeText(ItemsActivity.this, "Please Enter Data", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            ItemImage = imageViewToByteArray(imgDialogCompany);
                            if (RepositoryID != 0) {
                                createItem(CompanyApiID, CategoryID, Integer.parseInt(cost), ItemImage,
                                        bittofile(((BitmapDrawable) imgDialogCompany.getDrawable()).getBitmap()),
                                        RepositoryID, name, typeID, strExpirStart, strExpirEnd, QuantityID, Integer.parseInt(count), 0);
                                Toast.makeText(ItemsActivity.this, "Pleas wait...", Toast.LENGTH_LONG).show();

                                viewModelCompany.getOneCompany(CompanyApiID).observe(ItemsActivity.this, new Observer<Companies>() {
                                    @Override
                                    public void onChanged(@Nullable Companies companies) {
                                        sendNotification(name, companies.getCompanyName());
                                    }
                                });
                                Intent intent = new Intent(ItemsActivity.this, CompaniesActivity.class);
                                startActivity(intent);
                            } else
                                Toast.makeText(ItemsActivity.this, "Sorry...try again", Toast.LENGTH_SHORT).show();

                        } catch (Exception ex) {
                            Toast.makeText(ItemsActivity.this, "Please Enter Data", Toast.LENGTH_SHORT).show();
                        }

                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);


                    }
                });


            }
        });


        btnBottomSheetItemSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (spDialogCategory.getSelectedItem().toString().equals("") || spDialogQuantity.getSelectedItem().toString().equals("") || spDialogType.getSelectedItem().toString().equals("") || spDialogUnit.getSelectedItem().toString().equals("") || edtBottomSheetItemCost.getText().toString() == "" || edtBottomSheetItemName.getText().toString().equals("") || edtBottomSheetItemCount.getText().toString().equals("")) {
                        Toast.makeText(ItemsActivity.this, "Please Enter Data", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    viewModelCategory.getID(spDialogCategory.getSelectedItem().toString() + "").observe(ItemsActivity.this, new Observer<Integer>() {
                        @Override
                        public void onChanged(@Nullable Integer categoryID) {

                            SelectItem.setCategoryID(categoryID);
                            viewModelQuantity.getID(Integer.parseInt(spDialogQuantity.getSelectedItem().toString() + ""), spDialogUnit.getSelectedItem().toString() + "").observe(ItemsActivity.this, new Observer<Integer>() {
                                @Override
                                public void onChanged(@Nullable Integer quantityID) {
                                    if (quantityID == null) {
                                        return;
                                    } else {
                                        SelectItem.setQuantityID(quantityID);
                                    }
                                    viewModelRepository.getID(spDialogRepository.getSelectedItem().toString() + "").observe(ItemsActivity.this, new Observer<Integer>() {
                                        @Override
                                        public void onChanged(@Nullable Integer repositoryID) {

                                            SelectItem.setRepositoryID(repositoryID);

                                            viewModelType.getID(spDialogType.getSelectedItem().toString() + "").observe(ItemsActivity.this, new Observer<Integer>() {
                                                @Override
                                                public void onChanged(@Nullable Integer typeID) {
                                                    SelectItem.setTypeID(typeID);


                                                    SelectItem.setItemName(edtBottomSheetItemName.getText().toString());
                                                    SelectItem.setCost(Integer.parseInt(edtBottomSheetItemCost.getText().toString()));
                                                    SelectItem.setCount(Integer.parseInt(edtBottomSheetItemCount.getText().toString()));
                                                    SelectItem.setExpirStart(txtExpirStart.getText().toString());
                                                    SelectItem.setExpirEnd(txtExpirEnd.getText().toString());
                                                    ItemImage = imageViewToByteArray(imgDialogCompany);

                                                    SelectItem.setImg(ItemImage);


                                                    try {
                                                        updateItem(SelectItem.getItemApiID(), SelectItem.getCategoryID(), SelectItem.getCost()
                                                                , bittofile(((BitmapDrawable) imgDialogCompany.getDrawable()).getBitmap())
                                                                , SelectItem.getRepositoryID(), SelectItem.getItemName(), SelectItem.getTypeID(),
                                                                txtExpirStart.getText().toString(), txtExpirEnd.getText().toString(), SelectItem.getQuantityID(),
                                                                SelectItem.getCount()
                                                        );
                                                        Toast.makeText(ItemsActivity.this, "Please wait ...", Toast.LENGTH_LONG).show();
                                                        Toast.makeText(ItemsActivity.this, "Successful update ", Toast.LENGTH_LONG).show();
                                                        Intent intent = new Intent(ItemsActivity.this, CompaniesActivity.class);
                                                        startActivity(intent);

                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }

                                                    viewModelItem.update(SelectItem);

                                                }
                                            });


                                        }
                                    });
                                }
                            });
                        }
                    });


                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                } catch (Exception e) {

                }

            }
        });


        imgDialogCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isExternalGranted) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, GALLERY_PICK);
                } else {
                    Toast.makeText(ItemsActivity.this, "Permissions denied", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnExpirStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment datePicker = new DialogDatePicker();
                datePicker.show(getSupportFragmentManager(), "datepicker");
                StartDate = true;


            }
        });

        btnExpirEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment datePicker = new DialogDatePicker();
                datePicker.show(getSupportFragmentManager(), "datepicker");
                EndDate = true;

            }
        });


        viewModelRepository.getRepository().observe(ItemsActivity.this, new Observer<List<Repositories>>() {
            @Override
            public void onChanged(@Nullable List<Repositories> repositories) {
                Rep = repositories;
            }
        });

        viewModelCategory.getCategory().observe(ItemsActivity.this, new Observer<List<Categories>>() {
            @Override
            public void onChanged(@Nullable List<Categories> categories) {
                Cat = categories;
            }
        });

        viewModelQuantity.getQuantity().observe(ItemsActivity.this, new Observer<List<Quantities>>() {
            @Override
            public void onChanged(@Nullable List<Quantities> quantities) {
                Qun = quantities;
            }
        });

        viewModelType.getTypes().observe(ItemsActivity.this, new Observer<List<Types>>() {
            @Override
            public void onChanged(@Nullable List<Types> types) {
                Typ = types;
            }
        });

        createDialog(SelectItem);


    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);

        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        if (StartDate) {
            strExpirStart = currentDateString;
            StartDate = false;
            txtExpirStart.setText(strExpirStart + "");

        } else if (EndDate) {
            strExpirEnd = currentDateString;
            EndDate = false;
            txtExpirEnd.setText(strExpirEnd + "");
        }

    }

    public void showData(Items item)///////////////////////////////////////////////////////////////////////////////////////////////
    {


        Glide.with(ItemsActivity.this).load(item.getUrl()).into(imgDialogCompany);


        BolRepository = true;
        getRepository();

        BolUnit = true;
        BolSize = true;
        getQuantity();

        BolType = true;
        getType();

        BolCategory = true;
        getCategory();


        SelectItem = item;
        btnBottomSheetItemSave.setVisibility(View.VISIBLE);
        btnBottomSheetItemAdd.setVisibility(View.GONE);
        txtExpirStart.setText(SelectItem.getExpirStart());
        txtExpirEnd.setText(SelectItem.getExpirEnd());
        edtBottomSheetItemName.setText(item.getItemName());
        edtBottomSheetItemCost.setText(item.getCost() + "");
        edtBottomSheetItemCount.setText(item.getCount() + "");


        viewModelType.getOneName(item.getTypeID() + "").observe(ItemsActivity.this, new Observer<String>() {

            @Override
            public void onChanged(@Nullable String s) {
                NameTypeItem = s;
                int bb = 0;
                if (BolType) {
                    for (int i = 0; i < Type.size(); i++) {

                        if (Type.get(i).equals(NameTypeItem)) {
                            bb = i;

                        }

                    }
                    spDialogType.setSelection(bb);

                }

                BolType = false;
            }
        });


        viewModelRepository.getOneLocation(item.getRepositoryID()).observe(ItemsActivity.this, new Observer<String>() {

            @Override
            public void onChanged(@Nullable String s) {

                NameRepositoryItem = s;
                int bb = 0;
                if (BolRepository) {
                    for (int i = 0; i < Repository.size(); i++) {
                        if (Repository.get(i).equals(NameRepositoryItem)) {
                            bb = i;
                        }
                    }

                    spDialogRepository.setSelection(bb);
                }
                BolRepository = false;
            }
        });


        viewModelCategory.getOneName(item.getCategoryID() + "").observe(ItemsActivity.this, new Observer<String>() {

            @Override
            public void onChanged(@Nullable String s) {
                NameCategoryItem = s;
                int bb = 0;
                if (BolCategory) {
                    for (int i = 0; i < Category.size(); i++) {

                        if (Category.get(i).equals(NameCategoryItem)) {
                            bb = i;

                        }

                    }
                    spDialogCategory.setSelection(bb);
                }
                BolCategory = false;
            }
        });


        viewModelQuantity.getOneSize(item.getQuantityID() + "").observe(ItemsActivity.this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                int bb = 0;
                if (BolSize) {
                    for (int i = 0; i < Size.size(); i++) {

                        if (Size.get(i).equals(integer + "")) {
                            bb = i;

                        }

                    }
                    spDialogQuantity.setSelection(bb);

                }

                BolSize = false;
            }
        });


        viewModelQuantity.getOneUnit(item.getQuantityID() + "").observe(ItemsActivity.this, new Observer<String>() {

            @Override
            public void onChanged(@Nullable String s) {
                int bb = 0;
                if (BolUnit) {
                    for (int i = 0; i < Unit.size(); i++) {

                        if (Unit.get(i).equals(s)) {
                            bb = i;

                        }

                    }
                    spDialogUnit.setSelection(bb);

                }

                BolUnit = false;
            }
        });


        ItemImage = item.getImg();
        //  Bitmap bitmap = BitmapFactory.decodeByteArray(ItemImage, 0, ItemImage.length);
        //   Glide.with(ItemsActivity.this).load(bitmap).into(imgDialogCompany);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }

    public void delete(Items item) {
        alertDialog.show();
        SelectItem = item;
    }

    private void requestRuntimePermission() {
        ActivityCompat.requestPermissions(
                ItemsActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_EXTERNAL);
    }

    private byte[] imageViewToByteArray(CircleImageView circleImageView) {
        Bitmap bitmap = ((BitmapDrawable) circleImageView.getDrawable()).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    isExternalGranted = true;
                }
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {
            Uri imageUri = intent.getData();

            CropImage.activity(imageUri)
                    .setAspectRatio(3, 2)
                    .setMinCropWindowSize(100, 100)
                    .start(ItemsActivity.this);

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(intent);
            Uri croppedImageUri = result.getUri();

            File imageFile = new File(croppedImageUri.getPath());
            try {
                Bitmap bitmap = new Compressor(ItemsActivity.this)
                        .setQuality(10)
                        .compressToBitmap(imageFile);

                Glide.with(ItemsActivity.this).load(bitmap).into(imgDialogCompany);
            } catch (IOException ex) {

            }
        }
    }

    private void createDialog(Items items) {

        alertDialog = new AlertDialog.Builder(ItemsActivity.this);
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Do you sure you want Delete this?");
        alertDialog.setIcon(R.drawable.ic_delete);


        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                DeleteItem(AdaptetRecyclerItems.ItemApiID);
                viewModelItem.delete(SelectItem);
                adaptetRecyclerItems.setData(ItemsActivity.this.items);

                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

    }

    private void Clear() {
        spDialogCategory.setAdapter(null);
        ArrayAdapter adapterCategory = new ArrayAdapter(ItemsActivity.this, android.R.layout.simple_spinner_item, Category);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogCategory.setAdapter(adapterCategory);


        spDialogQuantity.setAdapter(null);
        ArrayAdapter adapterQuantity = new ArrayAdapter(ItemsActivity.this, android.R.layout.simple_spinner_item, Size);
        adapterQuantity.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogQuantity.setAdapter(adapterQuantity);


        spDialogRepository.setAdapter(null);
        ArrayAdapter adapterRepository = new ArrayAdapter(ItemsActivity.this, android.R.layout.simple_spinner_item, Repository);
        adapterQuantity.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogRepository.setAdapter(adapterRepository);


        spDialogUnit.setAdapter(null);
        ArrayAdapter adapterUnit = new ArrayAdapter(ItemsActivity.this, android.R.layout.simple_spinner_item, Unit);
        adapterUnit.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogUnit.setAdapter(adapterUnit);

        spDialogType.setAdapter(null);
        ArrayAdapter adapterType = new ArrayAdapter(ItemsActivity.this, android.R.layout.simple_spinner_item, Type);
        adapterType.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogType.setAdapter(adapterType);

        Glide.with(ItemsActivity.this).load(R.drawable.a).into(imgDialogCompany);

        edtBottomSheetItemName.setText("");
        edtBottomSheetItemCost.setText("");

        txtExpirStart.setText("");
        txtExpirEnd.setText("");


    }


    public void getItems(final int companyID) {
        Call<List<Items>> call = jsonPlaceHolderApi.getApiAllItems();

        call.enqueue(new Callback<List<Items>>() {
            @Override
            public void onResponse(Call<List<Items>> call, Response<List<Items>> response3) {
                if (!response3.isSuccessful()) {
                    return;
                }


                List<Items> items = response3.body();

                for (Items items1 : itemsApi)
                    itemsApi.remove(items1);


                for (Items item : items) {
                    if (companyID == item.getCompanyID()) {
                        itemsApi.add(item);
                    }
                }


                for (Items item : ItemsActivity.this.items) {
                    viewModelItem.delete(item);
                }
                for (Items item : itemsApi) {
                    viewModelItem.insert(item);
                }


                adaptetRecyclerItems.setData(ItemsActivity.this.items);

            }

            @Override
            public void onFailure(Call<List<Items>> call, Throwable t) {
                Toast.makeText(ItemsActivity.this, "Get error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createItem(int companyID, int categoryID, int cost, byte[] img,
                            File file, int repositoryID, String name,
                            int typeID, String expirStart, String expirEnd,
                            int quantityID, int count, int state) {

        Items item = new Items(quantityID, companyID, categoryID, cost, img, repositoryID, name, typeID, expirStart, expirEnd, count, state);


        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);


        RequestBody name1 = RequestBody.create(MediaType.parse("text/plain"), name);


        RequestBody expirStart1 = RequestBody.create(MediaType.parse("text/plain"), expirStart);


        RequestBody expirEnd1 = RequestBody.create(MediaType.parse("text/plain"), expirEnd);


        MultipartBody.Part filePart = MultipartBody.Part.createFormData("img", file.getName(), fileBody);


        viewModelItem.insert(item);


        Call<Items> call = jsonPlaceHolderApi.createItem(companyID, categoryID, cost, repositoryID, name1, typeID, expirStart1, expirEnd1, quantityID, count, state, filePart);


        call.enqueue(new Callback<Items>() {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response) {
                if (!response.isSuccessful()) {

                    return;
                }


            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {
                Toast.makeText(ItemsActivity.this, "Add Successful", Toast.LENGTH_SHORT).show();


            }
        });


    }


    public File bittofile(Bitmap bitmap) throws IOException {
        //create a file to write bitmap data
        File f = new File(ItemsActivity.this.getCacheDir(), Calendar.getInstance().getTime().toString());
        f.createNewFile();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }


    public void DeleteItem(int id) {
        Call<Void> call = jsonPlaceHolderApi.deleteItem(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(ItemsActivity.this, "Delete Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getRepository() {


        Call<List<Repositories>> callTypes = jsonPlaceHolderApi.getApiAllRepositories();

        callTypes.enqueue(new Callback<List<Repositories>>() {
            @Override
            public void onResponse(Call<List<Repositories>> call, Response<List<Repositories>> response3) {
                if (!response3.isSuccessful()) {
                    return;
                }
                List<Repositories> repositories = response3.body();


                for (Repositories repository : Rep) {
                    viewModelRepository.delete(repository);
                }
                for (Repositories repository : repositories) {
                    viewModelRepository.insert(repository);
                }


            }

            @Override
            public void onFailure(Call<List<Repositories>> call, Throwable t) {
            }
        });
    }


    public void getQuantity() {

        Call<List<Quantities>> callQuantities = jsonPlaceHolderApi.getApiAllQuantities();

        callQuantities.enqueue(new Callback<List<Quantities>>() {
            @Override
            public void onResponse(Call<List<Quantities>> call, Response<List<Quantities>> response2) {
                if (!response2.isSuccessful()) {
                    return;
                }
                List<Quantities> quantities = response2.body();


                for (Quantities quantities1 : Qun) {
                    viewModelQuantity.delete(quantities1);
                }

                for (Quantities QuantityApi : quantities) {
                    viewModelQuantity.insert(QuantityApi);
                }


            }

            @Override
            public void onFailure(Call<List<Quantities>> call, Throwable t) {
            }
        });
    }


    public void getType() {
        Call<List<Types>> callTypes = jsonPlaceHolderApi.getApiTypes();

        callTypes.enqueue(new Callback<List<Types>>() {
            @Override
            public void onResponse(Call<List<Types>> call, Response<List<Types>> response3) {
                if (!response3.isSuccessful()) {
                    return;
                }
                List<Types> types = response3.body();


                for (Types Type : Typ) {
                    viewModelType.delete(Type);
                }
                for (Types TypeApi : types) {
                    viewModelType.insert(TypeApi);
                }
            }

            @Override
            public void onFailure(Call<List<Types>> call, Throwable t) {
            }
        });
    }


    public void getCategory() {
        Call<List<Categories>> callCategory = jsonPlaceHolderApi.getApiAllCategories();

        callCategory.enqueue(new Callback<List<Categories>>() {
            @Override
            public void onResponse(Call<List<Categories>> call, Response<List<Categories>> response1) {
                if (!response1.isSuccessful()) {
                    return;
                }
                List<Categories> categories = response1.body();


                for (Categories categor : Cat) {
                    viewModelCategory.delete(categor);
                }
                for (Categories category : categories) {
                    viewModelCategory.insert(category);
                }

            }

            @Override
            public void onFailure(Call<List<Categories>> call, Throwable t) {
            }
        });
    }


    private void updateItem(int id, int categoryID, int cost,
                            File file, int repositoryID, String name,
                            int typeID, String expirStart, String expirEnd,
                            int quantityID, int count) {

        RequestBody fileBody = RequestBody.create(MediaType.parse("image/*"), file);

        RequestBody name1 = RequestBody.create(MediaType.parse("text/plain"), SelectItem.getItemName());

        RequestBody expirStart1 = RequestBody.create(MediaType.parse("text/plain"), SelectItem.getExpirStart());

        RequestBody expirEnd1 = RequestBody.create(MediaType.parse("text/plain"), SelectItem.getExpirEnd());

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("img", file.getName(), fileBody);

        Call<Items> call = jsonPlaceHolderApi.updateItem(SelectItem.getItemApiID(), SelectItem.getCategoryID(), cost, SelectItem.getRepositoryID(),
                name1, SelectItem.getTypeID(), expirStart1, expirEnd1
                , SelectItem.getQuantityID(), SelectItem.getCount(), filePart
        );

        call.enqueue(new Callback<Items>() {
            @Override
            public void onResponse(Call<Items> call, Response<Items> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Items> call, Throwable t) {
            }
        });


    }


    public void sendNotification(String nameItem, String nameCompany) {


        Retrofit retrofit = new Retrofit.Builder().
                baseUrl("https://fcm.googleapis.com/").
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);
        String a = "تمت اضافة عنصر جديد لشركة" + " " + nameCompany;
        notification not = new notification(a, nameItem);

        body b = new body("/topics/all", "high", not);


        Call<ResponseBody> call = jsonPlaceHolderApi.createNotification(b);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


    }


}






