package com.example.rteba.View.Fragment;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.rteba.Adapter.AdaptetRecyclerBillItem;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.BillsSale;
import com.example.rteba.Model.Entity.BillsSale_Items;
import com.example.rteba.Model.Entity.Custmers;
import com.example.rteba.View.Dialog.DialogEnterCount;
import com.example.rteba.ViewModel.ViewModelCustmer;
import com.example.tebavet.R;
import com.example.rteba.ViewHolder.ViewHolderBill;
import com.example.rteba.ViewModel.ViewModelBillSale;
import com.example.rteba.ViewModel.ViewModelBillSaleItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class BillsFragment extends Fragment {

    private JsonPlaceHolderApi jsonPlaceHolderApi;


    private FloatingActionButton FabnewBillSale;

    public static int billSaleID;


    public static Custmers Cus;

    private BottomSheetBehavior bottomSheetBehavior;
    private RelativeLayout bottomSheetBillItem;

    ViewModelBillSale viewModelBillSale;
    ViewModelBillSaleItem viewModelBillSaleItem;
    ViewModelCustmer viewModelCustmer;


    private RecyclerView recyclerbill;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerBills adaptetRecyclerBills;

    private RecyclerView recyclerBillItem;
    private RecyclerView.LayoutManager layoutManagerBillItems;
    private AdaptetRecyclerBillItem adaptetRecyclerBillItem;

    private List<BillsSale> billsSale;
    private List<BillsSale> AllbillsSale;

    public static List<BillsSale_Items> billsSale_items = new ArrayList<>();


    public static AlertDialog.Builder alertDialog;

    public static BillsSale selectedBillsSale;

    @SuppressLint("RestrictedApi")
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_bills, container, false);
        recyclerbill = (RecyclerView) view.findViewById(R.id.recyclerbill);


        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        billsSale = new ArrayList<>();


        if (Cus.getState() == 1)
            AllbillsSale = new ArrayList<>();


        viewModelBillSale = ViewModelProviders.of(getActivity()).get(ViewModelBillSale.class);
        viewModelBillSaleItem = ViewModelProviders.of(getActivity()).get(ViewModelBillSaleItem.class);
        viewModelCustmer = ViewModelProviders.of(getActivity()).get(ViewModelCustmer.class);

        FabnewBillSale = (FloatingActionButton) view.findViewById(R.id.newBillSale);


        if (Cus.getState() == 1)
            FabnewBillSale.setVisibility(View.GONE);


        recyclerbill.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerbill.setLayoutManager(layoutManager);
        adaptetRecyclerBills = new AdaptetRecyclerBills(getActivity(), billsSale);
        if (Cus.getState() == 1)
            adaptetRecyclerBills = new AdaptetRecyclerBills(getActivity(), AllbillsSale);

        recyclerbill.setAdapter(adaptetRecyclerBills);


        if (Cus.getState() == 1) {
            viewModelBillSale.getAllBillsSale().observe(BillsFragment.this, new Observer<List<BillsSale>>() {
                @Override
                public void onChanged(@Nullable List<BillsSale> billsSales) {
                    AllbillsSale = billsSales;
                    adaptetRecyclerBills.setData(BillsFragment.this.AllbillsSale);

                }
            });
        }


        if (Cus.getState() == 0) {
            viewModelBillSale.getBillSale(Cus.getIdCustmer()).observe(BillsFragment.this, new Observer<List<BillsSale>>() {
                @Override
                public void onChanged(@Nullable List<BillsSale> billsSales) {
                    BillsFragment.this.billsSale = billsSales;
                    adaptetRecyclerBills.setData(BillsFragment.this.billsSale);
                }
            });
        }

        if (Cus.getState() == 1)

            getAllBillsSale();

        if (Cus.getState() == 0)
            getBillsSale(Cus.getIdCustmer());


        recyclerBillItem = (RecyclerView) view.findViewById(R.id.recyclerBillItem);


        bottomSheetBillItem = (RelativeLayout) view.findViewById(R.id.bottomSheetBillItem);


        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetBillItem);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {


                } else if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED ||
                        bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                    replaceFragment(new BillsFragment());
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });


        recyclerBillItem.setHasFixedSize(true);
        layoutManagerBillItems = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerBillItem.setLayoutManager(layoutManagerBillItems);
        adaptetRecyclerBillItem = new AdaptetRecyclerBillItem(getActivity(), BillsFragment.this.billsSale_items, BillsFragment.this);
        recyclerBillItem.setAdapter(adaptetRecyclerBillItem);


        FabnewBillSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String time = Calendar.getInstance().get(Calendar.HOUR) + ":" + Calendar.getInstance().get(Calendar.MINUTE) + ":" + Calendar.getInstance().get(Calendar.SECOND);
                createBillSale(Cus.getIdCustmer(), Cus.getNameCustmer(), time);
                FabnewBillSale.setEnabled(false);
            }
        });


        createDialog();
        return view;
    }

    public static void delete(BillsSale billsSale) {
        if (Cus.getState() == 1) {
            selectedBillsSale = billsSale;
            alertDialog.show();
        } else
            return;

    }

    private void createDialog() {

        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Do you sure you want Delete this?");
        alertDialog.setIcon(R.drawable.ic_delete);

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                DeleteItem(selectedBillsSale.getBillsApiID());
                viewModelBillSale.delete(selectedBillsSale);
                adaptetRecyclerBills.setData(billsSale);

                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                dialog.dismiss();
            }
        });
    }

    public void Show(BillsSale BillsSale) {

        viewModelBillSaleItem.getBillsSaleItems(BillsSale.getBillsApiID()).observe(getActivity(), new Observer<List<BillsSale_Items>>() {
            @Override
            public void onChanged(@Nullable List<BillsSale_Items> billsSale_items) {
                BillsFragment.this.billsSale_items = billsSale_items;
                adaptetRecyclerBillItem.setData(BillsFragment.this.billsSale_items);

            }
        });


        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        getbillItems(BillsSale.getBillsApiID());


    }


    public class AdaptetRecyclerBills extends RecyclerView.Adapter<ViewHolderBill> {
        private Context context;
        private List<BillsSale> billsSales;

        public abstract class OnItemClickListener {
            public abstract void onItemClick(int position);
        }

        public AdaptetRecyclerBills(Context context, List<BillsSale> billsSales) {
            this.context = context;
            this.billsSales = billsSales;
        }


        @NonNull
        @Override
        public ViewHolderBill onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_bill, viewGroup, false);
            return new ViewHolderBill(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolderBill viewHolderBill, final int position) {

            final BillsSale billsSale = billsSales.get(position);


            viewHolderBill.getTxtBillCustmer().setText("Custmer: " + billsSale.getNameCustmer());
            viewHolderBill.getTxtBillDate().setText("Date: " + billsSale.getDate());
            viewHolderBill.getTxtBillTime().setText("Time: " + billsSale.getTime());


            viewHolderBill.getView().setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    billSaleID = billsSale.getBillsApiID();

                    replaceFragment(new SearchFragment());

                    return true;

                }
            });

            viewHolderBill.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Show(billsSale);

                }
            });


            viewHolderBill.getImgBillDelete().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BillsFragment.delete(billsSale);
                }
            });
        }

        @Override
        public int getItemCount() {
            return billsSales.size();
        }

        public void setData(List<BillsSale> billsSales) {
            this.billsSales = billsSales;
            notifyDataSetChanged();
        }
    }


    public void ShowDialog(BillsSale_Items billsSale_items) {
        DialogEnterCount dialogEnterCount = new DialogEnterCount();
        dialogEnterCount.show(getFragmentManager(), "dialogCustmers");
        dialogEnterCount.setItem(billsSale_items);
    }


    public void getBillsSale(int custmerID) {
        Call<List<BillsSale>> call = jsonPlaceHolderApi.getApiBillsSale(custmerID);

        call.enqueue(new Callback<List<BillsSale>>() {
            @Override
            public void onResponse(Call<List<BillsSale>> call, Response<List<BillsSale>> response1) {
                if (!response1.isSuccessful()) {
                    return;
                }
                final List<BillsSale> billsSales = response1.body();

                if (BillsFragment.this.billsSale.size() != billsSales.size()) {
                    for (BillsSale billSale : BillsFragment.this.billsSale)
                        viewModelBillSale.delete(billSale);

                    for (BillsSale billSale : billsSales)
                        viewModelBillSale.insert(billSale);

                }


                billsSale = billsSales;
                recyclerbill.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerbill.setLayoutManager(layoutManager);
                adaptetRecyclerBills = new AdaptetRecyclerBills(getActivity(), billsSale);
                recyclerbill.setAdapter(adaptetRecyclerBills);


            }

            @Override
            public void onFailure(Call<List<BillsSale>> call, Throwable t) {
            }
        });
    }


    public void getAllBillsSale() {
        Call<List<BillsSale>> call = jsonPlaceHolderApi.getApiAllBillSale();

        call.enqueue(new Callback<List<BillsSale>>() {
            @Override
            public void onResponse(Call<List<BillsSale>> call, Response<List<BillsSale>> response1) {
                if (!response1.isSuccessful()) {
                    return;
                }
                final List<BillsSale> billsSales = response1.body();

                if (BillsFragment.this.AllbillsSale.size() != billsSales.size()) {
                    for (BillsSale billSale : BillsFragment.this.AllbillsSale)
                        viewModelBillSale.delete(billSale);

                    for (BillsSale billSale : billsSales)
                        viewModelBillSale.insert(billSale);

                }


                AllbillsSale = billsSales;
                recyclerbill.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                recyclerbill.setLayoutManager(layoutManager);
                adaptetRecyclerBills = new AdaptetRecyclerBills(getActivity(), AllbillsSale);
                recyclerbill.setAdapter(adaptetRecyclerBills);


            }

            @Override
            public void onFailure(Call<List<BillsSale>> call, Throwable t) {
            }
        });
    }


    public void DeleteItem(int id) {
        Call<Void> call = jsonPlaceHolderApi.deleteBillSale(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getContext(), "Delete Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getbillItems(int billID) {
        Call<List<BillsSale_Items>> call = jsonPlaceHolderApi.getApiBillItemsSale(billID);

        call.enqueue(new Callback<List<BillsSale_Items>>() {
            @Override
            public void onResponse(Call<List<BillsSale_Items>> call, Response<List<BillsSale_Items>> response3) {
                if (!response3.isSuccessful()) {
                    return;
                }
                List<BillsSale_Items> billsSale_items = response3.body();

                for (BillsSale_Items billsSaleitem : BillsFragment.this.billsSale_items)
                    viewModelBillSaleItem.delete(billsSaleitem);

                for (BillsSale_Items billsSaleitem : billsSale_items)
                    viewModelBillSaleItem.insert(billsSaleitem);


                adaptetRecyclerBillItem.setData(BillsFragment.this.billsSale_items);


            }

            @Override
            public void onFailure(Call<List<BillsSale_Items>> call, Throwable t) {
            }
        });
    }

    private void replaceFragment(Fragment fragment) {

        String fragmentTag = fragment.getClass().getName();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.fragment_container, fragment, fragmentTag);
        ft.addToBackStack("fragments");
        ft.commit();

    }


    private void createBillSale(int custmerID, String name, String time) {
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String sDate = simpleDateFormat.format(date);
        BillsSale billSale = new BillsSale(1, sDate, custmerID, time, name);


        Call<BillsSale> call = jsonPlaceHolderApi.createBillSale(billSale);


        viewModelBillSale.insert(billSale);


        call.enqueue(new Callback<BillsSale>() {
            @Override
            public void onResponse(Call<BillsSale> call, Response<BillsSale> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<BillsSale> call, Throwable t) {

            }
        });

    }


    public void DeleteBillItemDialog(final BillsSale_Items billsSale_items) {
        Call<Void> call = jsonPlaceHolderApi.deleteBillSaleItem(billsSale_items.getID(), billsSale_items.getBillSaleItemID());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }


                viewModelBillSaleItem.delete(billsSale_items);

                adaptetRecyclerBillItem.setData(BillsFragment.this.billsSale_items);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }


}
