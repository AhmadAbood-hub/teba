package com.example.rteba.View.Dialog;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Employees;
import com.example.rteba.Model.Entity.Repositories;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.RepActivity;
import com.example.rteba.ViewModel.ViewModelEmployee;
import com.example.rteba.ViewModel.ViewModelRepository;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogRepository extends DialogFragment
{
    JsonPlaceHolderApi jsonPlaceHolderApi;
    private ViewModelRepository viewModelRepository;
    private ViewModelEmployee viewModelEmployee;
    public static boolean  AddSave = false;
    private String repositoryLocation;
    private int  repositoryAnnualRent;
    private List<String> managerName ;
    private   int Managerid;
    private  String SpManager;

    public void setRepositoryLocation(String repositoryLocation)
    {
        this.repositoryLocation = repositoryLocation;
    }
    public void setRepositoryAnnualRent(int repositoryAnnualRent)
    {
        this.repositoryAnnualRent = repositoryAnnualRent;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        managerName = new ArrayList<>();
        View view  = inflater.inflate(R.layout.dialog_repository, container, false);
        final TextInputEditText edtDialogRepositoryLocation = (TextInputEditText) view.findViewById(R.id.edtDialogRepositoryLocation);
        final TextInputEditText edtDialogRepositoryAnnualRent = (TextInputEditText) view.findViewById(R.id.edtDialogRepositoryAnnualRent);
        final Spinner spDialogRepositoryManager = (Spinner) view.findViewById(R.id.spDialogRepositoryManager);

        final Button btnDialogRepositoryAdd = (Button) view.findViewById(R.id.btnDialogRepositoryAdd);
        final Button btnDialogRepositorySave = (Button) view.findViewById(R.id.btnDialogRepositorySave);
        viewModelRepository = ViewModelProviders.of(DialogRepository.this).get(ViewModelRepository.class);
        viewModelEmployee = ViewModelProviders.of(DialogRepository.this).get(ViewModelEmployee.class);

        managerName =new ArrayList<>();

        viewModelEmployee.getManager().observe(getActivity(), new Observer<List<Employees>>()
        {
            @Override
            public void onChanged(@Nullable List<Employees> employees)
            {

                for(Employees a : employees)
                {
                    managerName.add(a.getName()+"");
                }
            }
        });



        managerName.add("");




        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, managerName);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spDialogRepositoryManager.setAdapter(adapter);



if(AddSave)
{


    btnDialogRepositoryAdd.setVisibility(View.GONE);
    btnDialogRepositorySave.setVisibility(View.VISIBLE);
    edtDialogRepositoryLocation.setText(repositoryLocation);
    edtDialogRepositoryAnnualRent.setText( String.valueOf(repositoryAnnualRent));


}

        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);



        btnDialogRepositoryAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                SpManager = spDialogRepositoryManager.getSelectedItem().toString();
                if(SpManager.equals(""))
                {
                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                    return;
                }
                viewModelEmployee.getID(SpManager+"").observe(getActivity(), new Observer<Integer>()
                {
                    @Override
                    public void onChanged(@Nullable final Integer integer)
                    {
                        Managerid=  integer;
                        viewModelEmployee.getNameID(Managerid).observe(getActivity(), new Observer<String>()
                        {
                            @Override
                            public void onChanged(@Nullable String s)
                            {
                                String location = edtDialogRepositoryLocation.getText().toString();
                                String annualrent = edtDialogRepositoryAnnualRent.getText().toString();

                                if(location.equals("")||annualrent.equals("")||SpManager.equals(""))
                                {
                                    Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                                    return;
                                }




                                createRepository(location,Integer.parseInt(annualrent) ,Managerid,s);


                                edtDialogRepositoryLocation.setText("");
                                edtDialogRepositoryAnnualRent.setText("");
                                dismiss();
                            }
                        });

                    }
                });

            }
        });
        btnDialogRepositorySave.setOnClickListener(new View.OnClickListener()
{
    @Override
    public void onClick(View view)
    {

try {

    repositoryLocation = edtDialogRepositoryLocation.getText().toString();
    repositoryAnnualRent =Integer.parseInt( edtDialogRepositoryAnnualRent.getText().toString());
    SpManager = spDialogRepositoryManager.getSelectedItem().toString();
    if(repositoryLocation.equals("")||repositoryAnnualRent==0||SpManager.equals(""))
    {
        Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
        return;
    }
    RepActivity.selectedRepository.setLocation(repositoryLocation);
    RepActivity.selectedRepository.setAnnualrent(repositoryAnnualRent);
    RepActivity.selectedRepository.setManagerName(SpManager);
    viewModelEmployee.getID(SpManager+"").observe(getActivity(), new Observer<Integer>() {
                @Override
                public void onChanged(@Nullable Integer integer)
                {
                    Managerid = integer;
                    RepActivity.selectedRepository.setManegerID(Managerid);
                    viewModelRepository.update(RepActivity.selectedRepository);
                }
            });


    edtDialogRepositoryLocation.setText("");
    edtDialogRepositoryAnnualRent.setText("");

    updateRepository(RepActivity.selectedRepository);
    dismiss();
}

     catch (Exception e)
     {

     }
    }
});
        return view;
    }

    private void createRepository( String location, int annualrent,int managerID,String ManagerName)
    {
        Repositories repository =new Repositories(location,annualrent,managerID,ManagerName);


        Call<Repositories> call = jsonPlaceHolderApi.createRepository(repository);


        viewModelRepository.insert(repository);


        call.enqueue(new Callback<Repositories>()
        {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t)
            {
                return;
            }
        });

    }



    private void updateRepository(Repositories repository)
    {


        Call<Repositories> call = jsonPlaceHolderApi.updateRepository(repository.getId(), repository);


        call.enqueue(new Callback<Repositories>()
        {
            @Override
            public void onResponse(Call<Repositories> call, Response<Repositories> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Repositories> call, Throwable t)
            {

            }
        });


    }




}
