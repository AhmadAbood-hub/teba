package com.example.rteba.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.rteba.Adapter.MyAdapter;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.BillsSale_Items;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.ViewModel.ViewModelBillSaleItem;
import com.example.rteba.ViewModel.ViewModelItem;
import com.example.tebavet.R;
import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.rteba.Adapter.MyAdapter.Favitems;
import static com.example.rteba.View.Activity.CompaniesActivity.selectedCompany;

import static com.example.rteba.View.Activity.QuaTypCatActivity.selectedCategory;
import static com.example.rteba.View.Activity.QuaTypCatActivity.selectedType;
import static com.example.rteba.View.Fragment.BillsFragment.billSaleID;

public class ItemsForCustomer extends AppCompatActivity
{

       private JsonPlaceHolderApi jsonPlaceHolderApi;

       private ViewModelItem viewModelItem;

       private ViewModelBillSaleItem viewModelBillSaleItem;



       private ProgressBar ProgressPar;
       HorizontalInfiniteCycleViewPager viewPager;
       List<Items> ItemList = new ArrayList<>();

       MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_for_customer);



          Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
          jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


              myAdapter = new MyAdapter(ItemsForCustomer.this,ItemList);

              ProgressPar = (ProgressBar) findViewById(R.id.ProgressPar);

             final FloatingActionButton btn_done = (FloatingActionButton) findViewById(R.id.btn_done);

             viewPager =(HorizontalInfiniteCycleViewPager) findViewById(R.id.view_pager);

             viewModelItem = ViewModelProviders.of(this).get(ViewModelItem.class);

             viewModelBillSaleItem = ViewModelProviders.of(this).get(ViewModelBillSaleItem.class);

             viewModelItem.getItem().observe(this, new Observer<List<Items>>()
            {
                @Override
               public void onChanged(@Nullable List<Items> items)
               {
                   try
                 {
                    ItemList = items;
                   ProgressPar.setVisibility(View.GONE);
                    MyAdapter myAdapter = new MyAdapter(ItemsForCustomer.this,ItemList);
                   viewPager.setAdapter(myAdapter);
                   btn_done.setOnClickListener(new View.OnClickListener()
                   {
                       @Override
                       public void onClick(View view)
                       {
                           addBillItems();
                           Toast.makeText(ItemsForCustomer.this, "Your order is done...", Toast.LENGTH_SHORT).show();
                           btn_done.setEnabled(false);
                       }
                   });
              }

catch (Exception e)
{

}
                  }
             });



        getItems();



    }


    public void getItems()
    {
        Call<List<Items>> call = jsonPlaceHolderApi.getApiAllItem(selectedCategory.getId(),selectedType.getId(),selectedCompany.getCompanyApiID());

        call.enqueue(new Callback<List<Items>>()
        {
            @Override
            public void onResponse(Call<List<Items>> call, Response<List<Items>> response3) {
                if (!response3.isSuccessful())
                {
                    return;
                }


                List<Items> items = response3.body();



                for (Items item : ItemsForCustomer.this.ItemList)
                {
                    viewModelItem.delete(item);
                }
                for (Items item : items)
                {
                    viewModelItem.insert(item);
                }




            }
            @Override
            public void onFailure(Call<List<Items>> call, Throwable t)
            {
            }
        });
    }




    public void addBillItems()
    {
        for(int i=0;i<Favitems.size();i++)
        {
            createBillItem(Favitems.get(i));
        }


        Favitems.clear();
    }




    private void createBillItem(Items item)
    {
        item.setOrder(0);


        BillsSale_Items billsSale_items1 = new BillsSale_Items(billSaleID, item.getItemApiID(),item.getOrder());


        Call<BillsSale_Items> call = jsonPlaceHolderApi.createBillItemSale(billsSale_items1);

        viewModelBillSaleItem.insert(billsSale_items1);



        call.enqueue(new Callback<BillsSale_Items>()
        {
            @Override
            public void onResponse(Call<BillsSale_Items> call, Response<BillsSale_Items> response)
            {
                if (!response.isSuccessful())
                {
                    return;
                }

            }

            @Override
            public void onFailure(Call<BillsSale_Items> call, Throwable t) {

            }
        });


    }






}
