package com.example.rteba.View.Dialog;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Jobs;
import com.example.tebavet.R;
import com.example.rteba.View.Activity.JobsActivity;
import com.example.rteba.ViewModel.ViewModelJob;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DialogJobs extends DialogFragment {
    JsonPlaceHolderApi jsonPlaceHolderApi;

    private ViewModelJob ViewModelJob;
    public static boolean AddSave = false;
    private String JobName;
    private int JobSalay;
    private int JobHour;
    private int JobCostPerHour;

    public void setJobName(String jobName) {
        JobName = jobName;
    }

    public void setJobSalay(int jobSalay) {
        JobSalay = jobSalay;
    }

    public void setJobHour(int jobHour) {
        JobHour = jobHour;
    }

    public void setJobCostPerHour(int jobCostPerHour) {
        JobCostPerHour = jobCostPerHour;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_jobs, container, false);
        final TextInputEditText edtDialogJobName = (TextInputEditText) view.findViewById(R.id.edtDialogJobName);
        final TextInputEditText edtDialogJobSalary = (TextInputEditText) view.findViewById(R.id.edtDialogJobSalary);
        final TextInputEditText edtDialogJobHour = (TextInputEditText) view.findViewById(R.id.edtDialogJobHour);
        final TextInputEditText edtDialogJobCostPerHour = (TextInputEditText) view.findViewById(R.id.edtDialogJobCostPerHour);
        final Button btnDialogJobAdd = (Button) view.findViewById(R.id.btnDialogJobAdd);
        final Button btnDialogJobSave = (Button) view.findViewById(R.id.btnDialogJobSave);

        ViewModelJob = ViewModelProviders.of(DialogJobs.this).get(ViewModelJob.class);
        if (AddSave) {
            btnDialogJobAdd.setVisibility(View.GONE);
            btnDialogJobSave.setVisibility(View.VISIBLE);
            edtDialogJobName.setText(JobName);
            edtDialogJobSalary.setText(JobSalay + "");
            edtDialogJobHour.setText(JobHour + "");
            edtDialogJobCostPerHour.setText(JobCostPerHour + "");

        }

        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        btnDialogJobAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    String name = edtDialogJobName.getText().toString();
                    int salary = Integer.parseInt(edtDialogJobSalary.getText().toString());
                    int Hour = Integer.parseInt(edtDialogJobHour.getText().toString());
                    int CostPerHour = Integer.parseInt(edtDialogJobCostPerHour.getText().toString());
                    if (name.equals("") || salary < 0 || Hour < 0 || CostPerHour < 0) {
                        return;
                    }


                    createJob(name, salary, Hour, CostPerHour);
                } catch (Exception e) {

                }
                edtDialogJobName.setText("");
                edtDialogJobSalary.setText("");
                edtDialogJobHour.setText("");
                edtDialogJobCostPerHour.setText("");
                dismiss();
            }
        });
        btnDialogJobSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {
                    JobName = edtDialogJobName.getText().toString();
                    JobSalay = Integer.parseInt(edtDialogJobSalary.getText().toString());
                    JobHour = Integer.parseInt(edtDialogJobHour.getText().toString());
                    JobCostPerHour = Integer.parseInt(edtDialogJobCostPerHour.getText().toString());
                    if (JobName.equals("") || JobSalay < 0 || JobHour < 0 || JobCostPerHour < 0) {
                        Toast.makeText(getActivity(), "Please Enter Data", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    JobsActivity.selectedJob.setName(JobName);
                    JobsActivity.selectedJob.setSalary(JobSalay);
                    JobsActivity.selectedJob.setHour(JobHour);
                    JobsActivity.selectedJob.setCostPerHour(JobCostPerHour);
                    ViewModelJob.update(JobsActivity.selectedJob);
                    edtDialogJobName.setText("");
                    edtDialogJobSalary.setText("");
                    edtDialogJobHour.setText("");
                    edtDialogJobCostPerHour.setText("");
                    updateJob(JobsActivity.selectedJob);
                    dismiss();
                } catch (Exception e) {

                }


            }
        });
        return view;
    }


    private void createJob(String name, int salary, int hour, int CostPerHour) {
        Jobs job = new Jobs(name, salary, hour, CostPerHour);


        Call<Jobs> call = jsonPlaceHolderApi.createJob(job);


        ViewModelJob.insert(job);


        call.enqueue(new Callback<Jobs>() {
            @Override
            public void onResponse(Call<Jobs> call, Response<Jobs> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Jobs> call, Throwable t) {
                Toast.makeText(getContext(), "Send error", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void updateJob(Jobs job) {


        Call<Jobs> call = jsonPlaceHolderApi.updateJob(job.getId(), job);


        call.enqueue(new Callback<Jobs>() {
            @Override
            public void onResponse(Call<Jobs> call, Response<Jobs> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Jobs> call, Throwable t) {

            }
        });


    }


}
