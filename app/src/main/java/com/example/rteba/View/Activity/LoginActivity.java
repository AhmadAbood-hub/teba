package com.example.rteba.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Custmers;
import com.example.rteba.View.Fragment.BillsFragment;
import com.example.rteba.ViewModel.ViewModelCustmer;
import com.example.tebavet.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class LoginActivity extends AppCompatActivity
{
    private JsonPlaceHolderApi jsonPlaceHolderApi;
    private Button cirLoginButton;
    private EditText  Name;
    private EditText  Number;
    private List<Custmers> customers;

    private List<String> names = new ArrayList<>();
    private List<String> numbers= new ArrayList<>();

    private ViewModelCustmer viewModelCustmer;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Retrofit retrofit =new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);



        viewModelCustmer = ViewModelProviders.of(LoginActivity.this).get(ViewModelCustmer.class);

        viewModelCustmer.getCustmer().observe(LoginActivity.this, new Observer<List<Custmers>>()
        {
            @Override
            public void onChanged(@Nullable List<Custmers> customers)
            {
                LoginActivity.this.customers = customers;
            }
        });




        cirLoginButton = findViewById(R.id.cirLoginButton);
        Name = findViewById(R.id.editTextName);
        Number = findViewById(R.id.editTextPasswordLogin);


        getCustmer();

        cirLoginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {


                for(Custmers customer : customers)
                {
                    names.add(customer.getNameCustmer());
                    numbers.add(customer.getNumberCustmer());

                    if(customer.getNameCustmer().equals(Name.getText().toString()) && customer.getNumberCustmer().equals(Number.getText().toString()))

                    {
                       BillsFragment.Cus = customer;
                       startActivity(new Intent(LoginActivity.this, NavigationDrawer.class));
                       return;
                    }

                }
                 if(Name.getText().toString().equals("")&&Number.getText().toString().equals(""))
                     Toast.makeText(LoginActivity.this, "Please enter your data", Toast.LENGTH_SHORT).show();
               else if(Name.getText().toString().equals(""))
                      Toast.makeText(LoginActivity.this, "Please enter your name", Toast.LENGTH_SHORT).show();
               else if(Number.getText().toString().equals(""))
                      Toast.makeText(LoginActivity.this, "Please enter your number", Toast.LENGTH_SHORT).show();


else if(!names.contains(Name.getText().toString()) ||!numbers.contains(Number.getText().toString()) )
    Toast.makeText(LoginActivity.this, "Your data is wrong", Toast.LENGTH_SHORT).show();
          }
        });


    }

    public void onLoginClick(View View)
    {
        startActivity(new Intent(this, RegisterActivitiy.class));
        overridePendingTransition(R.anim.slide_in_right, R.anim.stay);

    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
        moveTaskToBack(true);
    }





    public void getCustmer()
    {
        Call<List<Custmers>> call = jsonPlaceHolderApi.getApiAllCustmers();

        call.enqueue(new Callback<List<Custmers>>()
        {
            @Override
            public void onResponse(Call<List<Custmers>> call, Response<List<Custmers>> response3) {
                if (!response3.isSuccessful()) {
                    return;
                }
                List<Custmers> custmers = response3.body();

                LoginActivity.this.customers = custmers;

            }
            @Override
            public void onFailure(Call<List<Custmers>> call, Throwable t)
            {

            }
        });
    }





}
