package com.example.rteba.View.Activity;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.rteba.Adapter.AdaptetRecyclerCustmrs;
import com.example.rteba.Model.JsonPlaceHolderApi;
import com.example.rteba.Model.Entity.Custmers;
import com.example.tebavet.R;
import com.example.rteba.View.Dialog.DialogCustmers;
import com.example.rteba.ViewModel.ViewModelCustmer;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CustmersActivity extends AppCompatActivity {
    public JsonPlaceHolderApi jsonPlaceHolderApi;


    public int CustmerID;

    public static Custmers selectedCustmer;


    private ViewModelCustmer viewModelCustmer;


    private FloatingActionButton FabAddCustmer;

    private RecyclerView recyclerCustmers;
    private RecyclerView.LayoutManager layoutManager;
    private AdaptetRecyclerCustmrs adaptetRecyclerCustmrs;
    private List<Custmers> custmers;


    private AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custmer);


        viewModelCustmer = ViewModelProviders.of(CustmersActivity.this).get(ViewModelCustmer.class);
        viewModelCustmer.getCustmer().observe(CustmersActivity.this, new Observer<List<Custmers>>() {
            @Override
            public void onChanged(@Nullable List<Custmers> custmers) {

                CustmersActivity.this.custmers = custmers;
                adaptetRecyclerCustmrs.setData(custmers);
            }
        });


        Retrofit retrofit = new Retrofit.Builder().
                baseUrl(getString(R.string.domain_server)).
                addConverterFactory(GsonConverterFactory.create()).
                build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);


        getCustmer();


        FabAddCustmer = (FloatingActionButton) findViewById(R.id.FabAddCustmer);
        FabAddCustmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogCustmers dialogCustmers = new DialogCustmers();
                dialogCustmers.show(getSupportFragmentManager(), "dialogCustmers");
                DialogCustmers.AddSave = false;
            }
        });

        recyclerCustmers = (RecyclerView) findViewById(R.id.recyclerCustmers);
        recyclerCustmers.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(CustmersActivity.this, LinearLayoutManager.VERTICAL, false);
        // layoutManager = new GridLayoutManager(CustmersActivity.this, 2);
        recyclerCustmers.setLayoutManager(layoutManager);
        custmers = new ArrayList<>();
        adaptetRecyclerCustmrs = new AdaptetRecyclerCustmrs(CustmersActivity.this, custmers);
        recyclerCustmers.setAdapter(adaptetRecyclerCustmrs);

        createDialog();
    }


    public void delete(Custmers custmers) {
        selectedCustmer = custmers;
        alertDialog.show();

    }




    public void showData(Custmers custmers) {
        selectedCustmer = custmers;
        DialogCustmers dialogCustmers = new DialogCustmers();
        dialogCustmers.setCustmerName(selectedCustmer.getNameCustmer());
        dialogCustmers.setCustmerNumber(selectedCustmer.getNumberCustmer());
        dialogCustmers.show(getSupportFragmentManager(), "dialogCustmers");
        DialogCustmers.AddSave = true;

    }

    private void createDialog() {

        alertDialog = new AlertDialog.Builder(CustmersActivity.this);
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Do you sure you want Delete this?");
        alertDialog.setIcon(R.drawable.ic_delete);





        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                OneCustmer();
                viewModelCustmer.delete(selectedCustmer);
                adaptetRecyclerCustmrs.setData(custmers);

                dialog.dismiss();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                dialog.dismiss();
            }
        });
    }


    public void getCustmer() {
        Call<List<Custmers>> call = jsonPlaceHolderApi.getApiAllCustmers();

        call.enqueue(new Callback<List<Custmers>>() {
            @Override
            public void onResponse(Call<List<Custmers>> call, Response<List<Custmers>> response3) {
                if (!response3.isSuccessful()) {
                    return;
                }
                List<Custmers> custmers = response3.body();


                for (Custmers custmer : CustmersActivity.this.custmers) {
                    viewModelCustmer.delete(custmer);
                }
                for (Custmers custmer : custmers) {
                    viewModelCustmer.insert(custmer);
                }

                adaptetRecyclerCustmrs.setData(CustmersActivity.this.custmers);
            }

            @Override
            public void onFailure(Call<List<Custmers>> call, Throwable t) {
            }
        });
    }

    public void DeleteCustmer(int id) {
        Call<Void> call = jsonPlaceHolderApi.deleteCustmer(id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (!response.isSuccessful()) {
                    return;
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(CustmersActivity.this, "Delete Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void OneCustmer() {

        Call<List<Custmers>> call = jsonPlaceHolderApi.getOneCustmer();

        call.enqueue(new Callback<List<Custmers>>() {
            @Override
            public void onResponse(Call<List<Custmers>> call, Response<List<Custmers>> response3) {
                if (!response3.isSuccessful()) {
                    Toast.makeText(CustmersActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Custmers> custmers = response3.body();

                for (int i = 0; i < custmers.size(); i++) {
                    if (custmers.get(i).getNameCustmer().equals(selectedCustmer.getNameCustmer())) {
                        CustmerID = custmers.get(i).getIdCustmer();
                        DeleteCustmer(CustmerID);

                        return;
                    }

                }
            }

            @Override
            public void onFailure(Call<List<Custmers>> call, Throwable t) {
                Toast.makeText(CustmersActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
