package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.BillsBuy_Items;

import java.util.List;

@Dao
public interface BillBuyItemDao
{
    @Insert
    void insert(BillsBuy_Items billsBuy_items);

    @Delete
    void delete(BillsBuy_Items billsBuy_items);

    @Update
    void update(BillsBuy_Items billsBuy_items);

    @Query("select * from BillsBuy_Items")
    LiveData<List<BillsBuy_Items>> getBillsBuy_Items();
}


