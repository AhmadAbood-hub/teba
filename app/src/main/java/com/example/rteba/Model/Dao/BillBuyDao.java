package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.BillsBuy;

import java.util.List;

@Dao
public interface BillBuyDao
{
    @Insert
    void insert(BillsBuy billsBuy);

    @Delete
    void delete(BillsBuy billsBuy);

    @Update
    void update(BillsBuy billsBuy);

    @Query("Select * from BillsBuy")
    LiveData<List<BillsBuy>> getBillsBuy();
}


