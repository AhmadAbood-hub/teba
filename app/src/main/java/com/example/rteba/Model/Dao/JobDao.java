package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Jobs;

import java.util.List;

@Dao
public interface JobDao
{
    @Insert
    void insert(Jobs jobs);

    @Delete
    void delete(Jobs jobs);

    @Update
    void update(Jobs jobs);

    @Query("select * from Jobs")
    LiveData<List<Jobs>> getJobs();

    @Query("select ID from Jobs where Name = :Name")
    LiveData<Integer> getID(String Name);

    @Query("select Name from Jobs")
    LiveData<List<String>> getName();

    @Query("select Name from Jobs where ID = :ID")
    LiveData<String> getNameID(int ID);


}


