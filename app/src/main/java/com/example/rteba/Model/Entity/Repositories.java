package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "Repositories")
public class Repositories
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    private int id;
    @ColumnInfo(name = "Location")
    private String location;
    @ColumnInfo(name = "AnnualRent")
    private int annualrent;
    @ColumnInfo(name = "ManegerID")
    private int manegerID;
    private   String ManagerName;



    public Repositories(String location, int annualrent, int manegerID , String ManagerName)
    {
        this.location = location;
        this.annualrent = annualrent;
        this.manegerID = manegerID;
        this.ManagerName =ManagerName;
    }

    public int getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public int getAnnualrent() {
        return annualrent;
    }

    public int getManegerID() {
        return manegerID;
    }

    public void setAnnualrent(int annualrent) {
        this.annualrent = annualrent;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setManegerID(int manegerID) {
        this.manegerID = manegerID;
    }

    public String getManagerName()
    {
        return ManagerName;
    }

    public void setManagerName(String managerName)
    {
        ManagerName = managerName;
    }
}
