package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Custmers;

import java.util.List;

@Dao
public interface CustmerDao
{
    @Insert
    void insert(Custmers custmers);

    @Delete
    void delete(Custmers custmers);

    @Update
    void update(Custmers custmers);

    @Query("select * from Custmers")
    LiveData<List<Custmers>> getCustmers();

    @Query("select NameCustmer from Custmers")
    LiveData<List<String>> getNameCustmers();

    @Query("select IDCustmer from Custmers where NameCustmer= :NameCustmer ")
    LiveData <Integer> getIDCustmers(String NameCustmer);


    @Query("select NameCustmer from Custmers where IDCustmer= :idCustmer")
    LiveData<String> getNameCustmersFromID(int idCustmer);
}


