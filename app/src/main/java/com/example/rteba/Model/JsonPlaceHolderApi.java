package com.example.rteba.Model;

import com.example.rteba.Model.Entity.BillsSale;
import com.example.rteba.Model.Entity.BillsSale_Items;
import com.example.rteba.Model.Entity.Categories;
import com.example.rteba.Model.Entity.Companies;
import com.example.rteba.Model.Entity.Custmers;
import com.example.rteba.Model.Entity.Employees;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.Model.Entity.Jobs;
import com.example.rteba.Model.Entity.Quantities;
import com.example.rteba.Model.Entity.Repositories;
import com.example.rteba.Model.Entity.Types;
import com.example.rteba.Model.Entity.body;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface JsonPlaceHolderApi {
    @GET("api/Types")
    Call<List<Types>> getApiTypes();

    @GET("api/Types")
    Call<List<Types>> getOneType();


    @GET("api/AllQuantities")
    Call<List<Quantities>> getApiAllQuantities();

    @GET("api/AllQuantities")
    Call<List<Quantities>> getOneQuantities();


    @GET("api/AllCategories")
    Call<List<Categories>> getApiAllCategories();

    @GET("api/AllCategories")
    Call<List<Categories>> getOneCategory();


    @GET("api/AllManagers")
    Call<List<Employees>> getApiAllManagers();

    @GET("api/AllManagers")
    Call<List<Employees>> getOneManager();


    @GET("api/Repositories")
    Call<List<Repositories>> getApiAllRepositories();

    @GET("api/Repositories")
    Call<List<Repositories>> getOneRepository();


    @GET("api/AllEmployees")
    Call<List<Employees>> getApiAllEmployees();

    @GET("api/AllEmployees")
    Call<List<Employees>> getOneEmployee();


    @GET("api/AllJobs")
    Call<List<Jobs>> getApiAllJobs();

    @GET("api/AllJobs")
    Call<List<Jobs>> getOneJob();


    @GET("api/AllCustmers")
    Call<List<Custmers>> getApiAllCustmers();

    @GET("api/AllCustmers")
    Call<List<Custmers>> getOneCustmer();


    @GET("api/AllCompanies")
    Call<List<Companies>> getApiAllCompanies();

    @GET("api/AllCompanies")
    Call<List<Companies>> getOneCompany();


    @GET("api/AllItems")
    Call<List<Items>> getApiAllItems();


    @GET("api/AllItems")
    Call<List<Items>> getApiItem(
    );


    @GET("api/AllItem/{categoryID}/{typeID}/{companyID}")
    Call<List<Items>> getApiAllItem(@Path("categoryID") int categoryID, @Path("typeID") int typeID, @Path("companyID") int companyID);


    @GET("api/AllBillsSale/{id}")
    Call<List<BillsSale>> getApiBillsSale(@Path("id") int id);


    @GET("api/AllBillSale")
    Call<List<BillsSale>> getApiAllBillSale();


    @GET("api/AllBillitemsSale/{id}")
    Call<List<BillsSale_Items>> getApiBillItemsSale(@Path("id") int id);


///////////////////////////////////////////////////////////////////////////////////////////////


    @GET("destroyRepository/{id}")
    Call<Void> deleteRepository(@Path("id") int id);

    @GET("destroyCategory/{id}")
    Call<Void> deleteCategory(@Path("id") int id);

    @GET("destroyType/{id}")
    Call<Void> deleteType(@Path("id") int id);

    @GET("destroyQuantity/{id}")
    Call<Void> deleteQuantity(@Path("id") int id);

    @GET("destroyManager/{id}")
    Call<Void> deleteManager(@Path("id") int id);

    @GET("destroyEmployee/{id}")
    Call<Void> deleteEmployee(@Path("id") int id);

    @GET("destroyJob/{id}")
    Call<Void> deleteJob(@Path("id") int id);

    @GET("destroyCustmer/{id}")
    Call<Void> deleteCustmer(@Path("id") int id);

    @GET("DestroyCompany/{id}")
    Call<Void> deleteCompany(@Path("id") int id);

    @GET("destroyItem/{id}")
    Call<Void> deleteItem(@Path("id") int id);

    @GET("destroyBillSale/{id}")
    Call<Void> deleteBillSale(@Path("id") int id);

    @GET("destroyBillSaleItem/{id}/{billSaleItemID}")
    Call<Void> deleteBillSaleItem(@Path("id") int id, @Path("billSaleItemID") int billSaleItemID);


////////////////////////////////////////////////////////////////////////////////////////////

    @Headers({
            "Content-Type: application/json",
            "Authorization: key=AAAA80armDM:APA91bEDIoa_t80OmOG1WY9ctRAbik9IxeqHKWy0Jy7ZNmS7nxVmpLU-mIQ0jnSNooLyABH2wmdcgX1-nwgkLwg2eD1BQDem1erYQ3AQ0i5l7Wf3IVC82T9zNEWs1iR-NISqkey-F-GR"
    })
    @POST("fcm/send")
    Call<ResponseBody> createNotification(@Body body body);


    @POST("api/NewType")
    Call<Types> createType(@Body Types type);

    @POST("api/NewQuantity")
    Call<Quantities> createQuantity(@Body Quantities quantity);

    @POST("api/NewCategory")
    Call<Categories> createCategory(@Body Categories category);

    @POST("api/NewManager")
    Call<Employees> createManager(@Body Employees maneger);

    @POST("api/NewEmployee")
    Call<Employees> createEmployee(@Body Employees employee);

    @POST("api/NewRepository")
    Call<Repositories> createRepository(@Body Repositories repository);

    @POST("api/NewJob")
    Call<Jobs> createJob(@Body Jobs job);

    @POST("api/NewCustmer")
    Call<Custmers> createCustmer(@Body Custmers custmer);

    @Multipart
    @POST("api/NewCompany")
    Call<Companies> createCompany(@Part("companyName") RequestBody name,
                                  @Part("location") RequestBody location,
                                  @Part MultipartBody.Part image);

    @POST("api/NewCompany")
    Call<Companies> createCompanyWithoutImage(@Body Companies companies);


    @Multipart
    @POST("api/NewItem")
    Call<Items> createItem(@Part("companyID") int companyID, @Part("categoryID") int categoryID,
                           @Part("cost") int cost, @Part("repositoryID") int repositoryID,
                           @Part("itemName") RequestBody name, @Part("typeID") int typeID,
                           @Part("expirStart") RequestBody expirStart, @Part("expirEnd") RequestBody expirEnd,
                           @Part("quantityID") int quantityID, @Part("count") int count,
                           @Part("state") int state,
                           @Part MultipartBody.Part img);

    @POST("api/NewBillSale")
    Call<BillsSale> createBillSale(@Body BillsSale billsSale);


    @POST("api/NewBillItemSale")
    Call<BillsSale_Items> createBillItemSale(@Body BillsSale_Items billSale_item);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @POST("api/UpdateCustmer/{id}")
    Call<Custmers> updateCustmer(@Path("id") int id, @Body Custmers custmer);


    @POST("api/UpdateType/{id}")
    Call<Types> updateType(@Path("id") int id, @Body Types type);


    @POST("api/UpdateCategory/{id}")
    Call<Categories> updateCategory(@Path("id") int id, @Body Categories category);


    @POST("api/UpdateQuantity/{id}")
    Call<Quantities> updateQuantity(@Path("id") int id, @Body Quantities quantity);


    @POST("api/UpdateRepository/{id}")
    Call<Repositories> updateRepository(@Path("id") int id, @Body Repositories repository);


    @POST("api/UpdateManager/{id}")
    Call<Employees> updateManager(@Path("id") int id, @Body Employees employee);


    @POST("api/UpdateEmployee/{id}")
    Call<Employees> updateEmployee(@Path("id") int id, @Body Employees employee);


    @POST("api/UpdateJob/{id}")
    Call<Jobs> updateJob(@Path("id") int id, @Body Jobs job);


    @Multipart
    @POST("api/UpdateCompany/{id}")
    Call<Companies> updateCompany(@Path("id") int id, @Part("companyName") RequestBody name,
                                  @Part("location") RequestBody location,
                                  @Part MultipartBody.Part image);


    @Multipart
    @POST("api/UpdateItem/{id}")
    Call<Items> updateItem(@Path("id") int id, @Part("categoryID") int categoryID,
                           @Part("cost") int cost, @Part("repositoryID") int repositoryID,
                           @Part("itemName") RequestBody name, @Part("typeID") int typeID,
                           @Part("expirStart") RequestBody expirStart, @Part("expirEnd") RequestBody expirEnd,
                           @Part("quantityID") int quantityID, @Part("count") int count,
                           @Part MultipartBody.Part img);


    @POST("api/UpdateCountItem/{id}/{count}")
    Call<Items> updateCountItem(@Path("id") int id, @Path("count") int count);


    @POST("api/UpdateBillSaleItem/{id}")
    Call<BillsSale_Items> updateBillSaleItem(@Path("id") int id, @Body BillsSale_Items billSale_item);


}
