package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Employees")
public class Employees
{


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "EmpID")
    private int empid;
    @ColumnInfo(name = "EmpName")
    private String name;
    @ColumnInfo(name = "JobID")
    private int jobID;
    @ColumnInfo(name = "Number")
    private String number;
    @ColumnInfo(name = "ManegerID")
    private int manegerID;

    private   String jobName;
    @Embedded
    private Jobs jobs;

    public Employees( String name, int jobID, String number, int manegerID, String jobName)
    {

        this.name = name;
        this.jobID = jobID;
        this.number = number;
        this.manegerID = manegerID;
        this.jobName = jobName;
    }



    public String getName() {
        return name;
    }

    public int getJobID() {
        return jobID;
    }

    public int getManegerID() {
        return manegerID;
    }

    public String getNumber() {
        return number;
    }

    public int getEmpid()
    {
        return empid;
    }

    public void setEmpid(int empid)
    {
        this.empid = empid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setJobID(int jobID) {
        this.jobID = jobID;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setManegerID(int manegerID) {
        this.manegerID = manegerID;
    }

    public Jobs getJobs()
    {
        return jobs;
    }

    public void setJobs(Jobs jobs)
    {
        this.jobs = jobs;
    }

    public String getJobName()
    {
        return jobName;
    }

    public void setJobName(String jobName)
    {
        this.jobName = jobName;
    }
}
