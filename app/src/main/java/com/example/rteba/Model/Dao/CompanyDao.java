package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Companies;

import java.util.List;

@Dao
public interface CompanyDao
{
    @Insert
    void insert(Companies companies);

    @Delete
    void delete(Companies companies);

    @Update
    void update(Companies companies);

    @Query("select * from Companies")
    LiveData<List<Companies>> getCompanies();

    @Query("select * from Companies where CompanyApiID = :ID")
    LiveData<Companies> getOneCompany(int ID);
}


