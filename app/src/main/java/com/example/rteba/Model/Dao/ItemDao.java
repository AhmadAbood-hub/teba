package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Items;

import java.util.List;

@Dao
public interface ItemDao
{
    @Insert
    void insert(Items items);

    @Delete
    void delete(Items items);

    @Update
    void update(Items items);

    @Query("select * from Items")
    LiveData<List<Items>> getItem();

    @Query("select * from Items where CompanyID = :CompanyID")
    LiveData<List<Items>> getItems(int CompanyID);

    @Query("select * from Items where State = 1")
    LiveData<List<Items>> getFavItems();

    @Query("select * from Items where ItemApiID = :ItemID")
    LiveData<Items> getItemFromID(int ItemID);







}


