package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Employees;

import java.util.List;

@Dao
public interface EmployeeDao
{
    @Insert
    void insert(Employees Employees);

    @Delete
    void delete(Employees Employees);

    @Update
    void update(Employees Employees);

    @Query("select * from Employees where ManegerID!=0 ")
    LiveData<List<Employees>> getEmployees();

    @Query("select * from Employees where ManegerID=0 ")
    LiveData<List<Employees>> getManagers();

    @Query("select EmpName from Employees where ManegerID=0 ")
    LiveData<List<String>> getNameManager();

    @Query("select EmpID from Employees where ManegerID=0 and EmpName = :Name")
    LiveData<Integer> getID(String Name);

    @Query("select EmpName from Employees where EmpID = :ID and ManegerID=0 ")
    LiveData<String> getNameID(int ID);

}


