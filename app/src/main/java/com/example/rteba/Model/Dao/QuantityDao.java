package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Quantities;

import java.util.List;

@Dao
public interface QuantityDao
{
    @Insert
    void insert(Quantities quantity);

    @Delete
    void delete(Quantities quantity);

    @Update
    void update(Quantities quantity);

    @Query("select * from Quantities")
    LiveData<List<Quantities>> getQuantity();


    @Query("select ID from Quantities where Quantities = :size and Unit = :unit ")
    LiveData<Integer> getID(int size, String unit);



    @Query("select Quantities from Quantities")
    LiveData<List<Integer>> getSize();



    @Query("select Quantities from Quantities where ID= :ID")
    LiveData<Integer> getOneSize(String ID);



    @Query("select Unit from Quantities")
    LiveData<List<String>> getUnit();



    @Query("select Unit from Quantities where ID= :ID")
    LiveData<String> getOneUnit(String ID);

}


