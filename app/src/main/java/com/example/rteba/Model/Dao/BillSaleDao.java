package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.BillsSale;

import java.util.List;

@Dao
public interface BillSaleDao
{
    @Insert
    void insert(BillsSale billsSale);

    @Delete
    void delete(BillsSale billsSale);

    @Update
    void update(BillsSale billsSale);

    @Query("Select * from billssale where CustmerID= :CustmerID")
    LiveData<List<BillsSale>> getBillsSale(int CustmerID);

    @Query("Select * from billssale")
    LiveData<List<BillsSale>> getAllBillsSale();


    @Query("Select Max(BillsApiID) From billssale")
    int getMaxBillSaleID();


}


