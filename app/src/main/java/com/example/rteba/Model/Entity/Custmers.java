package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "Custmers")
public class Custmers

{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "IDCustmer")
    private int idCustmer;
    @ColumnInfo(name = "NameCustmer")
    private String nameCustmer;
    @ColumnInfo(name = "NumberCustmer")
    private String numberCustmer;
    @ColumnInfo(name = "State")
    private int state;

    public Custmers(String nameCustmer, String numberCustmer, int state) {
        this.nameCustmer = nameCustmer;
        this.numberCustmer = numberCustmer;
        this.state = state;
    }

    public int getIdCustmer() {
        return idCustmer;
    }

    public void setIdCustmer(int idCustmer) {
        this.idCustmer = idCustmer;
    }

    public String getNameCustmer() {
        return nameCustmer;
    }

    public void setNameCustmer(String nameCustmer) {
        this.nameCustmer = nameCustmer;
    }

    public String getNumberCustmer() {
        return numberCustmer;
    }

    public void setNumberCustmer(String numberCustmer) {
        this.numberCustmer = numberCustmer;
    }

    public int getState()
    {
        return state;
    }

    public void setState(int state)
    {
        this.state = state;
    }
}
