package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "Companies")
public class Companies
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "CompanyApiID")
    private int CompanyApiID;
    @ColumnInfo(name = "Name")
    private String companyName;
    @ColumnInfo(name = "Location")
    private String location;
    @ColumnInfo(name = "Image", typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    private String logo;

    public Companies(String companyName, String location, byte[] image)
    {
        this.companyName = companyName;
        this.location = location;
        this.image = image;
    }

    public int getCompanyPreID()
    {
        return CompanyApiID;
    }

    public void setCompanyPreID(int companyPreID)
    {
        this.CompanyApiID = companyPreID;
    }



    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public byte[] getImage()
    {
        return image;
    }

    public void setImage(byte[] image)
    {
        this.image = image;
    }

    public String getLogo()
    {
        return logo;
    }

    public void setLogo(String logo)
    {
        this.logo = logo;
    }

    public int getCompanyApiID()
    {
        return CompanyApiID;
    }

    public void setCompanyApiID(int companyApiID)
    {
        CompanyApiID = companyApiID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
