package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "BillsSale")
public class BillsSale
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "BillsApiID")
 private int billsApiID;
    @ColumnInfo(name = "WriterID")
 private int writerid;
    @ColumnInfo(name = "Date")
 private String date;
    @ColumnInfo(name = "CustmerID")
 private int custmerID;
    @ColumnInfo(name = "Time")
    private String time;

    private String nameCustmer;


    public BillsSale(int writerid, String date, int custmerID, String time, String nameCustmer)
    {
        this.writerid = writerid;
        this.date = date;
        this.custmerID = custmerID;
        this.time = time;
        this.nameCustmer = nameCustmer;
    }

    public int getBillsApiID()
    {
        return billsApiID;
    }

    public void setBillsApiID(int billsApiID)
    {
        this.billsApiID = billsApiID;
    }

    public int getWriterid() {
        return writerid;
    }

    public void setWriterid(int writerid) {
        this.writerid = writerid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCustmerID() {
        return custmerID;
    }

    public void setCustmerID(int custmerID) {
        this.custmerID = custmerID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNameCustmer() {
        return nameCustmer;
    }

    public void setNameCustmer(String nameCustmer) {
        this.nameCustmer = nameCustmer;
    }
}
