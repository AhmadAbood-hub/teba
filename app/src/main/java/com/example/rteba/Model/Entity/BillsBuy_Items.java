package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "BillsBuy_Items")
public class BillsBuy_Items
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
private int id;
    @ColumnInfo(name = "BillbuyID")
private int billbuyID;
    @ColumnInfo(name = "ItemID")
private int itemID;
    @ColumnInfo(name = "Count")
private  int count;


    public BillsBuy_Items(int id, int billbuyID, int itemID, int count)
    {
        this.id = id;
        this.billbuyID = billbuyID;
        this.itemID = itemID;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public int getBillbuyID() {
        return billbuyID;
    }

    public int getItemID() {
        return itemID;
    }

    public int getCount() {
        return count;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBillbuyID(int billbuyID) {
        this.billbuyID = billbuyID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

