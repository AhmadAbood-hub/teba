package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "Items",
        foreignKeys = @ForeignKey(
                entity = Companies.class,
                parentColumns = "CompanyApiID",
                childColumns = "CompanyID",
                onDelete = CASCADE))
public class Items
{


    public int getItemApiID() {
        return ItemApiID;
    }

    public void setItemApiID(int itemApiID) {
        ItemApiID = itemApiID;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public void setRepositoryID(int repositoryID) {
        this.repositoryID = repositoryID;
    }


    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setExpirStart(String expirStart) {
        this.expirStart = expirStart;
    }

    public void setExpirEnd(String expirEnd) {
        this.expirEnd = expirEnd;
    }

    public void setQuantityID(int quantityID) {
        this.quantityID = quantityID;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getQuantityID()
    {
        return quantityID;
    }

    public int getId()
    {
        return id;
    }

    public int getCompanyID() {
        return companyID;
    }

    public int getTypeID() {
        return typeID;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public int getCost() {
        return cost;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public byte[] getImg() {
        return img;
    }

    public int getRepositoryID() {
        return repositoryID;
    }


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getExpirStart() {
        return expirStart;
    }

    public String getExpirEnd() {
        return expirEnd;
    }



    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ItemID")
    private int id;
    @ColumnInfo(name = "CompanyID")
    private int companyID;
    @ColumnInfo(name = "CategoryID")
    private int categoryID;
    @ColumnInfo(name = "Cost")
    private int cost;
    @ColumnInfo(name = "Img", typeAffinity = ColumnInfo.BLOB)
    private byte[] img;
    @ColumnInfo(name = "RepositoryID")
    private int repositoryID;
    @ColumnInfo(name = "ItemName")
    private String itemName;
    @ColumnInfo(name = "TypeID")
    private int typeID;
    @ColumnInfo(name = "ExpirStart")
    private String expirStart;
    @ColumnInfo(name = "ExpirEnd")
    private String expirEnd;
    @ColumnInfo(name = "QuantityID")
    private int quantityID;
    @ColumnInfo(name = "Count")
    private int count;
    @ColumnInfo(name = "Order")
    private int order;
    @ColumnInfo(name = "State")
    private int state;

    @ColumnInfo(name = "ItemApiID")
    private int ItemApiID;

    private String Url;

    private String companyName;


    public Items(int quantityID, int companyID, int categoryID, int cost, byte[] img, int repositoryID, String itemName, int typeID, String expirStart, String expirEnd,int count,int state)
    {
        this.quantityID = quantityID;
        this.companyID = companyID;
        this.categoryID = categoryID;
        this.cost = cost;
        this.img = img;
        this.repositoryID = repositoryID;
        this.itemName = itemName;
        this.typeID = typeID;
        this.expirStart = expirStart;
        this.expirEnd = expirEnd;
        this.count =count;
        this.state =state;

    }
}
