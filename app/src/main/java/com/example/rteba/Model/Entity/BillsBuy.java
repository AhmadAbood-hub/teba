package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "BillsBuy")
public class BillsBuy
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    private int id;
    @ColumnInfo(name = "Date")
    private String date;
    @ColumnInfo(name = "WritterID")
    private int writterid;
    @ColumnInfo(name = "CompanyID")
    private int companyid;

    public BillsBuy(int id, String date, int writterid, int companyid)
    {
        this.id = id;
        this.date = date;
        this.writterid = writterid;
        this.companyid = companyid;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public int getWritterid() {
        return writterid;
    }

    public int getCompanyid() {
        return companyid;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setWritterid(int writterid) {
        this.writterid = writterid;
    }

    public void setCompanyid(int companyid) {
        this.companyid = companyid;
    }
}
