package com.example.rteba.Model.Entity;

import com.google.gson.annotations.SerializedName;

public class body
{
    @SerializedName("to")
    String to ;
    @SerializedName("priority")
    String priority;
    @SerializedName("notification")
    com.example.rteba.Model.Entity.notification notification;


    public body(String to, String priority, notification notification)
    {
        this.to = to;
        this.priority = priority;
        this.notification = notification;
    }


    public String getTo()
    {
        return to;
    }

    public void setTo(String to)
    {
        this.to = to;
    }

    public String getPriority()
    {
        return priority;
    }

    public void setPriority(String priority)
    {
        this.priority = priority;
    }

    public notification getNotification()
    {
        return notification;
    }

    public void setNotification( notification notification)
    {
        this.notification = notification;
    }
}
