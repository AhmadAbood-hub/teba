package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "Jobs")
public class Jobs
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    private int id;
    @ColumnInfo(name = "Name")
    private String name;
    @ColumnInfo(name = "Salary")
    private int salary;
    @ColumnInfo(name = "Hour")
    private int hour;
    @ColumnInfo(name = "CostPerHour")
    private int costPerHour;




    public Jobs(String name, int salary, int hour, int costPerHour)
    {

        this.name = name;
        this.salary = salary;
        this.hour = hour;
        this.costPerHour = costPerHour;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }

    public int getHour() {
        return hour;
    }

    public int getCostPerHour() {
        return costPerHour;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setCostPerHour(int costPerHour) {
        this.costPerHour = costPerHour;
    }
}

