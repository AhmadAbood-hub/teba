package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Types;

import java.util.List;

@Dao
public interface TypeDao
{
    @Insert
    void insert(Types types);

    @Delete
    void delete(Types types);

    @Update
    void update(Types types);

    @Query("select * from Types")
    LiveData<List<Types>> getTypes();



    @Query("select Name from Types")
    LiveData<List<String>> getName();



    @Query("select ID from Types where Name= :Name")
    LiveData<Integer> getID(String Name);



    @Query("select Name from Types where ID= :ID")
    LiveData<String> getOneName(String ID);

}


