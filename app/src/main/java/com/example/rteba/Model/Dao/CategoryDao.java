package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Categories;

import java.util.List;

@Dao
public interface CategoryDao
{
    @Insert
    void insert(Categories categories);

    @Delete
    void delete(Categories categories);

    @Update
    void update(Categories categories);

    @Query("select * from Categories")
    LiveData<List<Categories>> getCategories();


    @Query("select Name from Categories")
    LiveData<List<String>> getName();

    @Query("select Name from Categories where ID= :ID")
    LiveData<String> getOneName(String ID);



    @Query("select ID from Categories where Name=:Name")
    LiveData<Integer> getID(String Name);


}


