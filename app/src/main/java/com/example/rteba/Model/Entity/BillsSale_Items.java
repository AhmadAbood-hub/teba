package com.example.rteba.Model.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "BillsSale_Items",
       foreignKeys = @ForeignKey(
       entity = BillsSale.class,
       parentColumns = "BillsApiID",
      childColumns = "BillSaleID",
       onDelete = CASCADE)
        )
public class BillsSale_Items
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "ID")
    private int ID;
    @ColumnInfo(name = "BillSaleID")
    private int billSaleID;
    @ColumnInfo(name = "BillSaleItemID")
    private int billSaleItemID;
    @ColumnInfo(name = "BillSaleItemCount")
    private  int billSaleItemCount;

    private int cost;

    private String companyName;

    private String itemName;

    private String Url;





    public BillsSale_Items( int billSaleID, int billSaleItemID, int billSaleItemCount) {

        this.billSaleID = billSaleID;
        this.billSaleItemID = billSaleItemID;
        this.billSaleItemCount = billSaleItemCount;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getBillSaleID() {
        return billSaleID;
    }

    public void setBillSaleID(int billSaleID) {
        this.billSaleID = billSaleID;
    }

    public int getBillSaleItemID() {
        return billSaleItemID;
    }

    public void setBillSaleItemID(int billSaleItemID) {
        this.billSaleItemID = billSaleItemID;
    }

    public int getBillSaleItemCount() {
        return billSaleItemCount;
    }

    public void setBillSaleItemCount(int billSaleItemCount) {
        this.billSaleItemCount = billSaleItemCount;
    }

    public String getUrl()
    {
        return Url;
    }

    public void setUrl(String url)
    {
        Url = url;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getCompanyName()
    {
        return companyName;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }
}
