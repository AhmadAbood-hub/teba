package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.BillsSale_Items;

import java.util.List;

@Dao
public interface BillSaleItemDao
{
    @Insert
    void insert(BillsSale_Items billsSale_items);

    @Delete
    void delete(BillsSale_Items billsSale_items);

    @Update
    void update(BillsSale_Items billsSale_items);

    @Query("select * from BillsSale_Items where BillSaleID = :IDBillSale")
    LiveData<List<BillsSale_Items>> getBillsSaleItems(int IDBillSale);

}


