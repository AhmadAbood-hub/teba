package com.example.rteba.Model.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.rteba.Model.Entity.Repositories;

import java.util.List;

@Dao
public interface RepositoryDao
{


    @Insert
    void insert(Repositories repository);

    @Delete
    void delete(Repositories repository);

    @Update
    void update(Repositories repository);

    @Query("select * from Repositories")
    LiveData<List<Repositories>> getRepository();

    @Query("select Location from Repositories")
    LiveData<List<String>> getLocatin();

    @Query("select ID from Repositories where Location= :Location")
    LiveData<Integer> getID(String Location);

    @Query("select Location from Repositories where ID= :ID")
    LiveData<String> getOneLocatin(int ID);


}


