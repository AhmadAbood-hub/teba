package com.example.rteba.Model.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.rteba.Model.Dao.BillBuyDao;
import com.example.rteba.Model.Dao.BillBuyItemDao;
import com.example.rteba.Model.Dao.BillSaleDao;
import com.example.rteba.Model.Dao.BillSaleItemDao;
import com.example.rteba.Model.Dao.CategoryDao;
import com.example.rteba.Model.Dao.CompanyDao;
import com.example.rteba.Model.Dao.CustmerDao;
import com.example.rteba.Model.Dao.EmployeeDao;
import com.example.rteba.Model.Dao.ItemDao;
import com.example.rteba.Model.Dao.JobDao;
import com.example.rteba.Model.Dao.QuantityDao;
import com.example.rteba.Model.Dao.RepositoryDao;
import com.example.rteba.Model.Dao.TypeDao;
import com.example.rteba.Model.Entity.BillsBuy;
import com.example.rteba.Model.Entity.BillsBuy_Items;
import com.example.rteba.Model.Entity.BillsSale;
import com.example.rteba.Model.Entity.BillsSale_Items;
import com.example.rteba.Model.Entity.Categories;
import com.example.rteba.Model.Entity.Companies;
import com.example.rteba.Model.Entity.Custmers;
import com.example.rteba.Model.Entity.Employees;
import com.example.rteba.Model.Entity.Items;
import com.example.rteba.Model.Entity.Jobs;
import com.example.rteba.Model.Entity.Quantities;
import com.example.rteba.Model.Entity.Repositories;
import com.example.rteba.Model.Entity.Types;

@Database(entities = { BillsBuy.class, BillsBuy_Items.class, Items.class, BillsSale.class, BillsSale_Items.class, Categories.class
        , Companies.class, Custmers.class, Employees.class, Jobs.class, Quantities.class, Repositories.class, Types.class}, version =2)
public abstract class RoomDatabaseTebaVet extends RoomDatabase
{
    private static volatile RoomDatabaseTebaVet instance;

    public static RoomDatabaseTebaVet getDatabase(final Context context)
    {
        if (instance == null)
        {
            synchronized (RoomDatabaseTebaVet.class)
            {
                if (instance == null)
                {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabaseTebaVet.class, "TebaVetDb")
                            .build();
                }
            }
        }
        return instance;
    }

    public abstract BillBuyDao getBillBuyDao();

    public abstract BillBuyItemDao getBillBuyItemDao();

    public abstract ItemDao getItemsDao();

    public abstract CategoryDao getCategoriesDao();

    public abstract BillSaleDao getBillSaleDao();

    public abstract BillSaleItemDao getBillSaleItemDao();

    public abstract TypeDao getTypesDao();

    public abstract RepositoryDao getRepositoryDao();

    public abstract QuantityDao getQuantityDao();

    public abstract EmployeeDao getEmployeesDao();

    public abstract CustmerDao getCustmersDao();

    public abstract CompanyDao getCompaniesDao();

    public abstract JobDao getJobsDao();

}
