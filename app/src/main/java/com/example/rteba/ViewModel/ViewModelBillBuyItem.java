package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.BillsBuy_Items;
import com.example.rteba.Repository.RepositoryBillBuyItem;

import java.util.List;

public class ViewModelBillBuyItem extends AndroidViewModel
{
    private RepositoryBillBuyItem RepositoryBillBuyItem;
    private LiveData<List<BillsBuy_Items>> billbuyItem;
    public ViewModelBillBuyItem(Application application)
    {
        super(application);
        RepositoryBillBuyItem = new RepositoryBillBuyItem(application);
    }
    public void insert(BillsBuy_Items BillsBuy_Items)
    {
        RepositoryBillBuyItem.insert(BillsBuy_Items);
    }

    public void delete(BillsBuy_Items BillsBuy_Items)
    {
        RepositoryBillBuyItem.delete(BillsBuy_Items);
    }

    public void update(BillsBuy_Items BillsBuy_Items)
    {
        RepositoryBillBuyItem.update(BillsBuy_Items);
    }

    public LiveData<List<BillsBuy_Items>> getBillsBuy_Items()
    {
        billbuyItem = RepositoryBillBuyItem.getBillsBuy_Items();
        return billbuyItem;
    }

}
