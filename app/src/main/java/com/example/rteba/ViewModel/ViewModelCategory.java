package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Categories;
import com.example.rteba.Repository.RepositoryCategory;

import java.util.List;

public class ViewModelCategory extends AndroidViewModel
{
    private RepositoryCategory RepositoryCategory;
    private LiveData<List<Categories>> Company;
    public ViewModelCategory(Application application)
    {
        super(application);
        RepositoryCategory = new RepositoryCategory(application);
    }
    public void insert(Categories Categories)
    {
        RepositoryCategory.insert(Categories);
    }

    public void delete(Categories Categories)
    {
        RepositoryCategory.delete(Categories);
    }

    public void update(Categories Categories)
    {
        RepositoryCategory.update(Categories);
    }



    public LiveData<List<String>> getName()
    {

        return RepositoryCategory.getName();
    }

    public LiveData<Integer> getID(String Name)
    {

        return RepositoryCategory.getID(Name);
    }

    public LiveData<String> getOneName(String ID)
    {

        return RepositoryCategory.getOneName(ID);
    }



    public LiveData<List<Categories>> getCategory()
    {
        Company = RepositoryCategory.getCategory();
        return Company;
    }

}
