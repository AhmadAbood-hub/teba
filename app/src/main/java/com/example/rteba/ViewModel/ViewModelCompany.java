package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Companies;
import com.example.rteba.Repository.RepositoryCompany;

import java.util.List;

public class ViewModelCompany extends AndroidViewModel
{
    private static RepositoryCompany RepositoryCompany;
    private LiveData<List<Companies>> Company;
    public ViewModelCompany(Application application)
    {
        super(application);
        RepositoryCompany = new RepositoryCompany(application);
    }
    public  void insert(Companies Companies)
    {
        RepositoryCompany.insert(Companies);
    }

    public void delete(Companies Companies)
    {
        RepositoryCompany.delete(Companies);
    }

    public void update(Companies Companies)
    {
        RepositoryCompany.update(Companies);
    }

    public LiveData<List<Companies>> getCompany()
    {
        Company = RepositoryCompany.getCompany();
        return Company;
    }

    public LiveData<Companies> getOneCompany(int ID)
    {
        return RepositoryCompany.getOneCompany(ID);
    }

}
