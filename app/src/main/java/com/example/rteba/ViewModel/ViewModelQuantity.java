package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Quantities;
import com.example.rteba.Repository.RepositoryQuantity;

import java.util.List;

public class ViewModelQuantity extends AndroidViewModel
{
    private RepositoryQuantity RepositoryQuantity;
    private LiveData<List<Quantities>> Quantity;
    public ViewModelQuantity(Application application)
    {
        super(application);
        RepositoryQuantity = new RepositoryQuantity(application);
    }
    public void insert(Quantities Quantities)
    {
        RepositoryQuantity.insert(Quantities);
    }

    public void delete(Quantities Quantities)
    {
        RepositoryQuantity.delete(Quantities);
    }

    public void update(Quantities Quantities)
    {
        RepositoryQuantity.update(Quantities);
    }

    public LiveData<List<Quantities>> getQuantity()
    {
        Quantity = RepositoryQuantity.getQuantity();
        return Quantity;
    }


    public LiveData<Integer> getID(int size , String unit)
    {

        return   RepositoryQuantity.getID(size,unit);

    }


    public LiveData<Integer> getOneSize(String ID)

    {
        return  RepositoryQuantity.getOneSize(ID) ;
    }


    public LiveData<List<Integer>> getSize()
    {
        return  RepositoryQuantity.getSize() ;
    }


    public LiveData<List<String>> getUnit()
    {
        return  RepositoryQuantity.getUnit() ;
    }

    public LiveData<String> getOneUnit(String ID)
    {
        return  RepositoryQuantity.getOneUnit(ID) ;
    }


}
