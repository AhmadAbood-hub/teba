package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Jobs;
import com.example.rteba.Repository.RepositoryJob;

import java.util.List;

public class ViewModelJob extends AndroidViewModel
{
    private RepositoryJob RepositoryJob;
    private LiveData<List<Jobs>> Job;
    private LiveData<List<String>> Name;

    public ViewModelJob(Application application)
    {
        super(application);
        RepositoryJob = new RepositoryJob(application);
        Job = RepositoryJob.getJob();
        Name =RepositoryJob.getName();
    }
    public void insert(Jobs Jobs)
    {
        RepositoryJob.insert(Jobs);
    }

    public void delete(Jobs Jobs)
    {
        RepositoryJob.delete(Jobs);
    }

    public void update(Jobs Jobs)
    {
        RepositoryJob.update(Jobs);
    }

    public LiveData<List<Jobs>> getJob()
    {

        return Job;
    }


    public LiveData<Integer> getID(String Name)
    {
        return   RepositoryJob.getID(Name);
    }

    public LiveData<List<String>> getName()
    {
        return Name;
    }

    public LiveData<String> getNameID(int ID)
    {
        return RepositoryJob.getNameID(ID);
    }
}
