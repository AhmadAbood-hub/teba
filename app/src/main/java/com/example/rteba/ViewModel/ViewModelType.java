package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Types;
import com.example.rteba.Repository.RepositoryType;

import java.util.List;

public class ViewModelType extends AndroidViewModel
{
    private RepositoryType repositoryType;
    private LiveData<List<Types>> Type;
    public ViewModelType(Application application)
    {
        super(application);
        repositoryType = new RepositoryType(application);
    }
    public void insert(Types Types)
    {
        repositoryType.insert(Types);
    }

    public void delete(Types Types)
    {
        repositoryType.delete(Types);
    }

    public void update(Types Types)
    {
        repositoryType.update(Types);
    }

    public LiveData<List<Types>> getTypes()
    {
        Type = repositoryType.getTypes();
        return Type;
    }

    public LiveData<List<String>> getName()
    {
        return repositoryType.getName();
    }

    public LiveData<String> getOneName(String ID)
    {

        return repositoryType.getOneName(ID);
    }

    public LiveData<Integer> getID(String Name)
    {

        return repositoryType.getID(Name);
    }


}
