package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.BillsSale_Items;
import com.example.rteba.Repository.RepositoryBillSaleItem;
import com.example.rteba.Repository.insertTest;

import java.util.ArrayList;
import java.util.List;

public class ViewModelBillSaleItem extends AndroidViewModel
{
    private insertTest insertTest;
    private RepositoryBillSaleItem RepositoryBillSaleItem;
    private LiveData<List<BillsSale_Items>> billsaleitems;
    public ViewModelBillSaleItem(Application application)
    {
        super(application);
        RepositoryBillSaleItem = new RepositoryBillSaleItem(application);
    }
    public void insert(BillsSale_Items billsSale_items)
    {
        RepositoryBillSaleItem.insert(billsSale_items);
    }


    public void insertList(List<BillsSale_Items> billsSale_items)
    {
        insertTest.insert(billsSale_items);
    }



    public void delete(BillsSale_Items BillsSale_Items)
    {
        RepositoryBillSaleItem.delete(BillsSale_Items);
    }

    public void update(BillsSale_Items BillsSale_Items)
    {
        RepositoryBillSaleItem.update(BillsSale_Items);
    }

    public LiveData<List<BillsSale_Items>> getBillsSaleItems(int IDBillSale)
    {
        billsaleitems = RepositoryBillSaleItem.getBillsSaleItems(IDBillSale);
        return billsaleitems;
    }

}
