package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Repositories;
import com.example.rteba.Repository.RepositoryRepository;

import java.util.List;

public class ViewModelRepository extends AndroidViewModel
{
    private RepositoryRepository RepositoryRepository;
    private LiveData<List<Repositories>> Repository;
    public ViewModelRepository(Application application)
    {
        super(application);
        RepositoryRepository = new RepositoryRepository(application);
        Repository = RepositoryRepository.getRepository();
    }
    public void insert(Repositories Repositories)
    {
        RepositoryRepository.insert(Repositories);
    }

    public void delete(Repositories Repositories)
    {
        RepositoryRepository.delete(Repositories);
    }

    public void update(Repositories Repositories)
    {
        RepositoryRepository.update(Repositories);
    }




    public LiveData<List<Repositories>> getRepository()
    {

        return Repository;
    }


    public LiveData<List<String>> getLocation()
    {

        return RepositoryRepository.getLocation();
    }



    public LiveData<Integer> getID(String Location)
    {
        return RepositoryRepository.getID(Location);
    }
    public LiveData<String> getOneLocation(int ID)
    {

        return RepositoryRepository.getOneLocation(ID);
    }

}
