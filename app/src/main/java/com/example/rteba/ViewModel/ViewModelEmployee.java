package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Employees;
import com.example.rteba.Repository.RepositoryEmployee;

import java.util.List;

public class ViewModelEmployee extends AndroidViewModel
{
    private RepositoryEmployee RepositoryEmployee;
    private LiveData<List<Employees>> Employee;
    private LiveData<List<Employees>> Manager;
    private LiveData<Integer> resultID;
    private LiveData<List<String>> managerName;
    public ViewModelEmployee(Application application)
    {
        super(application);

        RepositoryEmployee = new RepositoryEmployee(application);
        resultID = RepositoryEmployee.getResultID();
        managerName =RepositoryEmployee.getNameManager();
    }

    public LiveData<Integer> getResultID()
    {
        return resultID;
    }

    public void insert(Employees Employees)
    {
        RepositoryEmployee.insert(Employees);
    }

    public void delete(Employees Employees)
    {
        RepositoryEmployee.delete(Employees);
    }

    public void update(Employees Employees)
    {
        RepositoryEmployee.update(Employees);
    }

    public LiveData<List<Employees>> getEmployee()
    {
        Employee = RepositoryEmployee.getEmployee();
        return Employee;
    }

    public LiveData<List<Employees>> getManager()
    {
        Manager = RepositoryEmployee.getManager();
        return Manager;
    }
    public LiveData<List<String>> getNameManager()
    {

        return managerName;
    }
    public LiveData<String> getNameID(int ID)
    {
        return RepositoryEmployee.getNameID(ID);
    }
    public LiveData<Integer> getID(String Name)
    {
        return   RepositoryEmployee.getID(Name);
    }
}
