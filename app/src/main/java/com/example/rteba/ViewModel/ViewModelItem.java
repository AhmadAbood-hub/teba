package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Items;
import com.example.rteba.Repository.RepositoryItem;

import java.util.List;

public class ViewModelItem extends AndroidViewModel
{
    private RepositoryItem RepositoryItem;

    public ViewModelItem(Application application)
    {
        super(application);
        RepositoryItem = new RepositoryItem(application);
    }
    public void insert(Items Items)
    {
        RepositoryItem.insert(Items);
    }

    public void delete(Items Items)
    {
        RepositoryItem.delete(Items);
    }

    public void update(Items Items)
    {
        RepositoryItem.update(Items);
    }

    public LiveData<List<Items>> getItems(int CompanyID)
    {
        return RepositoryItem.getItems(CompanyID);
    }


    public LiveData<Items> getItemFromID(int itemID)
    {

        return RepositoryItem.getItemFromID(itemID);
    }




    public LiveData<List<Items>> getItem()
    {
        return RepositoryItem.getItem();
    }

}
