package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.BillsSale;
import com.example.rteba.Repository.RepositoryBillSale;

import java.util.List;

public class ViewModelBillSale extends AndroidViewModel
{
    private LiveData<Integer> resultBillAdded;
    private RepositoryBillSale RepositoryBillSale;
    private LiveData<List<BillsSale>> billsale;
    private LiveData<List<BillsSale>> AllbillSale;

    public ViewModelBillSale(Application application)
    {
        super(application);
        RepositoryBillSale = new RepositoryBillSale(application);
        resultBillAdded = RepositoryBillSale.getResultBillAdded();

    }
    public void insert(BillsSale BillsSale)
    {
        RepositoryBillSale.insert(BillsSale);
    }

    public void delete(BillsSale BillsSale)
    {
        RepositoryBillSale.delete(BillsSale);
    }

    public void update(BillsSale BillsSale)
    {
        RepositoryBillSale.update(BillsSale);
    }

    public LiveData<List<BillsSale>> getBillSale(int CustmerID)
    {
        billsale = RepositoryBillSale.getBillSale(CustmerID);
        return billsale;
    }


    public LiveData<List<BillsSale>> getAllBillsSale()
    {
        AllbillSale = RepositoryBillSale.getAllBillsSale();
        return AllbillSale;
    }


    public LiveData<Integer> getResultBillAdded()
    {
        return resultBillAdded;
    }
}
