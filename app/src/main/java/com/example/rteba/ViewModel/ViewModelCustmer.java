package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.Custmers;
import com.example.rteba.Repository.RepositoryCustmer;

import java.util.List;

public class ViewModelCustmer extends AndroidViewModel
{
    private RepositoryCustmer RepositoryCustmer;
    private LiveData<List<Custmers>> Custmer;
    private LiveData<Integer> resultID;
    public ViewModelCustmer(Application application)
    {
        super(application);
        RepositoryCustmer = new RepositoryCustmer(application);
        resultID = RepositoryCustmer.getResultID();
    }
    public void insert(Custmers Custmers)
    {
        RepositoryCustmer.insert(Custmers);
    }

    public void delete(Custmers Custmers)
    {
        RepositoryCustmer.delete(Custmers);
    }

    public void update(Custmers Custmers)
    {
        RepositoryCustmer.update(Custmers);
    }

    public LiveData<List<Custmers>> getCustmer()
    {
        Custmer = RepositoryCustmer.getCustmer();
        return Custmer;
    }

    public LiveData<List<String>> getNameCustmer()
    {

        return RepositoryCustmer.getNameCustmer();
    }

    public LiveData<Integer> getIDCustmers(String NameCustmer)
    {

        return RepositoryCustmer.getIDCustmers(NameCustmer);
    }


    public LiveData<String> getNameCustmersFromID(int idCustmer)
    {
        return RepositoryCustmer.getNameCustmersFromID(idCustmer);
    }


    public LiveData<Integer> getResultID()
    {
        return resultID;
    }
}
