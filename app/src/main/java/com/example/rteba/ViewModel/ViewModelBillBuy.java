package com.example.rteba.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.rteba.Model.Entity.BillsBuy;
import com.example.rteba.Repository.RepositoryBillBuy;

import java.util.List;

public class ViewModelBillBuy extends AndroidViewModel
{
    private RepositoryBillBuy RepositoryBillBuy;
    private LiveData<List<BillsBuy>> billbuy;
    public ViewModelBillBuy(Application application)
    {
        super(application);
        RepositoryBillBuy = new RepositoryBillBuy(application);
    }
    public void insert(BillsBuy BillsBuy)
    {
        RepositoryBillBuy.insert(BillsBuy);
    }

    public void delete(BillsBuy BillsBuy)
    {
        RepositoryBillBuy.delete(BillsBuy);
    }

    public void update(BillsBuy BillsBuy)
    {
        RepositoryBillBuy.update(BillsBuy);
    }

    public LiveData<List<BillsBuy>> getBillBuy()
    {
        billbuy = RepositoryBillBuy.getBillBuy();
        return billbuy;
    }

}
